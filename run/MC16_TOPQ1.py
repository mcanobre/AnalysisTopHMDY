# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

import TopExamples.grid


TopExamples.grid.Add("Wjets_mc16a").datasets = [
'mc16_13TeV.361101.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wplusmunu.deriv.DAOD_STDM4.e3601_s3126_r9364_r9315_p3972',
'mc16_13TeV.361100.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wplusenu.deriv.DAOD_STDM4.e3601_s3126_r9364_p3972', 
'mc16_13TeV.361104.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wminusmunu.deriv.DAOD_STDM4.e3601_s3126_r9364_p3972',
'mc16_13TeV.361103.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wminusenu.deriv.DAOD_STDM4.e3601_s3126_r9364_r9315_p3972',
'mc16_13TeV.361102.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wplustaunu.deriv.DAOD_STDM4.e3601_s3126_r9364_r9315_p3975',
'mc16_13TeV.361105.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wminustaunu.deriv.DAOD_STDM4.e3601_s3126_r9364_r9315_p3975'
]

TopExamples.grid.Add("Wjets_mc16d").datasets = [
'mc16_13TeV.361103.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wminusenu.deriv.DAOD_STDM4.e3601_s3126_r10201_p3972',
'mc16_13TeV.361104.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wminusmunu.deriv.DAOD_STDM4.e3601_s3126_r10201_p3972',
'mc16_13TeV.361100.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wplusenu.deriv.DAOD_STDM4.e3601_s3126_r10201_p3972',       
'mc16_13TeV.361101.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wplusmunu.deriv.DAOD_STDM4.e3601_s3126_r10201_p3972',
'mc16_13TeV.361102.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wplustaunu.deriv.DAOD_STDM4.e3601_s3126_r10201_p3975',
'mc16_13TeV.361105.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wminustaunu.deriv.DAOD_STDM4.e3601_s3126_r10201_p3975'
]

TopExamples.grid.Add("Wjets_mc16e").datasets = [
'mc16_13TeV.361100.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wplusenu.deriv.DAOD_STDM4.e3601_e5984_s3126_r10724_r10726_p3972',
'mc16_13TeV.361101.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wplusmunu.deriv.DAOD_STDM4.e3601_s3126_r10724_p3972',
'mc16_13TeV.361104.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wminusmunu.deriv.DAOD_STDM4.e3601_e5984_s3126_s3136_r10724_r10726_p3975',
'mc16_13TeV.361103.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wminusenu.deriv.DAOD_STDM4.e3601_e5984_s3126_r10724_r10726_p3972',
'mc16_13TeV.361102.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wplustaunu.deriv.DAOD_STDM4.e3601_e5984_s3126_r10201_r10210_p3975',
'mc16_13TeV.361105.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wminustaunu.deriv.DAOD_STDM4.e3601_e5984_s3126_r10724_r10726_p3975'
]

TopExamples.grid.Add("Diboson_mc16a_Unbinned").datasets = [
'mc16_13TeV.363355.Sherpa_221_NNPDF30NNLO_ZqqZvv.deriv.DAOD_STDM4.e5525_s3126_r9364_p3975',
'mc16_13TeV.363356.Sherpa_221_NNPDF30NNLO_ZqqZll.deriv.DAOD_STDM4.e5525_s3126_r9364_r9315_p3983',
'mc16_13TeV.363357.Sherpa_221_NNPDF30NNLO_WqqZvv.deriv.DAOD_STDM4.e5525_s3126_r9364_r9315_p3975',
'mc16_13TeV.363358.Sherpa_221_NNPDF30NNLO_WqqZll.deriv.DAOD_STDM4.e5525_s3126_r9364_p3983',
'mc16_13TeV.363359.Sherpa_221_NNPDF30NNLO_WpqqWmlv.deriv.DAOD_STDM4.e5583_s3126_r9364_p3983',
'mc16_13TeV.363360.Sherpa_221_NNPDF30NNLO_WplvWmqq.deriv.DAOD_STDM4.e5983_s3126_r9364_p3983',
'mc16_13TeV.364250.Sherpa_222_NNPDF30NNLO_llll.deriv.DAOD_STDM4.e5894_s3126_r9364_p3983',
'mc16_13TeV.364253.Sherpa_222_NNPDF30NNLO_lllv.deriv.DAOD_STDM4.e5916_s3126_r9364_p3983',
'mc16_13TeV.364254.Sherpa_222_NNPDF30NNLO_llvv.deriv.DAOD_STDM4.e5916_s3126_r9364_r9315_p3983',
'mc16_13TeV.364255.Sherpa_222_NNPDF30NNLO_lvvv.deriv.DAOD_STDM4.e5916_s3126_r9364_r9315_p3983'
]

TopExamples.grid.Add("Diboson_mc16d_Unbinned").datasets = [
'mc16_13TeV.363355.Sherpa_221_NNPDF30NNLO_ZqqZvv.deriv.DAOD_STDM4.e5525_s3126_r10201_p3975',
'mc16_13TeV.363356.Sherpa_221_NNPDF30NNLO_ZqqZll.deriv.DAOD_STDM4.e5525_s3126_r10201_r10210_p3983',
'mc16_13TeV.363357.Sherpa_221_NNPDF30NNLO_WqqZvv.deriv.DAOD_STDM4.e5525_e5984_s3126_r10201_r10210_p3975',
'mc16_13TeV.363358.Sherpa_221_NNPDF30NNLO_WqqZll.deriv.DAOD_STDM4.e5525_s3126_r10201_p3983',
'mc16_13TeV.363359.Sherpa_221_NNPDF30NNLO_WpqqWmlv.deriv.DAOD_STDM4.e5583_s3126_r10201_r10210_p3983',
'mc16_13TeV.363360.Sherpa_221_NNPDF30NNLO_WplvWmqq.deriv.DAOD_STDM4.e5983_e5984_s3126_r10201_r10210_p3983',
'mc16_13TeV.364250.Sherpa_222_NNPDF30NNLO_llll.deriv.DAOD_STDM4.e5894_s3126_r10201_p3983',
'mc16_13TeV.364253.Sherpa_222_NNPDF30NNLO_lllv.deriv.DAOD_STDM4.e5916_s3126_r10201_p3983',
'mc16_13TeV.364254.Sherpa_222_NNPDF30NNLO_llvv.deriv.DAOD_STDM4.e5916_s3126_r10201_p3983',
'mc16_13TeV.364255.Sherpa_222_NNPDF30NNLO_lvvv.deriv.DAOD_STDM4.e5916_s3126_r10201_p3983'
]

TopExamples.grid.Add("Diboson_mc16e_Unbinned").datasets = [
'mc16_13TeV.363355.Sherpa_221_NNPDF30NNLO_ZqqZvv.deriv.DAOD_STDM4.e5525_e5984_s3126_r10724_r10726_p3975',
'mc16_13TeV.363356.Sherpa_221_NNPDF30NNLO_ZqqZll.deriv.DAOD_STDM4.e5525_e5984_s3126_r10724_r10726_p3983',
'mc16_13TeV.363357.Sherpa_221_NNPDF30NNLO_WqqZvv.deriv.DAOD_STDM4.e5525_s3126_r10724_p3975',
'mc16_13TeV.363358.Sherpa_221_NNPDF30NNLO_WqqZll.deriv.DAOD_STDM4.e5525_s3126_r10724_p3983',
'mc16_13TeV.363359.Sherpa_221_NNPDF30NNLO_WpqqWmlv.deriv.DAOD_STDM4.e5583_s3126_r10724_p3983',
'mc16_13TeV.363360.Sherpa_221_NNPDF30NNLO_WplvWmqq.deriv.DAOD_STDM4.e5983_e5984_s3126_r10724_r10726_p3983',
'mc16_13TeV.364250.Sherpa_222_NNPDF30NNLO_llll.deriv.DAOD_STDM4.e5894_s3126_r10724_p3983',
'mc16_13TeV.364253.Sherpa_222_NNPDF30NNLO_lllv.deriv.DAOD_STDM4.e5916_s3126_r10724_p3983',
'mc16_13TeV.364254.Sherpa_222_NNPDF30NNLO_llvv.deriv.DAOD_STDM4.e5916_e5984_s3126_r10724_r10726_p3983',
'mc16_13TeV.364255.Sherpa_222_NNPDF30NNLO_lvvv.deriv.DAOD_STDM4.e5916_e5984_s3126_r10724_r10726_p3983'
]

TopExamples.grid.Add("Diboson_mc16a_Unbinned_2").datasets = [
'mc16_13TeV.364250.Sherpa_222_NNPDF30NNLO_llll.deriv.DAOD_STDM4.e5894_s3126_r9364_p3972',
'mc16_13TeV.364253.Sherpa_222_NNPDF30NNLO_lllv.deriv.DAOD_STDM4.e5916_s3126_r9364_r9315_p3972',
'mc16_13TeV.364254.Sherpa_222_NNPDF30NNLO_llvv.deriv.DAOD_STDM4.e5916_s3126_r9364_p3972',
'mc16_13TeV.364255.Sherpa_222_NNPDF30NNLO_lvvv.deriv.DAOD_STDM4.e5916_s3126_r9364_r9315_p3972'
]

TopExamples.grid.Add("Diboson_mc16d_Unbinned_2").datasets = [
'mc16_13TeV.364250.Sherpa_222_NNPDF30NNLO_llll.deriv.DAOD_STDM4.e5894_e5984_s3126_r10201_r10210_p3972',
'mc16_13TeV.364253.Sherpa_222_NNPDF30NNLO_lllv.deriv.DAOD_STDM4.e5916_e5984_s3126_r10201_r10210_p3972',
'mc16_13TeV.364254.Sherpa_222_NNPDF30NNLO_llvv.deriv.DAOD_STDM4.e5916_e5984_s3126_r10201_r10210_p3972',
'mc16_13TeV.364255.Sherpa_222_NNPDF30NNLO_lvvv.deriv.DAOD_STDM4.e5916_e5984_s3126_r10201_r10210_p3972'
]

TopExamples.grid.Add("Diboson_mc16e_Unbinned_2").datasets = [
'mc16_13TeV.364250.Sherpa_222_NNPDF30NNLO_llll.deriv.DAOD_STDM4.e5894_s3126_r10724_p3972',
'mc16_13TeV.364253.Sherpa_222_NNPDF30NNLO_lllv.deriv.DAOD_STDM4.e5916_s3126_r10724_p3972',
'mc16_13TeV.364254.Sherpa_222_NNPDF30NNLO_llvv.deriv.DAOD_STDM4.e5916_e5984_s3126_r10724_r10726_p3972',
'mc16_13TeV.364255.Sherpa_222_NNPDF30NNLO_lvvv.deriv.DAOD_STDM4.e5916_s3126_r10724_p3972'
]



TopExamples.grid.Add("Ztoee_mc16a").datasets = [
	'mc16_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.deriv.DAOD_STDM4.e3601_s3126_r9364_r9315_p3985'
	]

TopExamples.grid.Add("Ztoee_mc16d").datasets = [
	'mc16_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.deriv.DAOD_STDM4.e3601_s3126_r10201_p3985'
	]

TopExamples.grid.Add("Ztoee_mc16e").datasets = [
	'mc16_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.deriv.DAOD_STDM4.e3601_s3126_r10724_p3985'
	]

TopExamples.grid.Add("Ztomumu_mc16a").datasets = [
	'mc16_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.deriv.DAOD_STDM4.e3601_s3126_r9364_r9315_p3985'
	]

TopExamples.grid.Add("Ztomumu_mc16d").datasets = [
	'mc16_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.deriv.DAOD_STDM4.e3601_e5984_s3126_r10201_r10210_p3985'
	]

TopExamples.grid.Add("Ztomumu_mc16e").datasets = [
	'mc16_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.deriv.DAOD_STDM4.e3601_e5984_s3126_r10724_r10726_p3985'
	]


TopExamples.grid.Add("Ztotautau_mc16a").datasets = [
    'mc16_13TeV.361108.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.deriv.DAOD_STDM4.e3601_s3126_r9364_r9315_p3983'
]

TopExamples.grid.Add("Ztotautau_mc16d").datasets = [
    'mc16_13TeV.361108.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.deriv.DAOD_STDM4.e3601_s3126_r10239_r10210_p3983'
]

TopExamples.grid.Add("Ztotautau_mc16e").datasets = [
    'mc16_13TeV.361108.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.deriv.DAOD_STDM4.e3601_e5984_s3126_s3136_r10724_r10726_p3983'
    ]

TopExamples.grid.Add("ttbar_mc16a").datasets = [
    'mc16_13TeV.361108.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.deriv.DAOD_STDM4.e3601_e5984_s3126_s3136_r10724_r10726_p3983'
    ]

TopExamples.grid.Add("TopQuark_mc16a").datasets = [ 
    'mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_STDM4.e6337_a875_r9364_p3983',
    'mc16_13TeV.410471.PhPy8EG_A14_ttbar_hdamp258p75_allhad.deriv.DAOD_STDM4.e6337_s3126_r9364_p3972',
    'mc16_13TeV.410472.PhPy8EG_A14_ttbar_hdamp258p75_dil.deriv.DAOD_STDM4.e6348_s3126_r9364_p3972',
'mc16_13TeV.410644.PowhegPythia8EvtGen_A14_singletop_schan_lept_top.deriv.DAOD_STDM4.e6527_e5984_s3126_r9364_r9315_p3983', 
'mc16_13TeV.410645.PowhegPythia8EvtGen_A14_singletop_schan_lept_antitop.deriv.DAOD_STDM4.e6527_e5984_s3126_r9364_r9315_p3983',
'mc16_13TeV.410646.PowhegPythia8EvtGen_A14_Wt_DR_inclusive_top.deriv.DAOD_STDM4.e6552_e5984_s3126_r9364_r9315_p3983',
'mc16_13TeV.410647.PowhegPythia8EvtGen_A14_Wt_DR_inclusive_antitop.deriv.DAOD_STDM4.e6552_e5984_s3126_r9364_r9315_p3983',
'mc16_13TeV.410658.PhPy8EG_A14_tchan_BW50_lept_top.deriv.DAOD_STDM4.e6671_s3126_r9364_p3983',
'mc16_13TeV.410659.PhPy8EG_A14_tchan_BW50_lept_antitop.deriv.DAOD_STDM4.e6671_s3126_r9364_p3983',
'mc16_13TeV.410408.aMcAtNloPythia8EvtGen_tWZ_Ztoll_minDR1.deriv.DAOD_TOPQ1.e6423_e5984_s3126_r9364_r9315_p4031'
    ]


TopExamples.grid.Add("TopQuark_mc16d").datasets = [ 
    'mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_STDM4.e6337_e5984_a875_r10201_r10210_p3983',
    'mc16_13TeV.410471.PhPy8EG_A14_ttbar_hdamp258p75_allhad.deriv.DAOD_STDM4.e6337_s3126_r10201_p3972',
    'mc16_13TeV.410472.PhPy8EG_A14_ttbar_hdamp258p75_dil.deriv.DAOD_STDM4.e6348_e5984_s3126_r10201_r10210_p3972',
'mc16_13TeV.410644.PowhegPythia8EvtGen_A14_singletop_schan_lept_top.deriv.DAOD_STDM4.e6527_s3126_r10201_p3983',
'mc16_13TeV.410645.PowhegPythia8EvtGen_A14_singletop_schan_lept_antitop.deriv.DAOD_STDM4.e6527_s3126_r10201_r10210_p3983',
'mc16_13TeV.410646.PowhegPythia8EvtGen_A14_Wt_DR_inclusive_top.deriv.DAOD_STDM4.e6552_e5984_s3126_r10201_r10210_p3983',
'mc16_13TeV.410647.PowhegPythia8EvtGen_A14_Wt_DR_inclusive_antitop.deriv.DAOD_STDM4.e6552_e5984_s3126_r10201_r10210_p3983',
'mc16_13TeV.410658.PhPy8EG_A14_tchan_BW50_lept_top.deriv.DAOD_STDM4.e6671_s3126_r10201_p3983',
'mc16_13TeV.410659.PhPy8EG_A14_tchan_BW50_lept_antitop.deriv.DAOD_STDM4.e6671_s3126_r10201_p3983'
    ]

TopExamples.grid.Add("TopQuark_mc16e").datasets = [ 
    'mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_STDM4.e6337_a875_r10724_p3983',
    'mc16_13TeV.410471.PhPy8EG_A14_ttbar_hdamp258p75_allhad.deriv.DAOD_STDM4.e6337_e5984_s3126_s3136_r10724_r10726_p3972',
    'mc16_13TeV.410472.PhPy8EG_A14_ttbar_hdamp258p75_dil.deriv.DAOD_STDM4.e6348_s3126_r10724_p3972',
'mc16_13TeV.410644.PowhegPythia8EvtGen_A14_singletop_schan_lept_top.deriv.DAOD_STDM4.e6527_e5984_s3126_r10724_r10726_p3983',
'mc16_13TeV.410645.PowhegPythia8EvtGen_A14_singletop_schan_lept_antitop.deriv.DAOD_STDM4.e6527_s3126_r10724_p3983',
'mc16_13TeV.410646.PowhegPythia8EvtGen_A14_Wt_DR_inclusive_top.deriv.DAOD_STDM4.e6552_s3126_r10724_p3983',
'mc16_13TeV.410647.PowhegPythia8EvtGen_A14_Wt_DR_inclusive_antitop.deriv.DAOD_STDM4.e6552_s3126_r10724_p3983',
'mc16_13TeV.410658.PhPy8EG_A14_tchan_BW50_lept_top.deriv.DAOD_STDM4.e6671_e5984_s3126_s3136_r10724_r10726_p3983',
'mc16_13TeV.410659.PhPy8EG_A14_tchan_BW50_lept_antitop.deriv.DAOD_STDM4.e6671_e5984_s3126_s3136_r10724_r10726_p3983',
'mc16_13TeV.410408.aMcAtNloPythia8EvtGen_tWZ_Ztoll_minDR1.deriv.DAOD_TOPQ1.e6423_e5984_s3126_r10724_r10726_p4031'
    ]

TopExamples.grid.Add("TopQuark_mc16a_Sys").datasets = [ 
'mc16_13TeV.410464.aMcAtNloPy8EvtGen_MEN30NLO_A14N23LO_ttbar_noShWe_SingleLep.deriv.DAOD_TOPQ1.e6762_a875_r9364_p4031',
'mc16_13TeV.410465.aMcAtNloPy8EvtGen_MEN30NLO_A14N23LO_ttbar_noShWe_dil.deriv.DAOD_TOPQ1.e6762_a875_r9364_p4031',
'mc16_13TeV.410480.PhPy8EG_A14_ttbar_hdamp517p5_SingleLep.deriv.DAOD_TOPQ1.e6454_a875_r9364_p4031',
'mc16_13TeV.410482.PhPy8EG_A14_ttbar_hdamp517p5_dil.deriv.DAOD_TOPQ1.e6454_a875_r9364_p4031', 
'mc16_13TeV.410557.PowhegHerwig7EvtGen_H7UE_tt_hdamp258p75_704_SingleLep.deriv.DAOD_TOPQ1.e6366_e5984_a875_r9364_r9315_p4031',
'mc16_13TeV.410558.PowhegHerwig7EvtGen_H7UE_tt_hdamp258p75_704_dil.deriv.DAOD_TOPQ1.e6366_e5984_a875_r9364_r9315_p4031',
'mc16_13TeV.411054.PowhegPythia8EvtGen_ttbar_172p00_dilep.deriv.DAOD_TOPQ1.e6696_e5984_a875_r9364_r9315_p4031',
'mc16_13TeV.411057.PowhegPythia8EvtGen_ttbar_173p00_dilep.deriv.DAOD_TOPQ1.e6696_e5984_a875_r9364_r9315_p4031'
    ]

TopExamples.grid.Add("TopQuark_mc16d_Sys").datasets = [ 
'mc16_13TeV.410464.aMcAtNloPy8EvtGen_MEN30NLO_A14N23LO_ttbar_noShWe_SingleLep.deriv.DAOD_TOPQ1.e6762_a875_r10201_p4031',
'mc16_13TeV.410465.aMcAtNloPy8EvtGen_MEN30NLO_A14N23LO_ttbar_noShWe_dil.deriv.DAOD_TOPQ1.e6762_e5984_a875_r10201_r10210_p4031', 
'mc16_13TeV.410480.PhPy8EG_A14_ttbar_hdamp517p5_SingleLep.deriv.DAOD_TOPQ1.e6454_a875_r10201_p4031',
'mc16_13TeV.410482.PhPy8EG_A14_ttbar_hdamp517p5_dil.deriv.DAOD_TOPQ1.e6454_e5984_a875_r10201_r10210_p4031',
'mc16_13TeV.410557.PowhegHerwig7EvtGen_H7UE_tt_hdamp258p75_704_SingleLep.deriv.DAOD_TOPQ1.e6366_a875_r10201_p4031', 
'mc16_13TeV.410558.PowhegHerwig7EvtGen_H7UE_tt_hdamp258p75_704_dil.deriv.DAOD_TOPQ1.e6366_e5984_a875_r10201_r10210_p4031', 
'mc16_13TeV.411054.PowhegPythia8EvtGen_ttbar_172p00_dilep.deriv.DAOD_TOPQ1.e6696_e5984_a875_r10201_r10210_p4031',
'mc16_13TeV.411057.PowhegPythia8EvtGen_ttbar_173p00_dilep.deriv.DAOD_TOPQ1.e6696_e5984_a875_r10201_r10210_p4031'
    ]

TopExamples.grid.Add("TopQuark_mc16e_Sys").datasets = [ 
'mc16_13TeV.410464.aMcAtNloPy8EvtGen_MEN30NLO_A14N23LO_ttbar_noShWe_SingleLep.deriv.DAOD_TOPQ1.e6762_e5984_a875_r10724_r10726_p4031',
'mc16_13TeV.410465.aMcAtNloPy8EvtGen_MEN30NLO_A14N23LO_ttbar_noShWe_dil.deriv.DAOD_TOPQ1.e6762_e5984_a875_r10724_r10726_p4031',
'mc16_13TeV.410480.PhPy8EG_A14_ttbar_hdamp517p5_SingleLep.deriv.DAOD_TOPQ1.e6454_a875_r10724_p4031',
'mc16_13TeV.410482.PhPy8EG_A14_ttbar_hdamp517p5_dil.deriv.DAOD_TOPQ1.e6454_a875_r10724_p4031',
'mc16_13TeV.410557.PowhegHerwig7EvtGen_H7UE_tt_hdamp258p75_704_SingleLep.deriv.DAOD_TOPQ1.e6366_e5984_a875_r10724_r10726_p4031',  
'mc16_13TeV.410558.PowhegHerwig7EvtGen_H7UE_tt_hdamp258p75_704_dil.deriv.DAOD_TOPQ1.e6366_e5984_a875_r10724_r10726_p4031',  
'mc16_13TeV.411054.PowhegPythia8EvtGen_ttbar_172p00_dilep.deriv.DAOD_TOPQ1.e6696_a875_r10724_p4031',
'mc16_13TeV.411057.PowhegPythia8EvtGen_ttbar_173p00_dilep.deriv.DAOD_TOPQ1.e6696_e5984_a875_r10724_r10726_p4031'
    ]

TopExamples.grid.Add("PI_ee_mc16a").datasets = [ 
'mc16_13TeV.364834.Pythia8EvtGen_A14_NNPDF31NLOLUXQED_ggTOee_20M60.deriv.DAOD_STDM4.e7621_e5984_s3126_r9364_r9315_p3956',                      
'mc16_13TeV.364835.Pythia8EvtGen_A14_NNPDF31NLOLUXQED_ggTOee_60M200.deriv.DAOD_STDM4.e7621_s3126_r9364_p3956',                                 
'mc16_13TeV.364836.Pythia8EvtGen_A14_NNPDF31NLOLUXQED_ggTOee_200M600.deriv.DAOD_STDM4.e7621_s3126_r9364_p3956',                                
'mc16_13TeV.364837.Pythia8EvtGen_A14_NNPDF31NLOLUXQED_ggTOee_600M1500.deriv.DAOD_STDM4.e7621_e5984_s3126_r9364_r9315_p3956',                   
'mc16_13TeV.364838.Pythia8EvtGen_A14_NNPDF31NLOLUXQED_ggTOee_1500M2500.deriv.DAOD_STDM4.e7621_s3126_r9364_p3956',                              
'mc16_13TeV.364839.Pythia8EvtGen_A14_NNPDF31NLOLUXQED_ggTOee_2500M5000.deriv.DAOD_STDM4.e7621_s3126_r9364_p3956'
]

TopExamples.grid.Add("DYee_mc16a_LowMass").datasets = [
'mc16_13TeV.301000.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_120M180.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985             ',
'mc16_13TeV.301001.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_180M250.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985 ',
'mc16_13TeV.301002.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_250M400.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985             ',
'mc16_13TeV.301003.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_400M600.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985             ',
'mc16_13TeV.301004.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_600M800.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985             ',
'mc16_13TeV.301005.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_800M1000.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985      '
]

TopExamples.grid.Add("DYee_mc16a").datasets = [ 
'mc16_13TeV.301000.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_120M180.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985             ',
'mc16_13TeV.301001.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_180M250.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985 ',
'mc16_13TeV.301002.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_250M400.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985             ',
'mc16_13TeV.301003.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_400M600.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985             ',
'mc16_13TeV.301004.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_600M800.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985	      ',
'mc16_13TeV.301005.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_800M1000.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985      ',
'mc16_13TeV.301006.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_1000M1250.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985     ',
'mc16_13TeV.301007.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_1250M1500.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985           ',
'mc16_13TeV.301008.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_1500M1750.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985           ',
'mc16_13TeV.301009.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_1750M2000.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985           ',
'mc16_13TeV.301010.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_2000M2250.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985           ',
'mc16_13TeV.301011.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_2250M2500.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985     ',
'mc16_13TeV.301012.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_2500M2750.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985           ',
'mc16_13TeV.301013.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_2750M3000.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985           ',
'mc16_13TeV.301014.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_3000M3500.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985     ',
'mc16_13TeV.301015.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_3500M4000.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985     ',
'mc16_13TeV.301016.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_4000M4500.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985     ',
'mc16_13TeV.301017.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_4500M5000.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985     ',
'mc16_13TeV.301018.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_5000M.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985         '
      ]   

TopExamples.grid.Add("PI_ee_mc16d").datasets = [
'mc16_13TeV.364834.Pythia8EvtGen_A14_NNPDF31NLOLUXQED_ggTOee_20M60.deriv.DAOD_STDM4.e7621_s3126_r10201_p3956',                                   
'mc16_13TeV.364835.Pythia8EvtGen_A14_NNPDF31NLOLUXQED_ggTOee_60M200.deriv.DAOD_STDM4.e7621_s3126_r10201_p3956',                                  
'mc16_13TeV.364836.Pythia8EvtGen_A14_NNPDF31NLOLUXQED_ggTOee_200M600.deriv.DAOD_STDM4.e7621_s3126_r10201_p3956',                                 
'mc16_13TeV.364837.Pythia8EvtGen_A14_NNPDF31NLOLUXQED_ggTOee_600M1500.deriv.DAOD_STDM4.e7621_s3126_r10201_p3956',                                
'mc16_13TeV.364838.Pythia8EvtGen_A14_NNPDF31NLOLUXQED_ggTOee_1500M2500.deriv.DAOD_STDM4.e7621_e5984_s3126_r10201_r10210_p3956',                  
'mc16_13TeV.364839.Pythia8EvtGen_A14_NNPDF31NLOLUXQED_ggTOee_2500M5000.deriv.DAOD_STDM4.e7621_e5984_s3126_r10201_r10210_p3956',                  
'mc16_13TeV.364840.Pythia8EvtGen_A14_NNPDF31NLOLUXQED_ggTOee_5000M.deriv.DAOD_STDM4.e7621_s3126_r10201_p3956'
]

TopExamples.grid.Add("DYee_mc16d_LowMass").datasets = [
'mc16_13TeV.301000.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_120M180.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985                               ',
'mc16_13TeV.301001.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_180M250.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985   ',
'mc16_13TeV.301002.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_250M400.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985                  ',
'mc16_13TeV.301003.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_400M600.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985                        ',
'mc16_13TeV.301004.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_600M800.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985                               ',
'mc16_13TeV.301005.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_800M1000.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985                              '
]

TopExamples.grid.Add("DYee_mc16d").datasets = [     
'mc16_13TeV.301000.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_120M180.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985                               ',
'mc16_13TeV.301001.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_180M250.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985   ',
'mc16_13TeV.301002.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_250M400.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985                  ',
'mc16_13TeV.301003.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_400M600.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985                        ',
'mc16_13TeV.301004.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_600M800.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985                               ',
'mc16_13TeV.301005.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_800M1000.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985                              ',
'mc16_13TeV.301006.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_1000M1250.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985                             ',
'mc16_13TeV.301007.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_1250M1500.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985                ',
'mc16_13TeV.301008.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_1500M1750.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985                ',
'mc16_13TeV.301009.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_1750M2000.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985                ',
'mc16_13TeV.301010.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_2000M2250.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985                ',
'mc16_13TeV.301011.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_2250M2500.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985                ',
'mc16_13TeV.301012.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_2500M2750.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985                ',
'mc16_13TeV.301013.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_2750M3000.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985                ',
'mc16_13TeV.301014.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_3000M3500.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985                             ',
'mc16_13TeV.301015.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_3500M4000.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985                ',
'mc16_13TeV.301016.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_4000M4500.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985                             ',
'mc16_13TeV.301017.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_4500M5000.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985                             ',
'mc16_13TeV.301018.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_5000M.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985                                 '
      ]   

TopExamples.grid.Add("PI_ee_mc16e").datasets = [
'mc16_13TeV.364834.Pythia8EvtGen_A14_NNPDF31NLOLUXQED_ggTOee_20M60.deriv.DAOD_STDM4.e7621_s3126_r10724_p3956',                                   
'mc16_13TeV.364835.Pythia8EvtGen_A14_NNPDF31NLOLUXQED_ggTOee_60M200.deriv.DAOD_STDM4.e7621_e5984_s3126_r10724_r10726_p3956',                     
'mc16_13TeV.364836.Pythia8EvtGen_A14_NNPDF31NLOLUXQED_ggTOee_200M600.deriv.DAOD_STDM4.e7621_e5984_s3126_r10724_r10726_p3956',                    
'mc16_13TeV.364837.Pythia8EvtGen_A14_NNPDF31NLOLUXQED_ggTOee_600M1500.deriv.DAOD_STDM4.e7621_s3126_r10724_p3956',                                
'mc16_13TeV.364838.Pythia8EvtGen_A14_NNPDF31NLOLUXQED_ggTOee_1500M2500.deriv.DAOD_STDM4.e7621_e5984_s3126_r10724_r10726_p3956',                  
'mc16_13TeV.364839.Pythia8EvtGen_A14_NNPDF31NLOLUXQED_ggTOee_2500M5000.deriv.DAOD_STDM4.e7621_s3126_r10724_p3956',                               
'mc16_13TeV.364840.Pythia8EvtGen_A14_NNPDF31NLOLUXQED_ggTOee_5000M.deriv.DAOD_STDM4.e7621_s3126_r10724_p3956'
]

TopExamples.grid.Add("DYee_mc16e_LowMass").datasets = [
'mc16_13TeV.301000.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_120M180.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985                               ',
'mc16_13TeV.301001.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_180M250.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985',
'mc16_13TeV.301002.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_250M400.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985                               ',
'mc16_13TeV.301003.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_400M600.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985   ',
'mc16_13TeV.301004.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_600M800.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985   ',
'mc16_13TeV.301005.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_800M1000.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985                 '
]

TopExamples.grid.Add("DYee_mc16e").datasets = [
'mc16_13TeV.301000.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_120M180.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985                               ',
'mc16_13TeV.301001.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_180M250.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985',
'mc16_13TeV.301002.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_250M400.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985                               ',
'mc16_13TeV.301003.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_400M600.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985   ',
'mc16_13TeV.301004.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_600M800.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985   ',
'mc16_13TeV.301005.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_800M1000.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985                 ',
'mc16_13TeV.301006.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_1000M1250.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985                             ',
'mc16_13TeV.301007.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_1250M1500.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985                ',
'mc16_13TeV.301008.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_1500M1750.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985                             ',
'mc16_13TeV.301009.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_1750M2000.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985                             ',
'mc16_13TeV.301010.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_2000M2250.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985                ',
'mc16_13TeV.301011.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_2250M2500.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985                             ',
'mc16_13TeV.301012.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_2500M2750.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985                             ',
'mc16_13TeV.301013.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_2750M3000.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985                ',
'mc16_13TeV.301014.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_3000M3500.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985 ',
'mc16_13TeV.301015.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_3500M4000.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985',
'mc16_13TeV.301016.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_4000M4500.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985                ',
'mc16_13TeV.301017.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_4500M5000.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985                ',
'mc16_13TeV.301018.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_5000M.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985                                 '
    ]

TopExamples.grid.Add("PI_mumu_mc16e").datasets = [
'mc16_13TeV.364841.Pythia8EvtGen_A14_NNPDF31NLOLUXQED_ggTOmumu_20M60.deriv.DAOD_STDM4.e7621_s3126_r10724_p3956',                                 
'mc16_13TeV.364842.Pythia8EvtGen_A14_NNPDF31NLOLUXQED_ggTOmumu_60M200.deriv.DAOD_STDM4.e7621_s3126_r10724_p3956',                                
'mc16_13TeV.364843.Pythia8EvtGen_A14_NNPDF31NLOLUXQED_ggTOmumu_200M600.deriv.DAOD_STDM4.e7621_e5984_s3126_r10724_r10726_p3956',                  
'mc16_13TeV.364844.Pythia8EvtGen_A14_NNPDF31NLOLUXQED_ggTOmumu_600M1500.deriv.DAOD_STDM4.e7621_e5984_s3126_r10724_r10726_p3956',                 
'mc16_13TeV.364845.Pythia8EvtGen_A14_NNPDF31NLOLUXQED_ggTOmumu_1500M2500.deriv.DAOD_STDM4.e7621_s3126_r10724_p3956',                             
'mc16_13TeV.364846.Pythia8EvtGen_A14_NNPDF31NLOLUXQED_ggTOmumu_2500M5000.deriv.DAOD_STDM4.e7621_e5984_s3126_r10724_r10726_p3956',                
'mc16_13TeV.364847.Pythia8EvtGen_A14_NNPDF31NLOLUXQED_ggTOmumu_5000M.deriv.DAOD_STDM4.e7621_e5984_s3126_r10724_r10726_p3956'     
]

TopExamples.grid.Add("DYmumu_mc16e_LowMass").datasets = [
'mc16_13TeV.301020.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_120M180.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985   ',
'mc16_13TeV.301021.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_180M250.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985   ',
'mc16_13TeV.301022.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_250M400.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985                  ',
'mc16_13TeV.301023.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_400M600.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985',
'mc16_13TeV.301024.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_600M800.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985                               ',
'mc16_13TeV.301025.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_800M1000.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985                              '
]
	
TopExamples.grid.Add("DYmumu_mc16e").datasets = [       
'mc16_13TeV.301020.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_120M180.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985   ',
'mc16_13TeV.301021.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_180M250.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985   ',
'mc16_13TeV.301022.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_250M400.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985                  ',
'mc16_13TeV.301023.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_400M600.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985',
'mc16_13TeV.301024.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_600M800.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985                               ',
'mc16_13TeV.301025.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_800M1000.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985                              ',
'mc16_13TeV.301026.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_1000M1250.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985 ',
'mc16_13TeV.301027.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_1250M1500.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985                             ',
'mc16_13TeV.301028.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_1500M1750.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985                ',
'mc16_13TeV.301029.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_1750M2000.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985 ',
'mc16_13TeV.301030.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_2000M2250.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985                ',
'mc16_13TeV.301031.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_2250M2500.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985                ',
'mc16_13TeV.301032.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_2500M2750.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985                ',
'mc16_13TeV.301033.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_2750M3000.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985                             ',
'mc16_13TeV.301034.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_3000M3500.deriv.DAOD_STDM4.e3649_s3126_r10724_p3731',
'mc16_13TeV.301035.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_3500M4000.deriv.DAOD_STDM4.e3649_s3126_r10724_p3731',
'mc16_13TeV.301036.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_4000M4500.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985                ',
'mc16_13TeV.301037.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_4500M5000.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985 ',
'mc16_13TeV.301038.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_5000M.deriv.DAOD_STDM4.e3649_s3126_r10724_p3985'
    ]

TopExamples.grid.Add("301022_mc16e").datasets = [
'mc16_13TeV.301022.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_250M400.deriv.DAOD_STDM4.e3649_s3126_r10724_p3731'
      ]


TopExamples.grid.Add("PI_mumu_mc16d").datasets = [ 
'mc16_13TeV.364841.Pythia8EvtGen_A14_NNPDF31NLOLUXQED_ggTOmumu_20M60.deriv.DAOD_STDM4.e7621_s3126_r10201_p3956',                               
'mc16_13TeV.364842.Pythia8EvtGen_A14_NNPDF31NLOLUXQED_ggTOmumu_60M200.deriv.DAOD_STDM4.e7621_e5984_s3126_r10201_r10210_p3956',                   
'mc16_13TeV.364843.Pythia8EvtGen_A14_NNPDF31NLOLUXQED_ggTOmumu_200M600.deriv.DAOD_STDM4.e7621_e5984_s3126_r10201_r10210_p3956',                  
'mc16_13TeV.364844.Pythia8EvtGen_A14_NNPDF31NLOLUXQED_ggTOmumu_600M1500.deriv.DAOD_STDM4.e7621_s3126_r10201_p3956',                              
'mc16_13TeV.364845.Pythia8EvtGen_A14_NNPDF31NLOLUXQED_ggTOmumu_1500M2500.deriv.DAOD_STDM4.e7621_e5984_s3126_r10201_r10210_p3956',                
'mc16_13TeV.364846.Pythia8EvtGen_A14_NNPDF31NLOLUXQED_ggTOmumu_2500M5000.deriv.DAOD_STDM4.e7621_e5984_s3126_r10201_r10210_p3956',                
'mc16_13TeV.364847.Pythia8EvtGen_A14_NNPDF31NLOLUXQED_ggTOmumu_5000M.deriv.DAOD_STDM4.e7621_s3126_r10201_p3956',  
]

TopExamples.grid.Add("DYmumu_mc16d_LowMass").datasets = [
'mc16_13TeV.301020.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_120M180.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985                               ',
'mc16_13TeV.301021.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_180M250.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985                  ',
'mc16_13TeV.301022.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_250M400.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985                  ',
'mc16_13TeV.301023.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_400M600.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985  ',
'mc16_13TeV.301024.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_600M800.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985                  ',
'mc16_13TeV.301025.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_800M1000.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985                              ',
]

TopExamples.grid.Add("DYmumu_mc16d").datasets = [ 
'mc16_13TeV.301020.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_120M180.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985                               ',
'mc16_13TeV.301021.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_180M250.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985                  ',
'mc16_13TeV.301022.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_250M400.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985                  ',
'mc16_13TeV.301023.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_400M600.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985  ',
'mc16_13TeV.301024.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_600M800.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985                  ',
'mc16_13TeV.301025.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_800M1000.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985                              ',
'mc16_13TeV.301026.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_1000M1250.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985                ',
'mc16_13TeV.301027.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_1250M1500.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985                ',
'mc16_13TeV.301028.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_1500M1750.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985                             ',
'mc16_13TeV.301029.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_1750M2000.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985                ',
'mc16_13TeV.301030.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_2000M2250.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985                             ',
'mc16_13TeV.301031.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_2250M2500.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985                             ',
'mc16_13TeV.301032.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_2500M2750.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985                             ',
'mc16_13TeV.301033.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_2750M3000.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985                             ',
'mc16_13TeV.301034.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_3000M3500.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985                ',
'mc16_13TeV.301035.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_3500M4000.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985                             ',
'mc16_13TeV.301036.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_4000M4500.deriv.DAOD_STDM4.e3649_e5984_r10201_p3985                ',
'mc16_13TeV.301037.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_4500M5000.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985                             ',
'mc16_13TeV.301038.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_5000M.deriv.DAOD_STDM4.e3649_s3126_r10201_p3985                                 ' 
      ]   

TopExamples.grid.Add("301022_mc16d").datasets = [
'mc16_13TeV.301022.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_250M400.deriv.DAOD_STDM4.e3649_e5984_s3126_r10201_r10210_p3731'
      ]    

TopExamples.grid.Add("PI_mumu_mc16a").datasets = [
'mc16_13TeV.364841.Pythia8EvtGen_A14_NNPDF31NLOLUXQED_ggTOmumu_20M60.deriv.DAOD_STDM4.e7621_s3126_r9364_p3956',                                
'mc16_13TeV.364842.Pythia8EvtGen_A14_NNPDF31NLOLUXQED_ggTOmumu_60M200.deriv.DAOD_STDM4.e7621_s3126_r9364_p3956',                               
'mc16_13TeV.364843.Pythia8EvtGen_A14_NNPDF31NLOLUXQED_ggTOmumu_200M600.deriv.DAOD_STDM4.e7621_e5984_s3126_r9364_r9315_p3956',                  
'mc16_13TeV.364844.Pythia8EvtGen_A14_NNPDF31NLOLUXQED_ggTOmumu_600M1500.deriv.DAOD_STDM4.e7621_s3126_r9364_p3956',                             
'mc16_13TeV.364845.Pythia8EvtGen_A14_NNPDF31NLOLUXQED_ggTOmumu_1500M2500.deriv.DAOD_STDM4.e7621_e5984_s3126_r9364_r9315_p3956',                
'mc16_13TeV.364846.Pythia8EvtGen_A14_NNPDF31NLOLUXQED_ggTOmumu_2500M5000.deriv.DAOD_STDM4.e7621_e5984_s3126_r9364_r9315_p3956',                
'mc16_13TeV.364847.Pythia8EvtGen_A14_NNPDF31NLOLUXQED_ggTOmumu_5000M.deriv.DAOD_STDM4.e7621_s3126_r9364_p3956'
]

TopExamples.grid.Add("DYmumu_mc16a_LowMass").datasets = [
'mc16_13TeV.301020.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_120M180.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985               ',
'mc16_13TeV.301021.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_180M250.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985   ',
'mc16_13TeV.301022.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_250M400.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985         ',
'mc16_13TeV.301023.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_400M600.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985   ',
'mc16_13TeV.301024.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_600M800.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985   ',
'mc16_13TeV.301025.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_800M1000.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985        '
]

TopExamples.grid.Add("DYmumu_mc16a").datasets = [  
'mc16_13TeV.301020.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_120M180.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985               ',
'mc16_13TeV.301021.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_180M250.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985   ',
'mc16_13TeV.301022.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_250M400.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985         ',
'mc16_13TeV.301023.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_400M600.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985   ',
'mc16_13TeV.301024.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_600M800.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985   ',
'mc16_13TeV.301025.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_800M1000.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985        ',
'mc16_13TeV.301026.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_1000M1250.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985             ',
'mc16_13TeV.301027.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_1250M1500.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985 ',
'mc16_13TeV.301028.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_1500M1750.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985             ',
'mc16_13TeV.301029.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_1750M2000.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985 ',
'mc16_13TeV.301030.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_2000M2250.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985       ',
'mc16_13TeV.301031.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_2250M2500.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985',
'mc16_13TeV.301032.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_2500M2750.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985',
'mc16_13TeV.301033.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_2750M3000.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985 ',
'mc16_13TeV.301034.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_3000M3500.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985             ',
'mc16_13TeV.301035.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_3500M4000.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985 ',
'mc16_13TeV.301036.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_4000M4500.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985',
'mc16_13TeV.301037.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_4500M5000.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985             ',
'mc16_13TeV.301038.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_5000M.deriv.DAOD_STDM4.e3649_s3126_r9364_p3985           '
      ]   


TopExamples.grid.Add("Data15Period").datasets = [
'data15_13TeV.periodD.physics_Main.PhysCont.DAOD_STDM4.grp23_v01_p3984',
'data15_13TeV.periodE.physics_Main.PhysCont.DAOD_STDM4.grp23_v01_p3984',
'data15_13TeV.periodF.physics_Main.PhysCont.DAOD_STDM4.grp23_v01_p3984',
'data15_13TeV.periodG.physics_Main.PhysCont.DAOD_STDM4.grp23_v01_p3984',
'data15_13TeV.periodH.physics_Main.PhysCont.DAOD_STDM4.grp23_v01_p3984',
'data15_13TeV.periodJ.physics_Main.PhysCont.DAOD_STDM4.grp23_v01_p3984'
]

TopExamples.grid.Add("Data16Period").datasets = [
'data16_13TeV.periodA.physics_Main.PhysCont.DAOD_STDM4.grp23_v01_p3984',
'data16_13TeV.periodB.physics_Main.PhysCont.DAOD_STDM4.grp23_v01_p3984',
'data16_13TeV.periodC.physics_Main.PhysCont.DAOD_STDM4.grp23_v01_p3984',
'data16_13TeV.periodD.physics_Main.PhysCont.DAOD_STDM4.grp23_v01_p3984',
'data16_13TeV.periodE.physics_Main.PhysCont.DAOD_STDM4.grp23_v01_p3984',
'data16_13TeV.periodF.physics_Main.PhysCont.DAOD_STDM4.grp23_v01_p3984',
'data16_13TeV.periodG.physics_Main.PhysCont.DAOD_STDM4.grp23_v01_p3984',
'data16_13TeV.periodI.physics_Main.PhysCont.DAOD_STDM4.grp23_v01_p3984',
'data16_13TeV.periodK.physics_Main.PhysCont.DAOD_STDM4.grp23_v01_p3984',
'data16_13TeV.periodL.physics_Main.PhysCont.DAOD_STDM4.grp23_v01_p3984'
]

TopExamples.grid.Add("Data17Period").datasets = [
'data17_13TeV.periodB.physics_Main.PhysCont.DAOD_STDM4.grp23_v01_p3984',
'data17_13TeV.periodC.physics_Main.PhysCont.DAOD_STDM4.grp23_v01_p3984',
'data17_13TeV.periodD.physics_Main.PhysCont.DAOD_STDM4.grp23_v01_p3984',
'data17_13TeV.periodE.physics_Main.PhysCont.DAOD_STDM4.grp23_v01_p3984',
'data17_13TeV.periodF.physics_Main.PhysCont.DAOD_STDM4.grp23_v01_p3984',
'data17_13TeV.periodH.physics_Main.PhysCont.DAOD_STDM4.grp23_v01_p3984',
'data17_13TeV.periodI.physics_Main.PhysCont.DAOD_STDM4.grp23_v01_p3984',
'data17_13TeV.periodK.physics_Main.PhysCont.DAOD_STDM4.grp23_v01_p3984'
]


TopExamples.grid.Add("Data18Period").datasets = [
'data18_13TeV.periodB.physics_Main.PhysCont.DAOD_STDM4.grp23_v01_p3984',
'data18_13TeV.periodC.physics_Main.PhysCont.DAOD_STDM4.grp23_v01_p3984',
'data18_13TeV.periodD.physics_Main.PhysCont.DAOD_STDM4.grp23_v01_p3984',
'data18_13TeV.periodF.physics_Main.PhysCont.DAOD_STDM4.grp23_v01_p3984',
'data18_13TeV.periodI.physics_Main.PhysCont.DAOD_STDM4.grp23_v01_p3984',
'data18_13TeV.periodK.physics_Main.PhysCont.DAOD_STDM4.grp23_v01_p3984',
'data18_13TeV.periodL.physics_Main.PhysCont.DAOD_STDM4.grp23_v01_p3984',
'data18_13TeV.periodM.physics_Main.PhysCont.DAOD_STDM4.grp23_v01_p3984',
'data18_13TeV.periodO.physics_Main.PhysCont.DAOD_STDM4.grp23_v01_p3984',
'data18_13TeV.periodQ.physics_Main.PhysCont.DAOD_STDM4.grp23_v01_p3984'
]

TopExamples.grid.Add("DYee_mc16a_Old").datasets = [
'mc16_13TeV.301000.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_120M180.deriv.DAOD_STDM4.e3649_e5984_s3126_r9364_r9315_p3731',  
'mc16_13TeV.301001.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_180M250.deriv.DAOD_STDM4.e3649_s3126_r9364_r9315_p3731',        
'mc16_13TeV.301002.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_250M400.deriv.DAOD_STDM4.e3649_s3126_r9364_r9315_p3731',               
'mc16_13TeV.301003.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_400M600.deriv.DAOD_STDM4.e3649_s3126_r9364_p3731',              
'mc16_13TeV.301004.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_600M800.deriv.DAOD_STDM4.e3649_s3126_r9364_r9315_p3731',        
'mc16_13TeV.301005.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_800M1000.deriv.DAOD_STDM4.e3649_s3126_r9364_r9315_p3731',              
'mc16_13TeV.301006.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_1000M1250.deriv.DAOD_STDM4.e3649_s3126_r9364_p3731',            
'mc16_13TeV.301007.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_1250M1500.deriv.DAOD_STDM4.e3649_s3126_r9364_p3731',            
'mc16_13TeV.301008.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_1500M1750.deriv.DAOD_STDM4.e3649_s3126_r9364_r9315_p3731',      
'mc16_13TeV.301009.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_1750M2000.deriv.DAOD_STDM4.e3649_s3126_r9364_p3731',
'mc16_13TeV.301010.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_2000M2250.deriv.DAOD_STDM4.e3649_s3126_r9364_r9315_p3731',             
'mc16_13TeV.301011.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_2250M2500.deriv.DAOD_STDM4.e3649_s3126_r9364_r9315_p3731',             
'mc16_13TeV.301012.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_2500M2750.deriv.DAOD_STDM4.e3649_s3126_r9364_r9315_p3731',             
'mc16_13TeV.301013.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_2750M3000.deriv.DAOD_STDM4.e3649_s3126_r9364_r9315_p3731',             
'mc16_13TeV.301014.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_3000M3500.deriv.DAOD_STDM4.e3649_s3126_r9364_p3731',            
'mc16_13TeV.301015.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_3500M4000.deriv.DAOD_STDM4.e3649_s3126_r9364_r9315_p3731',      
'mc16_13TeV.301016.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_4000M4500.deriv.DAOD_STDM4.e3649_s3126_r9364_r9315_p3731',      
'mc16_13TeV.301017.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_4500M5000.deriv.DAOD_STDM4.e3649_s3126_r9364_r9315_p3731',             
'mc16_13TeV.301018.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_5000M.deriv.DAOD_STDM4.e3649_s3126_r9364_p3731'
      ]


TopExamples.grid.Add("DYee_mc16d_Old").datasets = [
'mc16_13TeV.301000.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_120M180.deriv.DAOD_STDM4.e3649_s3126_r10201_p3731',                
'mc16_13TeV.301001.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_180M250.deriv.DAOD_STDM4.e3649_e5984_s3126_r10201_r10210_p3731',   
'mc16_13TeV.301002.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_250M400.deriv.DAOD_STDM4.e3649_s3126_r10201_p3731',                
'mc16_13TeV.301003.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_400M600.deriv.DAOD_STDM4.e3649_e5984_s3126_r10201_r10210_p3731',   
'mc16_13TeV.301004.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_600M800.deriv.DAOD_STDM4.e3649_s3126_r10201_r10210_p3731',         
'mc16_13TeV.301005.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_800M1000.deriv.DAOD_STDM4.e3649_e5984_s3126_r10201_r10210_p3731',  
'mc16_13TeV.301006.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_1000M1250.deriv.DAOD_STDM4.e3649_s3126_r10201_r10210_p3731',       
'mc16_13TeV.301007.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_1250M1500.deriv.DAOD_STDM4.e3649_e5984_s3126_r10201_r10210_p3731',        
'mc16_13TeV.301008.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_1500M1750.deriv.DAOD_STDM4.e3649_s3126_r10201_r10210_p3731',              
'mc16_13TeV.301009.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_1750M2000.deriv.DAOD_STDM4.e3649_e5984_s3126_r10201_r10210_p3731', 
'mc16_13TeV.301010.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_2000M2250.deriv.DAOD_STDM4.e3649_s3126_r10201_r10210_p3731',       
'mc16_13TeV.301011.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_2250M2500.deriv.DAOD_STDM4.e3649_s3126_r10201_p3731',              
'mc16_13TeV.301012.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_2500M2750.deriv.DAOD_STDM4.e3649_s3126_r10201_r10210_p3731',              
'mc16_13TeV.301013.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_2750M3000.deriv.DAOD_STDM4.e3649_e5984_s3126_r10201_r10210_p3731',        
'mc16_13TeV.301014.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_3000M3500.deriv.DAOD_STDM4.e3649_e5984_s3126_r10201_r10210_p3731',        
'mc16_13TeV.301015.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_3500M4000.deriv.DAOD_STDM4.e3649_s3126_r10201_r10210_p3731',       
'mc16_13TeV.301016.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_4000M4500.deriv.DAOD_STDM4.e3649_s3126_r10201_r10210_p3731',       
'mc16_13TeV.301017.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_4500M5000.deriv.DAOD_STDM4.e3649_e5984_s3126_r10201_r10210_p3731',        
'mc16_13TeV.301018.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_5000M.deriv.DAOD_STDM4.e3649_s3126_r10201_r10210_p3731'
      ]

TopExamples.grid.Add("DYee_mc16e_Old").datasets = [
'mc16_13TeV.301000.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_120M180.deriv.DAOD_STDM4.e3649_s3126_r10724_p3731',                
'mc16_13TeV.301001.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_180M250.deriv.DAOD_STDM4.e3649_s3126_r10724_p3731',                
'mc16_13TeV.301002.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_250M400.deriv.DAOD_STDM4.e3649_e5984_s3126_r10724_r10726_p3731',   
'mc16_13TeV.301003.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_400M600.deriv.DAOD_STDM4.e3649_e5984_s3126_r10724_r10726_p3731',   
'mc16_13TeV.301004.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_600M800.deriv.DAOD_STDM4.e3649_e5984_s3126_r10724_r10726_p3731',          
'mc16_13TeV.301005.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_800M1000.deriv.DAOD_STDM4.e3649_s3126_r10724_p3731',               
'mc16_13TeV.301006.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_1000M1250.deriv.DAOD_STDM4.e3649_e5984_s3126_r10724_r10726_p3731',        
'mc16_13TeV.301007.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_1250M1500.deriv.DAOD_STDM4.e3649_s3126_r10724_p3731',              
'mc16_13TeV.301008.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_1500M1750.deriv.DAOD_STDM4.e3649_e5984_s3126_r10724_r10726_p3731', 
'mc16_13TeV.301009.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_1750M2000.deriv.DAOD_STDM4.e3649_e5984_s3126_r10724_r10726_p3731', 
'mc16_13TeV.301010.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_2000M2250.deriv.DAOD_STDM4.e3649_e5984_s3126_r10724_r10726_p3731', 
'mc16_13TeV.301011.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_2250M2500.deriv.DAOD_STDM4.e3649_e5984_s3126_r10724_r10726_p3731', 
'mc16_13TeV.301012.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_2500M2750.deriv.DAOD_STDM4.e3649_s3126_r10724_p3731',              
'mc16_13TeV.301013.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_2750M3000.deriv.DAOD_STDM4.e3649_e5984_s3126_r10724_r10726_p3731', 
'mc16_13TeV.301014.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_3000M3500.deriv.DAOD_STDM4.e3649_e5984_s3126_r10724_r10726_p3731',        
'mc16_13TeV.301015.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_3500M4000.deriv.DAOD_STDM4.e3649_e5984_s3126_r10724_r10726_p3731', 
'mc16_13TeV.301016.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_4000M4500.deriv.DAOD_STDM4.e3649_e5984_s3126_r10724_r10726_p3731', 
'mc16_13TeV.301017.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_4500M5000.deriv.DAOD_STDM4.e3649_s3126_r10724_p3731',              
'mc16_13TeV.301018.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_5000M.deriv.DAOD_STDM4.e3649_e5984_s3126_r10724_r10726_p3731'
      ]


TopExamples.grid.Add("DYmumu_mc16a_Old").datasets = [
'mc16_13TeV.301020.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_120M180.deriv.DAOD_STDM4.e3649_e5984_s3126_r9364_r9315_p3731',   
'mc16_13TeV.301021.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_180M250.deriv.DAOD_STDM4.e3649_e5984_s3126_r9364_r9315_p3731',   
'mc16_13TeV.301022.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_250M400.deriv.DAOD_STDM4.e3649_s3126_r9364_r9315_p3731',         
'mc16_13TeV.301023.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_400M600.deriv.DAOD_STDM4.e3649_s3126_r9364_r9315_p3731',         
'mc16_13TeV.301024.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_600M800.deriv.DAOD_STDM4.e3649_e5984_s3126_r9364_r9315_p3731',   
'mc16_13TeV.301025.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_800M1000.deriv.DAOD_STDM4.e3649_s3126_r9364_r9315_p3731',        
'mc16_13TeV.301026.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_1000M1250.deriv.DAOD_STDM4.e3649_s3126_r9364_p3731',             
'mc16_13TeV.301027.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_1250M1500.deriv.DAOD_STDM4.e3649_s3126_r9364_r9315_p3731',       
'mc16_13TeV.301028.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_1500M1750.deriv.DAOD_STDM4.e3649_s3126_r9364_p3731',             
'mc16_13TeV.301029.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_1750M2000.deriv.DAOD_STDM4.e3649_s3126_r9364_r9315_p3731',       
'mc16_13TeV.301030.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_2000M2250.deriv.DAOD_STDM4.e3649_e5984_s3126_r9364_r9315_p3731', 
'mc16_13TeV.301031.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_2250M2500.deriv.DAOD_STDM4.e3649_e5984_s3126_r9364_r9315_p3731', 
'mc16_13TeV.301032.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_2500M2750.deriv.DAOD_STDM4.e3649_e5984_s3126_r9364_r9315_p3731', 
'mc16_13TeV.301033.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_2750M3000.deriv.DAOD_STDM4.e3649_s3126_r9364_r9315_p3731',       
'mc16_13TeV.301034.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_3000M3500.deriv.DAOD_STDM4.e3649_s3126_r9364_p3731',             
'mc16_13TeV.301035.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_3500M4000.deriv.DAOD_STDM4.e3649_e5984_s3126_r9364_r9315_p3731', 
'mc16_13TeV.301036.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_4000M4500.deriv.DAOD_STDM4.e3649_s3126_r9364_r9315_p3731',       
'mc16_13TeV.301037.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_4500M5000.deriv.DAOD_STDM4.e3649_e5984_s3126_r9364_r9315_p3731', 
'mc16_13TeV.301038.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_5000M.deriv.DAOD_STDM4.e3649_s3126_r9364_p3731'
      ]


TopExamples.grid.Add("DYmumu_mc16d_Old").datasets = [
'mc16_13TeV.301020.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_120M180.deriv.DAOD_STDM4.e3649_s3126_r10201_p3731',                
'mc16_13TeV.301021.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_180M250.deriv.DAOD_STDM4.e3649_e5984_s3126_r10201_r10210_p3731',   
'mc16_13TeV.301022.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_250M400.deriv.DAOD_STDM4.e3649_s3126_r10201_p3731',                
'mc16_13TeV.301023.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_400M600.deriv.DAOD_STDM4.e3649_s3126_r10201_p3731',                
'mc16_13TeV.301024.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_600M800.deriv.DAOD_STDM4.e3649_s3126_r10201_p3731',                
'mc16_13TeV.301025.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_800M1000.deriv.DAOD_STDM4.e3649_s3126_r10201_p3731',               
'mc16_13TeV.301026.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_1000M1250.deriv.DAOD_STDM4.e3649_e5984_s3126_r10201_r10210_p3731', 
'mc16_13TeV.301027.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_1250M1500.deriv.DAOD_STDM4.e3649_s3126_r10201_p3731',              
'mc16_13TeV.301028.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_1500M1750.deriv.DAOD_STDM4.e3649_e5984_s3126_r10201_r10210_p3731', 
'mc16_13TeV.301029.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_1750M2000.deriv.DAOD_STDM4.e3649_e5984_s3126_r10201_r10210_p3731', 
'mc16_13TeV.301030.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_2000M2250.deriv.DAOD_STDM4.e3649_e5984_s3126_r10201_r10210_p3731', 
'mc16_13TeV.301031.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_2250M2500.deriv.DAOD_STDM4.e3649_s3126_r10201_p3731',              
'mc16_13TeV.301032.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_2500M2750.deriv.DAOD_STDM4.e3649_e5984_s3126_r10201_r10210_p3731', 
'mc16_13TeV.301033.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_2750M3000.deriv.DAOD_STDM4.e3649_s3126_r10201_p3731',              
'mc16_13TeV.301034.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_3000M3500.deriv.DAOD_STDM4.e3649_s3126_r10201_p3731',              
'mc16_13TeV.301035.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_3500M4000.deriv.DAOD_STDM4.e3649_s3126_r10201_p3731',              
'mc16_13TeV.301036.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_4000M4500.deriv.DAOD_STDM4.e3649_s3126_r10201_p3731',              
'mc16_13TeV.301037.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_4500M5000.deriv.DAOD_STDM4.e3649_s3126_r10201_p3731',              
'mc16_13TeV.301038.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_5000M.deriv.DAOD_STDM4.e3649_s3126_r10201_p3731'
      ]

TopExamples.grid.Add("DYmumu_mc16e_Old").datasets = [
'mc16_13TeV.301020.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_120M180.deriv.DAOD_STDM4.e3649_s3126_r10724_p3731',                
'mc16_13TeV.301021.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_180M250.deriv.DAOD_STDM4.e3649_e5984_s3126_r10724_r10726_p3731',   
'mc16_13TeV.301022.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_250M400.deriv.DAOD_STDM4.e3649_e5984_s3126_r10724_r10726_p3731',          
'mc16_13TeV.301023.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_400M600.deriv.DAOD_STDM4.e3649_s3126_r10724_p3731',                
'mc16_13TeV.301024.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_600M800.deriv.DAOD_STDM4.e3649_e5984_s3126_r10724_r10726_p3731',          
'mc16_13TeV.301025.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_800M1000.deriv.DAOD_STDM4.e3649_s3126_r10724_p3731',               
'mc16_13TeV.301026.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_1000M1250.deriv.DAOD_STDM4.e3649_e5984_s3126_r10724_r10726_p3731',        
'mc16_13TeV.301027.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_1250M1500.deriv.DAOD_STDM4.e3649_s3126_r10724_p3731',              
'mc16_13TeV.301028.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_1500M1750.deriv.DAOD_STDM4.e3649_s3126_r10724_p3731',              
'mc16_13TeV.301029.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_1750M2000.deriv.DAOD_STDM4.e3649_s3126_r10724_p3731',              
'mc16_13TeV.301030.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_2000M2250.deriv.DAOD_STDM4.e3649_s3126_r10724_p3731',              
'mc16_13TeV.301031.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_2250M2500.deriv.DAOD_STDM4.e3649_e5984_s3126_r10724_r10726_p3731',        
'mc16_13TeV.301032.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_2500M2750.deriv.DAOD_STDM4.e3649_e5984_s3126_r10724_r10726_p3731',        
'mc16_13TeV.301033.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_2750M3000.deriv.DAOD_STDM4.e3649_s3126_r10724_p3731',              
'mc16_13TeV.301034.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_3000M3500.deriv.DAOD_STDM4.e3649_e5984_s3126_r10724_r10726_p3731', 
'mc16_13TeV.301035.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_3500M4000.deriv.DAOD_STDM4.e3649_s3126_r10724_p3731',              
'mc16_13TeV.301036.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_4000M4500.deriv.DAOD_STDM4.e3649_s3126_r10724_p3731',              
'mc16_13TeV.301037.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_4500M5000.deriv.DAOD_STDM4.e3649_e5984_s3126_r10724_r10726_p3731', 
'mc16_13TeV.301038.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_5000M.deriv.DAOD_STDM4.e3649_e5984_s3126_r10724_r10726_p3731'
      ]
TopExamples.grid.Add("SherpaDY_mc16a").datasets = [
'mc16_13TeV.364100.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_CVetoBVeto.deriv.DAOD_EXOT0.e5271_s3126_r9364_r9315_p4097',                    
'mc16_13TeV.364101.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_CFilterBVeto.deriv.DAOD_EXOT0.e5271_s3126_r9364_p4097',                        
'mc16_13TeV.364102.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_BFilter.deriv.DAOD_EXOT0.e5271_s3126_r9364_p4097',                             
'mc16_13TeV.364103.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_EXOT0.e5271_s3126_r9364_r9315_p4097',                  
'mc16_13TeV.364104.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_CFilterBVeto.deriv.DAOD_EXOT0.e5271_s3126_r9364_p4097',                      
'mc16_13TeV.364105.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_BFilter.deriv.DAOD_EXOT0.e5271_s3126_r9364_r9315_p4097',                     
'mc16_13TeV.364106.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_CVetoBVeto.deriv.DAOD_EXOT0.e5271_s3126_r9364_p4097',                       
'mc16_13TeV.364107.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_CFilterBVeto.deriv.DAOD_EXOT0.e5271_s3126_r9364_p4097',                     
'mc16_13TeV.364108.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_BFilter.deriv.DAOD_EXOT0.e5271_s3126_r9364_r9315_p4097',                    
'mc16_13TeV.364109.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_CVetoBVeto.deriv.DAOD_EXOT0.e5271_s3126_r9364_p4097',                       
'mc16_13TeV.364110.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_CFilterBVeto.deriv.DAOD_EXOT0.e5271_s3126_r9364_r9315_p4097',               
'mc16_13TeV.364111.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_BFilter.deriv.DAOD_EXOT0.e5271_s3126_r9364_p4097',                          
'mc16_13TeV.364112.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV500_1000.deriv.DAOD_EXOT0.e5271_s3126_r9364_p4097',     
'mc16_13TeV.364113.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV1000_E_CMS.deriv.DAOD_EXOT0.e5271_s3126_r9364_p4097',         
'mc16_13TeV.366300.Sh_221_NN30NNLO_Zmumu_MAXHTPTV0_70_Mll100_CVetoBVeto.deriv.DAOD_EXOT0.e7410_e5984_s3126_r9364_r9315_p4097',        
'mc16_13TeV.366301.Sh_221_NN30NNLO_Zmumu_MAXHTPTV0_70_Mll100_CFilterBVeto.deriv.DAOD_EXOT0.e7410_s3126_r9364_p4097',                  
'mc16_13TeV.366302.Sh_221_NN30NNLO_Zmumu_MAXHTPTV0_70_Mll100_BFilter.deriv.DAOD_EXOT0.e7729_s3126_r9364_p4097',                       
'mc16_13TeV.366303.Sh_221_NN30NNLO_Zmumu_MAXHTPTV70_140_Mll100_CVetoBVeto.deriv.DAOD_EXOT0.e7410_s3126_r9364_p4097',                  
'mc16_13TeV.366304.Sh_221_NN30NNLO_Zmumu_MAXHTPTV70_140_Mll100_CFilterBVeto.deriv.DAOD_EXOT0.e7410_s3126_r9364_p4097',                
'mc16_13TeV.366305.Sh_221_NN30NNLO_Zmumu_MAXHTPTV70_140_Mll100_BFilter.deriv.DAOD_EXOT0.e7729_e5984_s3126_r9364_r9315_p4097',         
'mc16_13TeV.366306.Sh_221_NN30NNLO_Zmumu_MAXHTPTV140_280_Mll100_CVetoBVeto.deriv.DAOD_EXOT0.e7410_e5984_s3126_r9364_r9315_p4097',     
'mc16_13TeV.366307.Sh_221_NN30NNLO_Zmumu_MAXHTPTV140_280_Mll100_CFilterBVeto.deriv.DAOD_EXOT0.e7729_e5984_s3126_r9364_r9315_p4097',   
'mc16_13TeV.366308.Sh_221_NN30NNLO_Zmumu_MAXHTPTV140_280_Mll100_BFilter.deriv.DAOD_EXOT0.e7729_s3126_r9364_p4097',        
'mc16_13TeV.364114.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV0_70_CVetoBVeto.deriv.DAOD_EXOT0.e5299_s3126_r9364_r9315_p4097',                   
'mc16_13TeV.364115.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV0_70_CFilterBVeto.deriv.DAOD_EXOT0.e5299_s3126_r9364_p4097',                       
'mc16_13TeV.364116.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV0_70_BFilter.deriv.DAOD_EXOT0.e5299_s3126_r9364_p4097',                            
'mc16_13TeV.364117.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_EXOT0.e5299_s3126_r9364_p4097',                       
'mc16_13TeV.364118.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV70_140_CFilterBVeto.deriv.DAOD_EXOT0.e5299_s3126_r9364_p4097',                     
'mc16_13TeV.364119.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV70_140_BFilter.deriv.DAOD_EXOT0.e5299_s3126_r9364_p4097',                          
'mc16_13TeV.364120.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV140_280_CVetoBVeto.deriv.DAOD_EXOT0.e5299_s3126_r9364_r9315_p4097',                
'mc16_13TeV.364121.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV140_280_CFilterBVeto.deriv.DAOD_EXOT0.e5299_s3126_r9364_r9315_p4097',              
'mc16_13TeV.364122.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV140_280_BFilter.deriv.DAOD_EXOT0.e5299_s3126_r9364_r9315_p4097',                   
'mc16_13TeV.364123.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV280_500_CVetoBVeto.deriv.DAOD_EXOT0.e5299_s3126_r9364_p4097',                      
'mc16_13TeV.364124.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV280_500_CFilterBVeto.deriv.DAOD_EXOT0.e5299_s3126_r9364_r9315_p4097',              
'mc16_13TeV.364125.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV280_500_BFilter.deriv.DAOD_EXOT0.e5299_e5984_s3126_r9364_r9315_p4097',             
'mc16_13TeV.364126.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV500_1000.deriv.DAOD_EXOT0.e5299_s3126_r9364_r9315_p4097',                          
'mc16_13TeV.364127.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV1000_E_CMS.deriv.DAOD_EXOT0.e5299_s3126_r9364_p4097',     
'mc16_13TeV.366309.Sh_221_NN30NNLO_Zee_MAXHTPTV0_70_Mll100_CVetoBVeto.deriv.DAOD_EXOT0.e7411_a875_r9364_p4097',                   
'mc16_13TeV.366310.Sh_221_NN30NNLO_Zee_MAXHTPTV0_70_Mll100_CFilterBVeto.deriv.DAOD_EXOT0.e7411_e5984_a875_r9364_r9315_p4097',     
'mc16_13TeV.366312.Sh_221_NN30NNLO_Zee_MAXHTPTV70_140_Mll100_CVetoBVeto.deriv.DAOD_EXOT0.e7411_a875_r9364_p4097',                 
'mc16_13TeV.366313.Sh_221_NN30NNLO_Zee_MAXHTPTV70_140_Mll100_CFilterBVeto.deriv.DAOD_EXOT0.e7411_a875_r9364_p4097',               
'mc16_13TeV.366315.Sh_221_NN30NNLO_Zee_MAXHTPTV140_280_Mll100_CVetoBVeto.deriv.DAOD_EXOT0.e7411_e5984_a875_r9364_r9315_p4097'               
] #end of if SherpaDYmc16a

TopExamples.grid.Add("SherpaDY_mc16d").datasets = [
'mc16_13TeV.364100.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_CVetoBVeto.deriv.DAOD_EXOT0.e5271_s3126_r10201_p4097',                         
'mc16_13TeV.364101.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_CFilterBVeto.deriv.DAOD_EXOT0.e5271_s3126_r10201_r10210_p4097',                
'mc16_13TeV.364102.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_BFilter.deriv.DAOD_EXOT0.e5271_s3126_r10201_r10210_p4097',                     
'mc16_13TeV.364103.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_EXOT0.e5271_s3126_r10201_r10210_p4097',                
'mc16_13TeV.364104.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_CFilterBVeto.deriv.DAOD_EXOT0.e5271_s3126_r10201_r10210_p4097',              
'mc16_13TeV.364105.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_BFilter.deriv.DAOD_EXOT0.e5271_s3126_r10201_p4097',                          
'mc16_13TeV.364106.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_CVetoBVeto.deriv.DAOD_EXOT0.e5271_s3126_r10201_r10210_p4097',               
'mc16_13TeV.364107.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_CFilterBVeto.deriv.DAOD_EXOT0.e5271_s3126_r10201_r10210_p4097',             
'mc16_13TeV.364108.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_BFilter.deriv.DAOD_EXOT0.e5271_s3126_r10201_r10210_p4097',                  
'mc16_13TeV.364109.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_CVetoBVeto.deriv.DAOD_EXOT0.e5271_s3126_r10201_r10210_p4097',               
'mc16_13TeV.364110.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_CFilterBVeto.deriv.DAOD_EXOT0.e5271_s3126_r10201_p4097',                    
'mc16_13TeV.364111.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_BFilter.deriv.DAOD_EXOT0.e5271_s3126_r10201_p4097',                         
'mc16_13TeV.364112.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV500_1000.deriv.DAOD_EXOT0.e5271_e5984_s3126_r10201_r10210_p4097',                   
'mc16_13TeV.364113.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV1000_E_CMS.deriv.DAOD_EXOT0.e5271_s3126_r10201_p4097',  
'mc16_13TeV.366300.Sh_221_NN30NNLO_Zmumu_MAXHTPTV0_70_Mll100_CVetoBVeto.deriv.DAOD_EXOT0.e7410_s3126_r10201_p4097',                   
'mc16_13TeV.366301.Sh_221_NN30NNLO_Zmumu_MAXHTPTV0_70_Mll100_CFilterBVeto.deriv.DAOD_EXOT0.e7410_e5984_s3126_r10201_r10210_p4097',    
'mc16_13TeV.366302.Sh_221_NN30NNLO_Zmumu_MAXHTPTV0_70_Mll100_BFilter.deriv.DAOD_EXOT0.e7729_s3126_r10201_p4097',                      
'mc16_13TeV.366303.Sh_221_NN30NNLO_Zmumu_MAXHTPTV70_140_Mll100_CVetoBVeto.deriv.DAOD_EXOT0.e7410_e5984_s3126_r10201_r10210_p4097',    
'mc16_13TeV.366304.Sh_221_NN30NNLO_Zmumu_MAXHTPTV70_140_Mll100_CFilterBVeto.deriv.DAOD_EXOT0.e7410_e5984_s3126_r10201_r10210_p4097',  
'mc16_13TeV.366305.Sh_221_NN30NNLO_Zmumu_MAXHTPTV70_140_Mll100_BFilter.deriv.DAOD_EXOT0.e7729_e5984_s3126_r10201_r10210_p4097',       
'mc16_13TeV.366306.Sh_221_NN30NNLO_Zmumu_MAXHTPTV140_280_Mll100_CVetoBVeto.deriv.DAOD_EXOT0.e7410_e5984_s3126_r10201_r10210_p4097',   
'mc16_13TeV.366307.Sh_221_NN30NNLO_Zmumu_MAXHTPTV140_280_Mll100_CFilterBVeto.deriv.DAOD_EXOT0.e7729_e5984_s3126_r10201_r10210_p4097', 
'mc16_13TeV.366308.Sh_221_NN30NNLO_Zmumu_MAXHTPTV140_280_Mll100_BFilter.deriv.DAOD_EXOT0.e7729_e5984_s3126_r10201_r10210_p4097',                                
'mc16_13TeV.364114.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV0_70_CVetoBVeto.deriv.DAOD_EXOT0.e5299_s3126_r10201_r10210_p4097',                 
'mc16_13TeV.364115.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV0_70_CFilterBVeto.deriv.DAOD_EXOT0.e5299_s3126_r10201_p4097',                      
'mc16_13TeV.364116.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV0_70_BFilter.deriv.DAOD_EXOT0.e5299_s3126_r10201_p4097',                           
'mc16_13TeV.364117.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_EXOT0.e5299_e5984_s3126_r10201_r10210_p4097',         
'mc16_13TeV.364118.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV70_140_CFilterBVeto.deriv.DAOD_EXOT0.e5299_s3126_r10201_p4097',                    
'mc16_13TeV.364119.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV70_140_BFilter.deriv.DAOD_EXOT0.e5299_s3126_r10201_p4097',                         
'mc16_13TeV.364120.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV140_280_CVetoBVeto.deriv.DAOD_EXOT0.e5299_s3126_r10201_p4097',                     
'mc16_13TeV.364121.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV140_280_CFilterBVeto.deriv.DAOD_EXOT0.e5299_e5984_s3126_r10201_r10210_p4097',      
'mc16_13TeV.364122.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV140_280_BFilter.deriv.DAOD_EXOT0.e5299_s3126_r10201_p4097',                        
'mc16_13TeV.364123.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV280_500_CVetoBVeto.deriv.DAOD_EXOT0.e5299_s3126_r10201_p4097',                     
'mc16_13TeV.364124.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV280_500_CFilterBVeto.deriv.DAOD_EXOT0.e5299_s3126_r10201_p4097',                   
'mc16_13TeV.364125.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV280_500_BFilter.deriv.DAOD_EXOT0.e5299_e5984_s3126_r10201_r10210_p4097',           
'mc16_13TeV.364126.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV500_1000.deriv.DAOD_EXOT0.e5299_e5984_s3126_r10201_r10210_p4097',                  
'mc16_13TeV.364127.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV1000_E_CMS.deriv.DAOD_EXOT0.e5299_s3126_r10201_p4097',
'mc16_13TeV.366309.Sh_221_NN30NNLO_Zee_MAXHTPTV0_70_Mll100_CVetoBVeto.deriv.DAOD_EXOT0.e7411_a875_r10201_p4097',                   
'mc16_13TeV.366310.Sh_221_NN30NNLO_Zee_MAXHTPTV0_70_Mll100_CFilterBVeto.deriv.DAOD_EXOT0.e7411_a875_r10201_p4097',                
'mc16_13TeV.366312.Sh_221_NN30NNLO_Zee_MAXHTPTV70_140_Mll100_CVetoBVeto.deriv.DAOD_EXOT0.e7411_e5984_a875_r10201_r10210_p4097',   
'mc16_13TeV.366313.Sh_221_NN30NNLO_Zee_MAXHTPTV70_140_Mll100_CFilterBVeto.deriv.DAOD_EXOT0.e7411_a875_r10201_p4097',              
'mc16_13TeV.366315.Sh_221_NN30NNLO_Zee_MAXHTPTV140_280_Mll100_CVetoBVeto.deriv.DAOD_EXOT0.e7411_a875_r10201_p4097'               
] #end of if SherpaDYmc16d

TopExamples.grid.Add("SherpaDY_mc16e").datasets = [
'mc16_13TeV.364100.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_CVetoBVeto.deriv.DAOD_EXOT0.e5271_s3126_r10724_p4097',                         
'mc16_13TeV.364101.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_CFilterBVeto.deriv.DAOD_EXOT0.e5271_e5984_s3126_r10724_r10726_p4097',          
'mc16_13TeV.364102.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_BFilter.deriv.DAOD_EXOT0.e5271_s3126_r10724_p4097',                            
'mc16_13TeV.364103.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_EXOT0.e5271_s3126_r10724_p4097',                       
'mc16_13TeV.364104.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_CFilterBVeto.deriv.DAOD_EXOT0.e5271_e5984_s3126_r10724_r10726_p4097',        
'mc16_13TeV.364105.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_BFilter.deriv.DAOD_EXOT0.e5271_s3126_r10724_p4097',                          
'mc16_13TeV.364106.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_CVetoBVeto.deriv.DAOD_EXOT0.e5271_s3126_r10724_p4097',                      
'mc16_13TeV.364107.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_CFilterBVeto.deriv.DAOD_EXOT0.e5271_s3126_r10201_p4097',                    
'mc16_13TeV.364108.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_BFilter.deriv.DAOD_EXOT0.e5271_s3126_r10724_p4097',                         
'mc16_13TeV.364109.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_CVetoBVeto.deriv.DAOD_EXOT0.e5271_s3126_r10724_p4097',                      
'mc16_13TeV.364110.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_CFilterBVeto.deriv.DAOD_EXOT0.e5271_s3126_r10724_p4097',                    
'mc16_13TeV.364111.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_BFilter.deriv.DAOD_EXOT0.e5271_e5984_s3126_r10724_r10726_p4097',            
'mc16_13TeV.364112.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV500_1000.deriv.DAOD_EXOT0.e5271_s3126_r10201_r10210_p4097',                         
'mc16_13TeV.364113.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV1000_E_CMS.deriv.DAOD_EXOT0.e5271_e5984_s3126_r10724_r10726_p4097',   
'mc16_13TeV.366300.Sh_221_NN30NNLO_Zmumu_MAXHTPTV0_70_Mll100_CVetoBVeto.deriv.DAOD_EXOT0.e7410_s3126_r10724_p4097',                   
'mc16_13TeV.366301.Sh_221_NN30NNLO_Zmumu_MAXHTPTV0_70_Mll100_CFilterBVeto.deriv.DAOD_EXOT0.e7410_s3126_r10724_p4097',                 
'mc16_13TeV.366302.Sh_221_NN30NNLO_Zmumu_MAXHTPTV0_70_Mll100_BFilter.deriv.DAOD_EXOT0.e7729_s3126_r10724_p4097',                      
'mc16_13TeV.366303.Sh_221_NN30NNLO_Zmumu_MAXHTPTV70_140_Mll100_CVetoBVeto.deriv.DAOD_EXOT0.e7410_e5984_s3126_r10724_r10726_p4097',    
'mc16_13TeV.366304.Sh_221_NN30NNLO_Zmumu_MAXHTPTV70_140_Mll100_CFilterBVeto.deriv.DAOD_EXOT0.e7410_e5984_s3126_r10724_r10726_p4097',  
'mc16_13TeV.366305.Sh_221_NN30NNLO_Zmumu_MAXHTPTV70_140_Mll100_BFilter.deriv.DAOD_EXOT0.e7729_e5984_s3126_r10724_r10726_p4097',       
'mc16_13TeV.366306.Sh_221_NN30NNLO_Zmumu_MAXHTPTV140_280_Mll100_CVetoBVeto.deriv.DAOD_EXOT0.e7410_s3126_r10724_p4097',                
'mc16_13TeV.366307.Sh_221_NN30NNLO_Zmumu_MAXHTPTV140_280_Mll100_CFilterBVeto.deriv.DAOD_EXOT0.e7729_s3126_r10724_p4097',                               
'mc16_13TeV.366308.Sh_221_NN30NNLO_Zmumu_MAXHTPTV140_280_Mll100_BFilter.deriv.DAOD_EXOT0.e7729_s3126_r10724_p4097',    
'mc16_13TeV.364114.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV0_70_CVetoBVeto.deriv.DAOD_EXOT0.e5299_s3126_r10724_p4097',                        
'mc16_13TeV.364115.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV0_70_CFilterBVeto.deriv.DAOD_EXOT0.e5299_e5984_s3126_s3136_r10724_r10726_p4097',   
'mc16_13TeV.364116.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV0_70_BFilter.deriv.DAOD_EXOT0.e5299_s3126_r10724_p4097',                           
'mc16_13TeV.364117.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_EXOT0.e5299_e5984_s3126_r10724_r10726_p4097',         
'mc16_13TeV.364118.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV70_140_CFilterBVeto.deriv.DAOD_EXOT0.e5299_e5984_s3126_r10724_r10726_p4097',       
'mc16_13TeV.364119.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV70_140_BFilter.deriv.DAOD_EXOT0.e5299_s3126_r10724_p4097',                         
'mc16_13TeV.364120.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV140_280_CVetoBVeto.deriv.DAOD_EXOT0.e5299_e5984_s3126_r10724_r10726_p4097',        
'mc16_13TeV.364121.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV140_280_CFilterBVeto.deriv.DAOD_EXOT0.e5299_s3126_r10724_p4097',                   
'mc16_13TeV.364122.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV140_280_BFilter.deriv.DAOD_EXOT0.e5299_e5984_s3126_r10724_r10726_p4097',           
'mc16_13TeV.364123.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV280_500_CVetoBVeto.deriv.DAOD_EXOT0.e5299_e5984_s3126_r10724_r10726_p4097',        
'mc16_13TeV.364124.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV280_500_CFilterBVeto.deriv.DAOD_EXOT0.e5299_s3126_r10724_p4097',                   
'mc16_13TeV.364125.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV280_500_BFilter.deriv.DAOD_EXOT0.e5299_e5984_s3126_s3136_r10724_r10726_p4097',     
'mc16_13TeV.364126.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV500_1000.deriv.DAOD_EXOT0.e5299_s3126_r10724_p4097',                               
'mc16_13TeV.364127.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV1000_E_CMS.deriv.DAOD_EXOT0.e5299_e5984_s3126_r10201_r10210_p4097',                
'mc16_13TeV.366309.Sh_221_NN30NNLO_Zee_MAXHTPTV0_70_Mll100_CVetoBVeto.deriv.DAOD_EXOT0.e7411_e5984_a875_r10724_r10726_p4097',     
'mc16_13TeV.366310.Sh_221_NN30NNLO_Zee_MAXHTPTV0_70_Mll100_CFilterBVeto.deriv.DAOD_EXOT0.e7411_e5984_a875_r10724_r10726_p4097',   
'mc16_13TeV.366312.Sh_221_NN30NNLO_Zee_MAXHTPTV70_140_Mll100_CVetoBVeto.deriv.DAOD_EXOT0.e7411_e5984_a875_r10724_r10726_p4097',   
'mc16_13TeV.366313.Sh_221_NN30NNLO_Zee_MAXHTPTV70_140_Mll100_CFilterBVeto.deriv.DAOD_EXOT0.e7411_a875_r10724_p4097',              
'mc16_13TeV.366315.Sh_221_NN30NNLO_Zee_MAXHTPTV140_280_Mll100_CVetoBVeto.deriv.DAOD_EXOT0.e7411_a875_r10724_p4097'      
] #end of if SherpaDYmc16e

