#!/usr/bin/env python

# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
import TopExamples.grid
#import DerivationTags
import Data17
import MC16_TOPQ1

config = TopExamples.grid.Config()
config.code          = 'top-xaod'
config.settingsFile  = 'dil-cuts_Bkg_mc16e.txt'                                                                    
config.gridUsername  = 'mcanobre'
config.suffix        = '21_2_111Ret'
#config.excludedSites = 'ANALY_AGLT2_SL7-condor,CERN_central_ACTA,ANALY_FZK'
config.noSubmit      = False # set to True if you just want to test the submission
config.CMake         = True # need to set to True for CMake-based releases (release 21)
config.mergeType     = 'Default' #'None', 'Default' or 'xAOD'
#config.destSE        = '' #This is the default (anywhere), or try e.g. 'UKI-SOUTHGRID-BHAM-HEP_LOCAL
#config.maxFileSize   = '19157786'
#config.memory  	     = '1800' 
#config.maxNFilesPerJob = '1'

#config.nFilesPerJob  = '1'
#config.nJobs	     = '1' 
###Data - look in Data_rel21.py
###MC Simulation - look in MC16_TOPQ1.py
###Using a few test samples produced with release 21
###Edit these lines if you don't want to run everything!

names = [
       #'SherpaDY_mc16d'
       'TopQuark_mc16e'
       #'Ztomumu_mc16a',"Ztoee_mc16a"
       #,'DYmumu_mc16e','PI_mumu_mc16e', 'Ztoee_mc16e','DYee_mc16e','PI_ee_mc16e'
       #'Diboson_mc16e_Unbinned_2',
       #'Diboson_mc16e_Unbinned_1'
       #'Wjets_mc16a','Ztotautau_mc16a'
       #'Diboson_mc16d_Unbinned'#,'TopQuark_mc16e'
       #'Data18Period'
       #'ttbar_mc16a'
       #'PI_mumu_mc16a',
       #'Ztomumu_mc16a','Ztoee_mc16a' 
       #'TopQuark_mc16a' 
       #,'Diboson','Ztotautau','WJets'
       #'Ztotautau_mc16a'
       #'TopQuark_mc16e','Diboson_mc16e','Wjets_mc16e' 
       #'Ztoee_mc16e','Ztomumu_mc16e' 
       #'Ztomumu','DYmumu',
       #'Ztomumu','DYmumu' 
       #'Ztoee'#,'TopQuark','Ztomumu'
       #'RetryDYmumu'
       #'EGAM8Data'
       #'TopQuark_mc16a'
       #'Data15'
       #'TopQuark','Diboson','Ztotautau'#,'Diboson_mc16d','Ztotautau' 
       #'Diboson'	 
       #'Data15'#,
       #'Data15Period'#,'Data16','Data17','Data18'
       #'TopQuark_mc16a'#,
       #'EGAM8Data' 
       #'Ztomumu'
       #'WJets'#,'Diboson_mc16d'
       #'Data18'#,'Data16'
]

samples = TopExamples.grid.Samples(names)
TopExamples.grid.submit(config, samples)

