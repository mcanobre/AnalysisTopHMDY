#ifndef MYWLNUSELECTION_CUSTOMEVENTSAVER_H
#define MYWLNUSELECTION_CUSTOMEVENTSAVER_H

#include "TopObjectSelectionTools/FwdElectronMC15.h"
#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"
#include "LPXKfactorTool/LPXKfactorTool.h"
#include "TopAnalysis/EventSaverFlatNtuple.h"
#include "TopEventSelectionTools/PlotManager.h"
//#include "TopObjectSelectionTools/MuonMC15.h"
#include "TopEvent/EventTools.h"
#include "MuonSelectorTools/MuonSelectionTool.h"
#include "ElectronPhotonSelectorTools/AsgForwardElectronLikelihoodTool.h"
#include "IsolationSelection/IIsolationSelectionTool.h"
#include "IsolationCorrections/IIsolationCorrectionTool.h"
#include "ElectronPhotonFourMomentumCorrection/egammaEnergyCorrectionTool.h"
#include "ElectronPhotonFourMomentumCorrection/EgammaCalibrationAndSmearingTool.h"
#include "ElectronEfficiencyCorrection/AsgElectronEfficiencyCorrectionTool.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODEgamma/Egamma.h"
#include "xAODEgamma/EgammaxAODHelpers.h"
#include "xAODCaloEvent/CaloCluster.h"
#include "xAODCore/ShallowCopy.h"
#include "PATInterfaces/SystematicsUtil.h"
#include "TrigBunchCrossingTool/WebBunchCrossingTool.h"
#include "TrigBunchCrossingTool/StaticBunchCrossingTool.h"

/**
 * This class shows you how to extend the flat ntuple to include your own variables
 * 
 * It inherits from top::EventSaverFlatNtuple, which will be doing all the hard work 
 * 
 */

namespace top{
    class CustomEventSaver : public top::EventSaverFlatNtuple {
        public:
            ///-- Default constrcutor with no arguments - needed for ROOT --///
            CustomEventSaver();
            ///-- Destructor does nothing --///
            virtual ~CustomEventSaver(){}

            ///-- initialize function for top::EventSaverFlatNtuple --///
            ///-- We will be setting up out custom variables here --///
            virtual void initialize(std::shared_ptr<top::TopConfig> config, TFile* file, const std::vector<std::string>& extraBranches) override;

            ///-- Keep the asg::AsgTool happy --///
            virtual StatusCode initialize(){return StatusCode::SUCCESS;}      

            ///-- saveEvent function for top::EventSaverFlatNtuple --///
            ///-- We will be setting our custom variables on a per-event basis --///
            virtual void saveEvent(const top::Event& event) override;
            virtual void saveTruthEvent();
	    int isBranchStored(top::TreeManager const *treeManager, const std::string &variableName);

        private:
            ///-- Some additional custom variables for the output --///
            //float m_randomNumber;
            //float m_someOtherVariable;
            ///-- Hadronic Recoil variables --///
            //TLorentzVector m_PFOHR;
            //double m_sumET_PFOHR;
            //hadrecoil *m_hadrecoil;

            // Tool for tag event if electron is tight
    	    ToolHandle<AsgForwardElectronLikelihoodTool> m_fwdElectronSelectionTool;
    	    ToolHandle<AsgForwardElectronLikelihoodTool> m_fwdElectronLooseSelectionTool;
	    ToolHandle<CP::IMuonSelectionTool> m_muonSelectionTool;
	    ToolHandle<CP::IMuonSelectionTool> m_muonSelectionToolLoose;
	    ToolHandle<CP::IEgammaCalibrationAndSmearingTool> m_egammaCalibrationAndSmearingTool;
	    ToolHandle<CP::IIsolationSelectionTool> m_isolationTool_FixedCutLoose;
	    ToolHandle<CP::IIsolationSelectionTool> m_isolationTool_FixedCutTight;
    	    ToolHandle<CP::IIsolationSelectionTool> m_isolationTool_FCHighPtCaloOnly;
	    //ToolHandle<CP::IIsolationSelectionTool> m_isolationTool_FixedCutTightTrackOnly;
	    //ToolHandle<CP::IIsolationSelectionTool> m_isolationTool_Gradient;
	    //ToolHandle<CP::IIsolationSelectionTool> m_isolationTool_FixCutHighPtTrackOnly;
	    //ToolHandle<CP::IIsolationSelectionTool> m_isolationTool_FixedCutHighPtCaloOnly;
	    const xAOD::ElectronContainer* m_forward_ele;
	     xAOD::ElectronContainer* fwdelsCorr;
	    const xAOD::TruthParticleContainer* telecs;
	    const xAOD::TruthParticleContainer* tmuons;
	    const xAOD::TruthParticleContainer* tBoson;
            //CP::EgammaCalibrationAndSmearingTool *m_EleCalibTool;
	    std::string m_mapFileNew;
	    //std::string m_idFwdKey1;
	    std::string m_idFwdKey2;
	    std::string m_idFwdKey3;
	    //AsgElectronEfficiencyCorrectionTool *m_idFwdEgCorrections1;
	    AsgElectronEfficiencyCorrectionTool *m_idFwdEgCorrections2;
	    AsgElectronEfficiencyCorrectionTool *m_idFwdEgCorrections3;
            std::vector<bool> el_isElTight;
            std::vector<bool> el_isElMedium;
            std::vector<bool> el_isElLoose;
            CP::MuonSelectionTool *m_muonSelection;
            //CP::MuonSelectionTool *m_muonSelection_HighPt;
            LPXKfactorTool *m_kfactorTool;
            double m_weight_kfactor;
            //double m_weight_kfactor_truth;
	    double m_BornMass;
	    float m_weight_pileup_hash_truth;
	    float m_weight_pileup_hash;
	    AsgForwardElectronLikelihoodTool *m_FwdElectronIDToolLoose;
	    AsgForwardElectronLikelihoodTool *m_FwdElectronIDToolMedium;
	    AsgForwardElectronLikelihoodTool *m_FwdElectronIDToolTight;
	    std::vector<float> mu_PromptLeptonVeto;
	    std::vector<float> el_isPromptLepton;
	    std::vector<float> ForwardEle_PromptLeptonVeto;
	    Trig::WebBunchCrossingTool *bct; 

   	    std::vector<bool> fwdel_isTight;
    	    std::vector<bool> fwdel_isMedium;
    	    std::vector<bool> fwdel_isLoose;

	    std::vector<bool> el_DFCommonElectronsECIDS;
	    std::vector<double> el_DFCommonElectronsECIDSResult;

    	    std::vector<bool> mu_isMedium;
    	    std::vector<bool> mu_isHighPt;
    	    std::vector<float> mu_PtMS;
    	    std::vector<float> mu_PtID;
    	    std::vector<bool> mu_isolation_FixedCutTight;
    	    std::vector<bool> mu_isolation_FixedCutLoose;
    	    std::vector<bool> mu_isolation_FixedCutPflowTight;
    	    std::vector<bool> mu_isolation_FixedCutPflowLoose;
    	    std::vector<bool> mu_isolation_FCTightTrackOnly_FixedRad;
    	    std::vector<bool> el_isolation_FixedCutTight;
    	    std::vector<bool> el_isolation_FixedCutLoose;
    	    std::vector<bool> el_isolation_PflowTight;
    	    std::vector<bool> el_isolation_TightTrackOnly;
    	    std::vector<bool> el_isolation_TightTrackOnly_FixedRad;
    	    std::vector<bool> el_isolation_FCHighPtCaloOnly;
    	    float Lepton_Pt_dressed;
	    float Lepton_Phi_dressed;
	    float Lepton_Eta_dressed;
	    float Lepton_Charge_dressed;
    	    float AntiLepton_Pt_dressed;
	    float AntiLepton_Phi_dressed;
	    float AntiLepton_Eta_dressed;
	    float AntiLepton_Charge_dressed;
  	    std::vector<CP::SystematicSet> m_sysList; 

	    //std::vector<double> ForwardEle_IDSF_Loose;
	    std::vector<double> ForwardEle_IDSF_Medium;
	    std::vector<double> ForwardEle_IDSF_Tight;
    	    //std::vector<bool> ForwardEle_isElLoose;
    	    std::vector<bool> ForwardEle_isElMedium;
    	    std::vector<bool> ForwardEle_isElTight;
    	    std::vector<float> ForwardEle_EtCone20;
    	    std::vector<float> ForwardEle_EtCone30;
    	    std::vector<float> ForwardEle_EtCone40;
    	    std::vector<double> ForwardEle_Iso;
    	    std::vector<double> ForwardEle_Pt;
    	    std::vector<double> ForwardEle_Phi;
    	    std::vector<double> ForwardEle_Eta;

    	    std::vector<int> m_mc_status;
    	    std::vector<int> m_mc_barcode;
    	    float m_mc_Dilepton_Pt_born;
	    float m_mc_Dilepton_Rapidity_born;
	    float m_mc_Dilepton_Phi_born;
	    float m_mc_Dilepton_Mass_born;
	    float m_mc_Dilepton_m_born;
	    float m_mc_Dilepton_Eta_born;
    	    float m_mc_Lepton_Pt_born;
	    float m_mc_AntiLepton_Pt_born;
	    float m_mc_Lepton_Phi_born;
	    float m_mc_AntiLepton_Phi_born;
	    float m_mc_Lepton_Eta_born;
	    float m_mc_AntiLepton_Eta_born;
	    float m_mc_Lepton_m_born;
	    float m_mc_AntiLepton_m_born;
	    float m_mc_Lepton_Pt_bare;
	    float m_mc_AntiLepton_Pt_bare;
	    float m_mc_Lepton_Phi_bare;
	    float m_mc_AntiLepton_Phi_bare;
	    float m_mc_Lepton_Eta_bare;
	    float m_mc_AntiLepton_Eta_bare;
	    float m_mc_Lepton_m_bare;
	    float m_mc_AntiLepton_m_bare; 

	    float m_mc_Quark1_PDGID; 
	    float m_mc_Quark2_PDGID;
	    float m_mc_Quark1_Px; 
	    float m_mc_Quark2_Px;
	    float m_mc_Quark1_Py; 
	    float m_mc_Quark2_Py;
	    float m_mc_Quark1_Pz; 
	    float m_mc_Quark2_Pz;
	    float m_mc_Quark1_E; 
	    float m_mc_Quark2_E;

	    std::shared_ptr<top::TreeManager> m_truthTreeManager;
  	    std::shared_ptr<top::TopConfig> m_config;

            ///-- Tell RootCore to build a dictionary (we need this) --///
            ClassDef(top::CustomEventSaver, 0);
    };
}

#endif
