#include "MyWlnuSelection/M120.h"

namespace top {
	// W or Z born mass cut in 120 GeV
	bool M120::apply(const top::Event& event) const {
		if (event.m_info->eventType(xAOD::EventInfo::IS_SIMULATION)) {
			double m_bornMass = 0;
			for (const auto *truthP : *(event.m_truth)) {
				if ((truthP->isW()) || (truthP->isZ())) {
					m_bornMass = truthP->p4().M();
					break;
				}

			}
			if ((m_bornMass == 0) || (m_bornMass > 120000))
				return false;
		}
	return true;
	}

	//For cutflow and terminal output
	std::string M120::name() const {
		return "M120";
	}
}
