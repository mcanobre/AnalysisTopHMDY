#include "MyWlnuSelection/CustomEventSaver.h"
#include "TopEvent/Event.h"
#include "TopEventSelectionTools/TreeManager.h"

#include <TRandom3.h>

#include <exception>

//#include "TopEvent/Event.h"
//#include "TopEvent/EventTools.h"
//#include "TopEventSelectionTools/TreeManager.h"
//#include "TopParticleLevel/ParticleLevelEvent.h"
#include "TopConfiguration/TopConfig.h"
//#include "TopEventSelectionTools/PlotManager.h"

//#include "TopEvent/TopEventMaker.h"
//#include "TopEvent/Event.h"

#include "TH1.h"
#include "TH2.h"

namespace top {
    ///-- Constrcutor --///

    CustomEventSaver::CustomEventSaver() :
        el_isElTight(0.0),
	fwdel_isTight(0.0),
	fwdel_isMedium(0.0),
	fwdel_isLoose(0.0),
        el_isElMedium(0.0),
        el_isElLoose(0.0),
        el_isPromptLepton(0.0),
	el_isolation_FixedCutLoose(0.0),
	el_isolation_FixedCutTight(0.0),
	el_isolation_PflowTight(0.0),
	el_isolation_TightTrackOnly(0.0),
	el_isolation_TightTrackOnly_FixedRad(0.0),
	el_isolation_FCHighPtCaloOnly(0.0),
	mu_isolation_FixedCutLoose(0.0),
	mu_isolation_FixedCutTight(0.0),
	mu_isolation_FixedCutPflowTight(0.0),
	mu_isolation_FixedCutPflowLoose(0.0),
	mu_isolation_FCTightTrackOnly_FixedRad(0.0),
	mu_isMedium(0.0),
	mu_PtMS(0.0),
	mu_PtID(0.0),	
	mu_isHighPt(0.0),
    	m_mc_Quark2_Px(0.0),
    	m_mc_Quark1_Px(0.0),
    	m_mc_Quark2_Py(0.0),
    	m_mc_Quark1_Py(0.0),
    	m_mc_Quark2_Pz(0.0),
    	m_mc_Quark1_Pz(0.0),
    	m_mc_Quark2_E(0.0),
    	m_mc_Quark1_E(0.0),
    	m_mc_Quark2_PDGID(0.0),
    	m_mc_Quark1_PDGID(0.0),
        m_weight_kfactor(1.0),
    	m_mc_Lepton_Pt_born(0.0),
        m_weight_pileup_hash(0),
        m_weight_pileup_hash_truth(0),
    	m_mc_Dilepton_Eta_born(0.0),
    	m_mc_Dilepton_Pt_born(0.0),
	m_mc_Dilepton_Rapidity_born(0.0),
	m_mc_Dilepton_Phi_born(0.0),
	m_mc_Dilepton_Mass_born(0.0),
	m_mc_Dilepton_m_born(0.0),
	m_mc_AntiLepton_Pt_born(0.0),
	m_mc_Lepton_Phi_born(0.0),
	m_mc_AntiLepton_Phi_born(0.0),
	m_mc_Lepton_Eta_born(0.0),
	m_mc_AntiLepton_Eta_born(0.0),
	m_mc_Lepton_m_born(0.0),
	m_mc_AntiLepton_m_born(0.0),
	m_mc_Lepton_Pt_bare(0.0),
	m_mc_AntiLepton_Pt_bare(0.0),
	m_mc_Lepton_Phi_bare(0.0),
	m_mc_AntiLepton_Phi_bare(0.0),
	m_mc_Lepton_Eta_bare(0.0),
	m_mc_AntiLepton_Eta_bare(0.0),
	m_mc_Lepton_m_bare(0.0),
	m_mc_AntiLepton_m_bare(0.0),
	Lepton_Pt_dressed(0.0),
	Lepton_Eta_dressed(0.0),
	Lepton_Phi_dressed(0.0),
	Lepton_Charge_dressed(0.0),
	AntiLepton_Pt_dressed(0.0),
	AntiLepton_Eta_dressed(0.0),
	AntiLepton_Phi_dressed(0.0),
	AntiLepton_Charge_dressed(0.0),
        el_DFCommonElectronsECIDS(0.0),
        el_DFCommonElectronsECIDSResult(0.0),
	m_isolationTool_FixedCutLoose("CP::IsolationTool_FixedCutLoose"),
	m_isolationTool_FixedCutTight("CP::IsolationTool_FixedCutTight"),
        m_fwdElectronSelectionTool("CP::FwdElectronSelector"),
        m_fwdElectronLooseSelectionTool("CP::FwdElectronSelectorLoose"),
	//m_muonSelection_HighPt("CP::MuonSelectionTool"),
        m_muonSelectionTool("CP::MuonSelectionTool"),
        m_muonSelectionToolLoose("CP::MuonSelectionToolLoose"),
        m_egammaCalibrationAndSmearingTool("CP::EgammaCalibrationAndSmearingTool"),
        m_isolationTool_FCHighPtCaloOnly("CP::IsolationTool_FCHighPtCaloOnly"),
	m_truthTreeManager(nullptr)
	//m_isolationTool_Gradient("CP::IsolationTool_Gradient"),
	//m_isolationTool_FixedCutTightTrackOnly("CP::IsolationTool_FixedCutTightTrackOnly"),
	//m_isolationTool_FixedCutHighPtCaloOnly("CP::IsolationTool_FixedCutHighPtCaloOnly"),
  
    {
        branchFilters().push_back(std::bind(&top::CustomEventSaver::isBranchStored, this, std::placeholders::_1, std::placeholders::_2));
    }
    ///-- initialize - done once at the start of a job before the loop over events --///
    void CustomEventSaver::initialize(std::shared_ptr<top::TopConfig> config, TFile* file, const std::vector<std::string>& extraBranches)
    {
        ///-- Let the base class do all the hard work --///
        ///-- It will setup TTrees for each systematic with a standard set of variables --///
        m_config = config;
        top::EventSaverFlatNtuple::initialize(config, file, extraBranches);

        //if (m_config->isMC()){
	//m_truthTreeManager = EventSaverFlatNtuple::truthTreeManager();
            /*m_truthTreeManager->makeOutputVariable(m_mc_status, "mc_status");
            m_truthTreeManager->makeOutputVariable(m_mc_barcode, "mc_barcode");
            m_truthTreeManager->makeOutputVariable(m_mc_Lepton_Pt, "Lepton_Pt");
            m_truthTreeManager->makeOutputVariable(m_mc_AntiLepton_Pt, "AntiLepton_Pt");
            m_truthTreeManager->makeOutputVariable(m_mc_Lepton_Eta, "Lepton_Eta");
            m_truthTreeManager->makeOutputVariable(m_mc_AntiLepton_Eta, "AntiLepton_Eta");
            m_truthTreeManager->makeOutputVariable(m_mc_Lepton_Phi, "Lepton_Phi");
            m_truthTreeManager->makeOutputVariable(m_mc_AntiLepton_Phi, "AntiLepton_Phi");
            m_truthTreeManager->makeOutputVariable(m_mc_Lepton_m, "Lepton_m");
            m_truthTreeManager->makeOutputVariable(m_mc_AntiLepton_m, "AntiLepton_m");
            m_truthTreeManager->makeOutputVariable(m_mc_Lepton_Pt, "Lepton_Pt_bare");
            m_truthTreeManager->makeOutputVariable(m_mc_AntiLepton_Pt, "AntiLepton_Pt_bare");
            m_truthTreeManager->makeOutputVariable(m_mc_Lepton_Eta, "Lepton_Eta_bare");
            m_truthTreeManager->makeOutputVariable(m_mc_AntiLepton_Eta, "AntiLepton_Eta_bare");
            m_truthTreeManager->makeOutputVariable(m_mc_Lepton_Phi, "Lepton_Phi_bare");
            m_truthTreeManager->makeOutputVariable(m_mc_AntiLepton_Phi, "AntiLepton_Phi_bare");
	    m_truthTreeManager->makeOutputVariable(m_weight_pileup_hash_truth, "weight_pileup_hash_truth");
            m_truthTreeManager->makeOutputVariable(m_weight_kfactor_truth, "weight_KFactor_truth");*/
  	//} // end of if statement 
	/*m_FwdElectronIDToolLoose = new AsgForwardElectronLikelihoodTool("AsgFwdElectronLikelihoodToolLoose"); 
	top::check( m_FwdElectronIDToolLoose->setProperty("ConfigFile", "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/ElectronPhotonSelectorTools/offline/mc16_20190729/FwdLHLooseConf.conf") , "Failed to set config for Loose AsgElectronFwdLikelihoodTool"); 
	top::check(m_FwdElectronIDToolLoose->initialize(), "Couldn't initialise Forward Electron LH ID Tool Medium" );

	m_FwdElectronIDToolMedium = new AsgForwardElectronLikelihoodTool("AsgFwdElectronLikelihoodToolMedium"); 
	top::check( m_FwdElectronIDToolMedium->setProperty("ConfigFile", "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/ElectronPhotonSelectorTools/offline/mc16_20190729/FwdLHMediumConf.conf") , "Failed to set config for Medium AsgElectronFwdLikelihoodTool"); 
	top::check(m_FwdElectronIDToolMedium->initialize(), "Couldn't initialise Forward Electron LH ID Tool Medium" );

	m_FwdElectronIDToolTight = new AsgForwardElectronLikelihoodTool("AsgFwdElectronLikelihoodToolTight"); 
	top::check( m_FwdElectronIDToolTight->setProperty("ConfigFile", "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/ElectronPhotonSelectorTools/offline/mc16_20190729/FwdLHTightConf.conf") , "Failed to set config for Tight AsgElectronFwdLikelihoodTool"); 
	top::check(m_FwdElectronIDToolTight->initialize(), "Couldn't initialise Forward Electron LH ID Tool Tight" );*/

	/*m_muonSelection_HighPt = new  CP::MuonSelectionTool("MuonSelectionTool");
	top::check( m_muonSelection_HighPt->setProperty("MaxEta", 2.5 ) , "Failed to initialize property for CP::m_muonSelection_HighPt" );
	top::check( m_muonSelection_HighPt->setProperty("MuQuality", 4 ) , "Failed to initialize  property for CP::m_muonSelection_HighPt" );
	top::check( m_muonSelection_HighPt->initialize() , "Failed to initialize CP::m_muonSelection_HighPt" );*/

	//top::check( m_fwdElectronSelectionTool.retrieve() , "Failed to retrieve CP::FwdElectronSelector" );
	//top::check( m_fwdElectronLooseSelectionTool.retrieve() , "Failed to retrieve CP::FwdElectronSelectorLoose" );


        top::check( m_muonSelectionTool.retrieve() , "Failed to retrieve muonSelectionTool" );       
	top::check( m_muonSelectionToolLoose.retrieve() , "Failed to retrieve muonSelectionTool" );
        top::check( m_egammaCalibrationAndSmearingTool.retrieve() , "Failed to retrieve EgammaCalibrationAndSmearingTool" );
	top::check( m_isolationTool_FCHighPtCaloOnly.retrieve(), "Failed to retrieve m_isolationTool_FCHighPtCaloOnly" );

	declareProperty( "IsolationTool_FCLoose" , m_isolationTool_FixedCutLoose );
	top::check( m_isolationTool_FixedCutLoose.retrieve() , "Failed to retrieve Isolation Tool" );

	declareProperty( "IsolationTool_FCTight" , m_isolationTool_FixedCutTight );
	top::check( m_isolationTool_FixedCutTight.retrieve() , "Failed to retrieve Isolation Tool" );

        // K factor
        m_kfactorTool = new LPXKfactorTool("LPXKfactorTool");
        top::check(m_kfactorTool->setProperty("isMC15",true),
                "Failed to set property for k-factor calculation");
        top::check(m_kfactorTool->initialize(),
                "Failed initialize k factor calculation");

        ///-- Loop over the systematic TTrees and add the custom variables --///
        for (auto systematicTree : treeManagers()) {

          systematicTree->makeOutputVariable(m_weight_kfactor, "weight_KFactor");
          systematicTree->makeOutputVariable(mu_isMedium, "mu_isMedium");
	  systematicTree->makeOutputVariable(mu_isHighPt, "mu_isHighPt");
	  systematicTree->makeOutputVariable(mu_PtID, "mu_PtID");
	  systematicTree->makeOutputVariable(mu_PtMS, "mu_PtMS");
          systematicTree->makeOutputVariable(mu_isolation_FixedCutTight, "mu_isolation_FixedCutTight");
          systematicTree->makeOutputVariable(mu_isolation_FixedCutLoose, "mu_isolation_FixedCutLoose");
          systematicTree->makeOutputVariable(mu_isolation_FixedCutPflowTight, "mu_isolation_FixedCutPflowTight");
          systematicTree->makeOutputVariable(mu_isolation_FixedCutPflowLoose, "mu_isolation_FixedCutPflowLoose");
          systematicTree->makeOutputVariable(mu_isolation_FCTightTrackOnly_FixedRad, "mu_isolation_FCTightTrackOnly_FixedRad");
          systematicTree->makeOutputVariable(el_isolation_FixedCutLoose, "el_isolation_FixedCutLoose");
          systematicTree->makeOutputVariable(el_isolation_FixedCutTight, "el_isolation_FixedCutTight");
          systematicTree->makeOutputVariable(el_isolation_PflowTight, "el_isolation_PflowTight");
          systematicTree->makeOutputVariable(el_isolation_TightTrackOnly, "el_isolation_TightTrackOnly");
          systematicTree->makeOutputVariable(el_isolation_TightTrackOnly_FixedRad, "el_isolation_TightTrackOnly_FixedRad");
          systematicTree->makeOutputVariable(el_isElTight, "el_isElTight");
          systematicTree->makeOutputVariable(fwdel_isTight, "fwdel_isTight");
          systematicTree->makeOutputVariable(fwdel_isMedium, "fwdel_isMedium");
          systematicTree->makeOutputVariable(fwdel_isLoose, "fwdel_isLoose");
          systematicTree->makeOutputVariable(el_isElMedium, "el_isElMedium");
          systematicTree->makeOutputVariable(el_isElLoose, "el_isElLoose");
          systematicTree->makeOutputVariable(el_DFCommonElectronsECIDS, "el_DFCommonElectronsECIDS" );
          systematicTree->makeOutputVariable(el_DFCommonElectronsECIDSResult, "el_DFCommonElectronsECIDSResult" );
  	  systematicTree->makeOutputVariable(el_isPromptLepton, "el_isPromptLepton");
	  if( m_config->isMC() ){
	        systematicTree->makeOutputVariable(m_mc_Quark2_Px, "Quark2_Px");
            	systematicTree->makeOutputVariable(m_mc_Quark2_Py, "Quark2_Py");
            	systematicTree->makeOutputVariable(m_mc_Quark2_Pz, "Quark2_Pz");
            	systematicTree->makeOutputVariable(m_mc_Quark2_E, "Quark2_E");
            	systematicTree->makeOutputVariable(m_mc_Quark2_PDGID, "Quark2_PDGID");
            	systematicTree->makeOutputVariable(m_mc_Quark1_Px, "Quark1_Px");
            	systematicTree->makeOutputVariable(m_mc_Quark1_Py, "Quark1_Py");
            	systematicTree->makeOutputVariable(m_mc_Quark1_Pz, "Quark1_Pz");
            	systematicTree->makeOutputVariable(m_mc_Quark1_E, "Quark1_E");
            	systematicTree->makeOutputVariable(m_mc_Quark1_PDGID, "Quark1_PDGID");
            	systematicTree->makeOutputVariable(m_mc_Dilepton_Pt_born, "Dilepton_Pt_born");
            	systematicTree->makeOutputVariable(m_mc_Dilepton_Rapidity_born, "Dilepton_Rapidity_born");
            	systematicTree->makeOutputVariable(m_mc_Dilepton_Eta_born, "Dilepton_Eta_born");
            	systematicTree->makeOutputVariable(m_mc_Dilepton_Phi_born, "Dilepton_Phi_born");
            	systematicTree->makeOutputVariable(m_mc_Dilepton_Mass_born, "Dilepton_Mass_born");
            	systematicTree->makeOutputVariable(m_mc_Dilepton_m_born, "Dilepton_m_born");
            	systematicTree->makeOutputVariable(m_mc_Lepton_Pt_born, "Lepton_Pt_born");
            	systematicTree->makeOutputVariable(m_mc_AntiLepton_Pt_born, "AntiLepton_Pt_born");
            	systematicTree->makeOutputVariable(m_mc_Lepton_Eta_born, "Lepton_Eta_born");
            	systematicTree->makeOutputVariable(m_mc_AntiLepton_Eta_born, "AntiLepton_Eta_born");
            	systematicTree->makeOutputVariable(m_mc_Lepton_Phi_born, "Lepton_Phi_born");
            	systematicTree->makeOutputVariable(m_mc_AntiLepton_Phi_born, "AntiLepton_Phi_born");
            	systematicTree->makeOutputVariable(m_mc_Lepton_m_born, "Lepton_m_born");
            	systematicTree->makeOutputVariable(m_mc_AntiLepton_m_born, "AntiLepton_m_born");
            	systematicTree->makeOutputVariable(m_mc_Lepton_Pt_bare, "Lepton_Pt_bare");
            	systematicTree->makeOutputVariable(m_mc_AntiLepton_Pt_bare, "AntiLepton_Pt_bare");
            	systematicTree->makeOutputVariable(m_mc_Lepton_Eta_bare, "Lepton_Eta_bare");
            	systematicTree->makeOutputVariable(m_mc_AntiLepton_Eta_bare, "AntiLepton_Eta_bare");
            	systematicTree->makeOutputVariable(m_mc_Lepton_Phi_bare, "Lepton_Phi_bare");
            	systematicTree->makeOutputVariable(m_mc_AntiLepton_Phi_bare, "AntiLepton_Phi_bare");
	    	systematicTree->makeOutputVariable(Lepton_Pt_dressed, "Lepton_Pt_dressed");
	    	systematicTree->makeOutputVariable(Lepton_Eta_dressed, "Lepton_Eta_dressed");
	    	systematicTree->makeOutputVariable(Lepton_Phi_dressed, "Lepton_Phi_dressed");
	    	systematicTree->makeOutputVariable(Lepton_Charge_dressed, "Lepton_Charge_dressed");
	    	systematicTree->makeOutputVariable(AntiLepton_Pt_dressed, "AntiLepton_Pt_dressed");
	    	systematicTree->makeOutputVariable(AntiLepton_Eta_dressed, "AntiLepton_Eta_dressed");
	    	systematicTree->makeOutputVariable(AntiLepton_Phi_dressed, "AntiLepton_Phi_dressed");
	    	systematicTree->makeOutputVariable(AntiLepton_Charge_dressed, "AntiLepton_Charge_dressed"); 
	     } //end of is MC


        } // end Of tree loops
    } // end of initialise

    int CustomEventSaver::isBranchStored(top::TreeManager const *treeManager, const std::string &variableName){
  	if (variableName.find("weight_oldTriggerSF") != std::string::npos){
    	    return 0;
  	}
  	//if (variableName.find("weight_leptonSF_") != std::string::npos){
    	//    return 0;
  	//}
  	//if (variableName.find("weight_indiv") != std::string::npos){
    	//    return 0;
  	//}
  	//if (variableName.find("_STAT_LOWPT") != std::string::npos){
    	//    return 0;
  	//}
  	//if (variableName.find("_SYST_LOWPT") != std::string::npos){
    	//    return 0;
  	//}
  	//if (variableName.find("_STAT_UP") != std::string::npos){
    	//    return 0;
  	//}
  	//if (variableName.find("_STAT_DOWN") != std::string::npos){ 
    	//    return 0;
  	//}
 	if (variableName.find("jet_truth") != std::string::npos){
    	    return 0;
  	}
 	//if (variableName.find("jet_DL1") != std::string::npos){
    	//    return 0;
  	//}
	//if (variableName.find("jet_mv2c00") != std::string::npos){ 
    	//    return 0;
  	//}
	//if (variableName.find("jet_mv2c20") != std::string::npos){ 
    	  //  return 0;
  	//}
	//if (variableName.find("jet_MV2") != std::string::npos){ 
    	//   return 0;
  	//}
        //if( systematicTree->name() != "nominal" ){
	//  if (variableName.find("HLT") != std::string::npos){  
    	//    return 0;
        //  } // end of if statement
  	//}
	if (variableName.find("jet_ip3dsv1") != std::string::npos){  
    	    return 0;
  	}
	if (variableName.find("jet_isTrueHS") != std::string::npos){  
    	    return 0;
  	} 
	if (variableName.find("el_true") != std::string::npos){  
    	    return 0;
  	} 
	if (variableName.find("mu_true") != std::string::npos){  
    	    return 0;
  	} 

  	return -1;
    } //end of function


    ///-- saveEvent - run for every systematic and every event --///
    void CustomEventSaver::saveEvent(const top::Event& event) 
    {
        ///-- set our variables to zero --///
        el_isElTight.clear();
        el_isElMedium.clear();
        el_isElLoose.clear();
	el_DFCommonElectronsECIDS.clear();
	el_DFCommonElectronsECIDSResult.clear();
        el_isPromptLepton.clear();
	el_isolation_FixedCutLoose.clear();
	el_isolation_FixedCutTight.clear();
	el_isolation_PflowTight.clear();
	el_isolation_TightTrackOnly.clear();
	el_isolation_TightTrackOnly_FixedRad.clear();
        el_isolation_FCHighPtCaloOnly.clear();
	mu_isMedium.clear();
	mu_PtMS.clear();
        mu_PtID.clear();
	mu_isHighPt.clear();
	mu_isolation_FixedCutTight.clear();
	mu_isolation_FixedCutLoose.clear();	
	mu_isolation_FixedCutPflowTight.clear();
	mu_isolation_FixedCutPflowLoose.clear();
	mu_isolation_FCTightTrackOnly_FixedRad.clear();
	fwdel_isTight.clear();
	fwdel_isMedium.clear();
	fwdel_isLoose.clear();
	//mu_isolation_FixedCutTightTrackOnly.clear();
	//mu_isolation_FixCutHighPtTrackOnly.clear();

        // Apply ID selection
        /*for (const auto fwdElItr : event.m_fwdElectrons ) {
		fwdel_isTight.push_back( fwdElItr->auxdata<char>("DFCommonForwardElectronsLHTight") );
		fwdel_isMedium.push_back( fwdElItr->auxdata<char>("DFCommonForwardElectronsLHMedium") );
		fwdel_isLoose.push_back( fwdElItr->auxdata<char>("DFCommonForwardElectronsLHLoose") );
		
		//fwdel_isTight.push_back( m_FwdElectronIDToolTight->accept(fwdElItr) );
		//fwdel_isMedium.push_back( m_FwdElectronIDToolMedium->accept(fwdElItr) );
		//fwdel_isLoose.push_back( m_FwdElectronIDToolLoose->accept(fwdElItr) );
	} // end of if statement */

        // Apply ID selection
        for (const auto elItr : event.m_electrons) {
		//el_isolation_FixedCutHighPtCaloOnly.push_back( m_isolationTool_FixedCutHighPtCaloOnly->accept(*elItr) );
   	        //el_isPromptLepton.push_back( elItr->auxdata<float>("PromptLeptonVeto")  );
		
		el_isolation_FixedCutLoose.push_back( elItr->auxdata<char>("AnalysisTop_Isol_FCLoose")  );
		el_isolation_FixedCutTight.push_back( elItr->auxdata<char>("AnalysisTop_Isol_FCTight") );

		el_isolation_PflowTight.push_back( elItr->auxdata<char>("AnalysisTop_Isol_PflowTight") );
		el_isolation_TightTrackOnly.push_back( elItr->auxdata<char>("AnalysisTop_Isol_TightTrackOnly") );
		el_isolation_TightTrackOnly_FixedRad.push_back( elItr->auxdata<char>("AnalysisTop_Isol_TightTrackOnly_FixedRad") );

		el_isolation_FCHighPtCaloOnly.push_back( m_isolationTool_FCHighPtCaloOnly->accept(*elItr) );


		if( elItr->isAvailable<char>("DFCommonElectronsECIDS") ) {
			el_DFCommonElectronsECIDS.push_back( elItr->auxdata<char>("DFCommonElectronsECIDS") );
		} // end of if statement

		if( elItr->isAvailable<double>("DFCommonElectronsECIDSResult") ) {
			el_DFCommonElectronsECIDSResult.push_back( elItr->auxdata<double>("DFCommonElectronsECIDSResult") );
		} // end of if statement

		//el_isolation_Gradient.push_back( m_isolationTool_Gradient->accept(*elItr) );
		if( elItr->isAvailable<char>("DFCommonElectronsLHTight")) {
		  el_isElTight.push_back( elItr->auxdataConst<char>("DFCommonElectronsLHTight")  );
		} // end of loop
		if( elItr->isAvailable<char>("DFCommonElectronsLHMedium")) {
                  el_isElMedium.push_back( elItr->auxdataConst<char>("DFCommonElectronsLHMedium")  );
                } // end of loop    
		if( elItr->isAvailable<char>("DFCommonElectronsLHLoose")) {
                  el_isElLoose.push_back( elItr->auxdataConst<char>("DFCommonElectronsLHLoose")  );
                } // end of loop    
	} // end of if statement


        // Apply ID selection Muons
        for (const auto muItr : event.m_muons) {
		mu_isHighPt.push_back( m_muonSelectionTool->accept(*muItr) );
		mu_PtMS.push_back( muItr->auxdataConst<float>("MuonSpectrometerPt") );
		mu_PtID.push_back( muItr->auxdataConst<float>("InnerDetectorPt") );
		mu_isMedium.push_back( m_muonSelectionTool->accept(*muItr) );
		mu_isolation_FixedCutTight.push_back( m_isolationTool_FixedCutTight->accept(*muItr) );	    
		mu_isolation_FixedCutLoose.push_back( m_isolationTool_FixedCutLoose->accept(*muItr) );
	   
		mu_isolation_FixedCutTight.push_back( muItr->auxdata<char>("AnalysisTop_Isol_FCTight") );	    
		mu_isolation_FixedCutLoose.push_back( muItr->auxdata<char>("AnalysisTop_Isol_FCLoose") );

		mu_isolation_FixedCutPflowTight.push_back( muItr->auxdata<char>("AnalysisTop_Isol_FixedCutPflowTight") );
		mu_isolation_FixedCutPflowLoose.push_back( muItr->auxdata<char>("AnalysisTop_Isol_FixedCutPflowLoose") );
		mu_isolation_FCTightTrackOnly_FixedRad.push_back( muItr->auxdata<char>("AnalysisTop_Isol_FCTightTrackOnly_FixedRad") );

		//std::cout<<"Muons Pt MS "<<muItr->auxdataConst<float>("MuonSpectrometerPt")<<std::endl;
		//std::cout<<"Muons Pt ID "<<muItr->auxdataConst<float>("InnerDetectorPt")<<std::endl;
		//mu_isolation_FixedCutTightTrackOnly.push_back( m_isolationTool_FixedCutTightTrackOnly->accept(*muItr) );	    
		//mu_isolation_FixCutHighPtTrackOnly.push_back( m_isolationTool_FixCutHighPtTrackOnly->accept(*muItr) );	    
	} // end of if statement

        if (event.m_info->eventType(xAOD::EventInfo::IS_SIMULATION)) {
	        top::check(m_kfactorTool->getKFactor(m_weight_kfactor), "Error getting LPXKfactor.");
	        //m_BornMass = event.m_info->auxdata<double>("BornMass");
        }

        ///-- Let the base class do all the hard work --///
        top::EventSaverFlatNtuple::saveEvent(event);
    } // end of saveEvent

    ///-- saveTruthEvent - store some extra stuff from the truth information --///
    void CustomEventSaver::saveTruthEvent() 
    {

        if( m_config->isMC() /*&& m_config->useTruthParticles() && */ ) {


                bool FoundBornLepton=false, FoundBornAntiLepton=false, FoundBareLepton=false, FoundBareAntiLepton=false, FoundZBoson=false;
                TLorentzVector Lepton(0,0,0,0),AntiLepton(0,0,0,0), BareLepton(0,0,0,0),BareAntiLepton(0,0,0,0), Propagator(0,0,0,0), ZBoson(0,0,0,0), DressedLepton(0,0,0,0),DressedAntiLepton(0,0,0,0);
                int NumberQuarksFound=0;
		double PropagatorBosonMass=0.0;

                //std::cout<<"**** New Event *****"<<std::endl;                                                                                                                                                   

	        top::check( evtStore()->retrieve(tmuons,"STDMTruthMuons") , "Failed to get truth electrons"); 	   
		// Truth muons
	        for (const auto* const TrMu : *tmuons) {

			//Bare muons
			if( TrMu->charge()  == -1.0 &&  0.001*TrMu->pt() > BareLepton.Pt() ){ 
                                                                                                                                                                                                                 	                   m_mc_Lepton_Pt_bare = 0.001*TrMu->pt();
  			  m_mc_Lepton_Eta_bare = TrMu->eta();
			  m_mc_Lepton_Phi_bare = TrMu->phi();
			  BareLepton.SetPtEtaPhiM( m_mc_Lepton_Pt_bare, m_mc_Lepton_Eta_bare, m_mc_Lepton_Phi_bare, 0.001*TrMu->m() );

			} // end of if lepton    
                        if( TrMu->charge()  == 1.0 &&  0.001*TrMu->pt() > BareAntiLepton.Pt() ){
                                                                                                                                                                                                                  
                          m_mc_AntiLepton_Pt_bare = 0.001*TrMu->pt();
                          m_mc_AntiLepton_Eta_bare = TrMu->eta();
                          m_mc_AntiLepton_Phi_bare = TrMu->phi();
             		  BareAntiLepton.SetPtEtaPhiM( m_mc_AntiLepton_Pt_bare, m_mc_AntiLepton_Eta_bare, m_mc_AntiLepton_Phi_bare, 0.001*TrMu->m() );

                        } // end of if lepton  

			//Dressed muons
			if( TrMu->charge()  == -1.0 &&  0.001*TrMu->pt() > DressedLepton.Pt() ){ 
			        Lepton_Pt_dressed=0.001*TrMu->auxdata<float>("pt_dressed");
	    			Lepton_Phi_dressed=TrMu->auxdata<float>("phi_dressed");
	    			Lepton_Eta_dressed=TrMu->auxdata<float>("eta_dressed");
	    			Lepton_Charge_dressed=TrMu->charge();

			  	DressedLepton.SetPtEtaPhiM( 0.001*TrMu->auxdata<float>("pt_dressed"), TrMu->auxdata<float>("eta_dressed"), TrMu->auxdata<float>("phi_dressed"), 0.001*TrMu->m() );

			} // end of if lepton    

                        if( TrMu->charge()  == 1.0 &&  0.001*TrMu->pt() > DressedAntiLepton.Pt() ){
                                                                                                                                                                                                                  
    	    			AntiLepton_Pt_dressed=0.001*TrMu->auxdata<float>("pt_dressed");
	    			AntiLepton_Phi_dressed=TrMu->auxdata<float>("phi_dressed");
	    			AntiLepton_Eta_dressed=TrMu->auxdata<float>("eta_dressed");
	    			AntiLepton_Charge_dressed=TrMu->charge();

			 	DressedAntiLepton.SetPtEtaPhiM( 0.001*TrMu->auxdata<float>("pt_dressed"), TrMu->auxdata<float>("eta_dressed"), TrMu->auxdata<float>("phi_dressed"), 0.001*TrMu->m() );

                        } // end of if lepton  

		} // end of loop 

	        top::check( evtStore()->retrieve(telecs,"STDMTruthElectrons") , "Failed to get truth electrons"); 	   
		// Truth electrons
	        for (const auto* const TrEle : *telecs) {

			// Bare electrons
                        if( TrEle->charge()  == -1.0 &&  0.001*TrEle->pt() > BareLepton.Pt() ){
                                                                                                                                                                                                                  
                          m_mc_Lepton_Pt_bare = 0.001*TrEle->pt();
                          m_mc_Lepton_Eta_bare = TrEle->eta();
                          m_mc_Lepton_Phi_bare = TrEle->phi();
                          BareLepton.SetPtEtaPhiM( m_mc_Lepton_Pt_bare, m_mc_Lepton_Eta_bare, m_mc_Lepton_Phi_bare, 0.001*(TrEle->p4()).M() );
			  DressedLepton.SetPtEtaPhiM( 0.001*TrEle->auxdata<float>("pt_dressed"), TrEle->auxdata<float>("eta_dressed"), TrEle->auxdata<float>("phi_dressed"), 0.001*TrEle->m() );
                        } // end of if lepton                                                                                                                                                                     

                        if( TrEle->charge()  == 1.0 &&  0.001*TrEle->pt() > BareAntiLepton.Pt() ){
			  
                          m_mc_AntiLepton_Pt_bare = 0.001*TrEle->pt();
                          m_mc_AntiLepton_Eta_bare = TrEle->eta();
                          m_mc_AntiLepton_Phi_bare = TrEle->phi();
                          BareAntiLepton.SetPtEtaPhiM( m_mc_AntiLepton_Pt_bare, m_mc_AntiLepton_Eta_bare, m_mc_AntiLepton_Phi_bare, 0.001*(TrEle->p4()).M() );
			  DressedAntiLepton.SetPtEtaPhiM( 0.001*TrEle->auxdata<float>("pt_dressed"), TrEle->auxdata<float>("eta_dressed"), TrEle->auxdata<float>("phi_dressed"), 0.001*TrEle->m() );                                                                                                                       
                        } // end of if lepton  

			//Dressed electrons
			if( TrEle->charge()  == -1.0 &&  0.001*TrEle->pt() > DressedLepton.Pt() ){ 
                                                                                                                                                                                                                     	         			Lepton_Pt_dressed=0.001*TrEle->auxdata<float>("pt_dressed");
	    			Lepton_Phi_dressed=TrEle->auxdata<float>("phi_dressed");
	    			Lepton_Eta_dressed=TrEle->auxdata<float>("eta_dressed");
	    			Lepton_Charge_dressed=TrEle->charge();

			  	DressedLepton.SetPtEtaPhiM( 0.001*TrEle->auxdata<float>("pt_dressed"), TrEle->auxdata<float>("eta_dressed"), TrEle->auxdata<float>("phi_dressed"), 0.001*TrEle->m() );

			} // end of if lepton    

                        if( TrEle->charge()  == 1.0 &&  0.001*TrEle->pt() > DressedAntiLepton.Pt() ){
                                                                                                                                                                                                                  
    	    			AntiLepton_Pt_dressed=0.001*TrEle->auxdata<float>("pt_dressed");
	    			AntiLepton_Phi_dressed=TrEle->auxdata<float>("phi_dressed");
	    			AntiLepton_Eta_dressed=TrEle->auxdata<float>("eta_dressed");
	    			AntiLepton_Charge_dressed=TrEle->charge();

			 	DressedAntiLepton.SetPtEtaPhiM( 0.001*TrEle->auxdata<float>("pt_dressed"), TrEle->auxdata<float>("eta_dressed"), TrEle->auxdata<float>("phi_dressed"), 0.001*TrEle->m() );

                        } // end of if lepton  
   
		} // end of loop 

	        //top::check( evtStore()->retrieve(tBoson,"TruthBoson") , "Failed to get TruthBoson"); 	   
		// Truth Boson
	        /*for (const auto* const TrZ : *tBoson) {

			//std::cout<<"Propagator Truth Boson "<<0.001*(TrZ->p4()).M()<<std::endl;
			//PropagatorBosonMass=0.001*(TrZ->p4()).M();

			m_mc_Dilepton_Eta_born = TrZ->eta();
			m_mc_Dilepton_Pt_born = 0.001*TrZ->pt();
			m_mc_Dilepton_Rapidity_born = (TrZ->p4()).Rapidity();
			m_mc_Dilepton_Phi_born = TrZ->phi();
			m_mc_Dilepton_Mass_born = 0.001*(TrZ->p4()).Mag();
			m_mc_Dilepton_m_born = 0.001*(TrZ->p4()).M();

		} // end of loop */

  		const xAOD::EventInfo* eventInfo(nullptr);
  		top::check( evtStore()->retrieve(eventInfo, m_config->sgKeyEventInfo()) , "Failed to retrieve EventInfo" );

	        const xAOD::TruthParticleContainer* truth(nullptr);
	        top::check( evtStore()->retrieve(truth, m_config->sgKeyMCParticle()) , "Failed to retrieve TruthParticleContainer" );

	        //if (event.m_info->isAvailable<unsigned long long>("PileupWeight_Hash"))
                m_weight_pileup_hash_truth = eventInfo->auxdataConst<unsigned long long>("PileupWeight_Hash");

		//std::cout<<"**** New Event *****"<<std::endl;
                for (const auto* const mcPtr : *truth) {

	          m_mc_status.clear();
        	  m_mc_barcode.clear();

         	  if (truth != nullptr) {
	            unsigned int i(0);
	            unsigned int truthSize = truth->size();
	            //m_mc_status.resize(truthSize);
        	    //m_mc_barcode.resize(truthSize);
                    //m_mc_barcode[i] = mcPtr->barcode();
                    //m_mc_status[i] = mcPtr->status();

		    if( ((fabs(mcPtr->pdgId()) >= 1 && fabs(mcPtr->pdgId()) <= 6) ||  mcPtr->pdgId() == 21) && fabs(mcPtr->status()) == 21 ){
		      if( NumberQuarksFound == 0 ){
			m_mc_Quark1_Px = 0.001*(mcPtr->p4()).Px();
			m_mc_Quark1_Py = 0.001*(mcPtr->p4()).Py();
			m_mc_Quark1_Pz = 0.001*(mcPtr->p4()).Pz();
			m_mc_Quark1_E = 0.001*(mcPtr->p4()).E();
			m_mc_Quark1_PDGID = mcPtr->pdgId();
		      } // end of if statement
		      if( NumberQuarksFound == 1 ){
                        m_mc_Quark2_Px = 0.001*(mcPtr->p4()).Px();
			m_mc_Quark2_Py = 0.001*(mcPtr->p4()).Py();
			m_mc_Quark2_Pz = 0.001*(mcPtr->p4()).Pz();
			m_mc_Quark2_E = 0.001*(mcPtr->p4()).E();
			m_mc_Quark2_PDGID = mcPtr->pdgId();
                      } // end of if statement  
		      NumberQuarksFound++;
		    } //end of if statement

		    // Getting lepton information for Powheg
		    if( fabs(mcPtr->pdgId()) == 11 || fabs(mcPtr->pdgId()) == 13 ){
		      //std::cout<<"Found particle with PDGID "<<mcPtr->pdgId()<<" with status "<<mcPtr->status()<<" and pT "<<0.001*(mcPtr->p4()).Pt()<<std::endl;
		       //if( mcPtr->status() == 3  ){
		       if( mcPtr->status() == 11  ){
		       //if( mcPtr->status() == 23 || mcPtr->status() == 44 || mcPtr->status() == 51 || mcPtr->status() == 52  ){
		   		//std::cout<<"Found "<<mcPtr->pdgId()<<" with status "<<mcPtr->status()<<" and mass "<<0.001*(mcPtr->p4()).Mag()<<std::endl;		                
				if( (mcPtr->pdgId() == 11 || mcPtr->pdgId() == 13) &&  0.001*mcPtr->pt() > Lepton.Pt() ){ 
				 	//std::cout<<"Found lepton with pT "<<0.001*mcPtr->pt()<<" and eta "<<mcPtr->eta()<<std::endl;                                                                                                   
					FoundBornLepton=true;
					m_mc_Lepton_Pt_born = 0.001*mcPtr->pt();
					m_mc_Lepton_Eta_born = mcPtr->eta();
					m_mc_Lepton_Phi_born = mcPtr->phi();
					m_mc_Lepton_m_born = 0.001*(mcPtr->p4()).M();
					Lepton.SetPtEtaPhiM( m_mc_Lepton_Pt_born, m_mc_Lepton_Eta_born, m_mc_Lepton_Phi_born, m_mc_Lepton_m_born  );
					//std::cout<<"4-vector lepton pT "<<0.001*Lepton.Pt()<<" and eta "<<Lepton.Eta()<<std::endl;
			        } // end of if lepton 				
				if( (mcPtr->pdgId() == -11 || mcPtr->pdgId() == -13) && 0.001*mcPtr->pt() > AntiLepton.Pt() ){ 
				  	//std::cout<<"Found antilepton with pT "<<0.001*mcPtr->pt()<<" and eta "<<mcPtr->eta()<<std::endl;
					FoundBornAntiLepton=true;
					m_mc_AntiLepton_Pt_born = 0.001*mcPtr->pt();
					m_mc_AntiLepton_Eta_born = mcPtr->eta();
					m_mc_AntiLepton_Phi_born = mcPtr->phi();
					m_mc_AntiLepton_m_born = 0.001*(mcPtr->p4()).M();
					AntiLepton.SetPtEtaPhiM( m_mc_AntiLepton_Pt_born, m_mc_AntiLepton_Eta_born, m_mc_AntiLepton_Phi_born, m_mc_AntiLepton_m_born  );
					//std::cout<<"4-vector antilepton pT "<<0.001*AntiLepton.Pt()<<" and eta "<<AntiLepton.Eta()<<std::endl;
			        } // end of if lepton 
		       } // end of if statement 

		       if( mcPtr->status() == 1  ){
				if( (mcPtr->pdgId() == 11 || mcPtr->pdgId() == 13) &&  0.001*mcPtr->pt() > BareLepton.Pt() ){ 
				  //std::cout<<"Found lepton with pT "<<0.001*mcPtr->pt()<<" and eta "<<mcPtr->eta()<<std::endl;                                                                                                   
					FoundBareLepton=true;
					m_mc_Lepton_Pt_bare = 0.001*mcPtr->pt();
					m_mc_Lepton_Eta_bare = mcPtr->eta();
					m_mc_Lepton_Phi_bare = mcPtr->phi();
					BareLepton = mcPtr->p4();
					//std::cout<<"4-vector lepton pT "<<Lepton.Pt()<<" and eta "<<Lepton.Eta()<<std::endl;
			        } // end of if lepton 				
				if( (mcPtr->pdgId() == -11 || mcPtr->pdgId() == -13) && 0.001*mcPtr->pt() > BareAntiLepton.Pt() ){ 
				  //std::cout<<"Found antilepton with pT "<<0.001*mcPtr->pt()<<" and eta "<<mcPtr->eta()<<std::endl;
					FoundBareAntiLepton=true;
					m_mc_AntiLepton_Pt_bare = 0.001*mcPtr->pt();
					m_mc_AntiLepton_Eta_bare = mcPtr->eta();
					m_mc_AntiLepton_Phi_bare = mcPtr->phi();
					BareAntiLepton = mcPtr->p4();
					//std::cout<<"4-vector antilepton pT "<<AntiLepton.Pt()<<" and eta "<<AntiLepton.Eta()<<std::endl;
			        } // end of if lepton 
			} // end of if statement 
			
                    } //end of if lepton/antilepton



		    if( (mcPtr->pdgId() == 23 ) && !FoundZBoson ){

				if( mcPtr->pdgId() == 23 && mcPtr->status() == 22 ){
				   FoundZBoson=true;
				   ZBoson = mcPtr->p4();
				   m_mc_Dilepton_Eta_born = mcPtr->eta();
				   m_mc_Dilepton_Pt_born = 0.001*mcPtr->pt();
				   m_mc_Dilepton_Rapidity_born = (mcPtr->p4()).Rapidity();
				   m_mc_Dilepton_Phi_born = mcPtr->phi();
				   m_mc_Dilepton_Mass_born = 0.001*(mcPtr->p4()).Mag();
				   m_mc_Dilepton_m_born = 0.001*(mcPtr->p4()).M();
				} // end of if statement

		    } // end of if statement
                    ++i;
                  } // end of if not null pointer

                } // end of particle Loop

		//Check for the case in which status 3 leptons are not found and take status 1 instead
		if( !FoundBornLepton ){
		        //std::cout<<"Substituting born lepton with bare values "<<std::endl;
   			//std::cout<<"Bare Lepton pT "<<BareLepton.Pt()<<" and eta "<<BareLepton.Eta()<<std::endl;
			m_mc_Lepton_Pt_born = BareLepton.Pt();
			m_mc_Lepton_Eta_born = BareLepton.Eta();
			m_mc_Lepton_Phi_born = BareLepton.Phi();
			Lepton=BareLepton;

		} // end of if statement 
		if( !FoundBornAntiLepton ){
	         	//std::cout<<"Substituting born antilepton with bare values "<<std::endl;
		        //std::cout<<"Bare Antilepton pT "<<BareAntiLepton.Pt()<<" and eta "<<BareAntiLepton.Eta()<<std::endl;
			m_mc_AntiLepton_Pt_born = BareAntiLepton.Pt();
			m_mc_AntiLepton_Eta_born = BareAntiLepton.Eta();
			m_mc_AntiLepton_Phi_born = BareAntiLepton.Phi();
			AntiLepton=BareAntiLepton;

		} // end of if statement

   		//std::cout<<"Born Lepton pT "<<m_mc_Lepton_Pt_born<<" and eta "<<m_mc_Lepton_Eta_born<<std::endl;
   		//std::cout<<"Born Antilepton pT "<<m_mc_AntiLepton_Pt_born<<" and eta "<<m_mc_AntiLepton_Eta_born<<std::endl;

		Propagator=Lepton+AntiLepton;

		if( !FoundZBoson ){
		                   //std::cout<<"Didn't find Z boson "<<std::endl;
				   m_mc_Dilepton_Eta_born = Propagator.Eta();
				   m_mc_Dilepton_Pt_born = 0.001*Propagator.Pt();
				   m_mc_Dilepton_Rapidity_born = Propagator.Rapidity();
				   m_mc_Dilepton_Phi_born = Propagator.Phi();
				   m_mc_Dilepton_Mass_born = 0.001*Propagator.Mag();
				   m_mc_Dilepton_m_born = 0.001*Propagator.M();			

		} // end of if statement

		//std::cout<<"Lepton 1 Pt "<<0.001*Lepton.Pt()<<" Eta "<<Lepton.Eta()<<std::endl;
		//std::cout<<"Lepton 2 Pt "<<0.001*AntiLepton.Pt()<<" Eta "<<AntiLepton.Eta()<<std::endl;
		//std::cout<<"Propagator dilPt "<<Propagator.Pt()<<" Propagator Boson "<<Propagator.Mag()<<" and rapidity "<<Propagator.Rapidity()<<std::endl;
		//std::cout<<"Zboson dilPt "<<m_mc_Dilepton_Pt_born<<" Propagator Boson "<<m_mc_Dilepton_Mass_born<<" and rapidity "<<m_mc_Dilepton_Rapidity_born<<std::endl;

		if( (Propagator.Mag() - m_mc_Dilepton_Mass_born) > 1.0 ) std::cout<<"WTF they are different!"<<std::endl;

    		EventSaverFlatNtuple::saveTruthEvent();

	} // end of isSimulation */
  } // end of saveTruthEvent

} // end of program
