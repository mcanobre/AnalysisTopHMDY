#!/bin/bash
if test $# -ne 5
then
	echo "Usage: makeConfig.bash [GRL] [PRW] [lumicalc] [output] [triggers]"
	echo "Given " $#
	exit 5
fi

#Writing libraries
echo "LibraryNames libTopEventSelectionTools libTopEventReconstructionTools libMyWlnuSelection libLPXKfactorTool"

#Writing good runs list
echo "#--> GOOD RUNS LIST
GRLDir MyWlnuSelection/GRL
GRLFile ${1}"

#Writing pileup files
echo "#--> PILEUP REWEIGHTING
PRWConfigFiles MyWlnuSelection/PRW_lumicalc/${2}
PRWLumiCalcFiles MyWlnuSelection/PRW_lumicalc/${3}"

echo "#--> FAST SIMULATION
IsAFII False"

#Writing object configuration
echo "#--> ELECTRON CONFIGURATION
ElectronCollectionName Electrons
ElectronID MediumLH
ElectronIDLoose MediumLH
ElectronIsolation FixedCutLoose
ElectronPt 20000
ElectronLarCrack True

#--> MUON CONFIGURATION
MuonCollectionName Muons
MuonIsolation FixedCutLoose
#MuonIsolation LooseTrackOnly
MuonIsolationLoose LooseTrackOnly
MuonPt 20000
MuonQuality Medium
MuonQualityLoose Medium
MuonTriggerSF HLT_mu50

#--> JET CONFIGURATION
JetCollectionName AntiKt4EMTopoJets
LargeJetCollectionName None
LargeJetSubstructure None
JetPt 20000
JetEta 2.4
JVTinMETCalculation True

#--> TAU CONFIGURATION
TauCollectionName None

#--> PHOTON CONFIGURATION
PhotonCollectionName None

#-->MET CONFIGURATION
METCollectionName MET_Reference_AntiKt4EMTopo

#--> TRUTH CONFIGURATION
TruthCollectionName TruthParticles
TruthJetCollectionName AntiKt4TruthJets"

#Writing framework configuration
echo "#--> FRAMEWORK CONFIGURATION
ObjectSelectionName top::ObjectLoaderStandardCuts
#ObjectSelectionName top::CustomObjectLoader
OutputFormat top::CustomEventSaver
#OutputFormat top::EventSaverFlatNtuple
#OutputFormat top::EventSaverxAODNext
OutputEvents SelectedEvents
OutputFilename ${4}

NEvents -1000"

#Writing event selections
echo "SELECTION ElectronSelection

INITIAL
GRL
GOODCALO
PRIVTX
TRIGDEC  ${5}
#TRIGMATCH
EL_N 65000 == 1
EL_N 20000 == 1
#POSELEC
#VETO2NDMUON
MU_N 20000 == 0
JETCLEAN LooseBad
MET > 65000
MWT > 130000
SAVE
#PRINT"
