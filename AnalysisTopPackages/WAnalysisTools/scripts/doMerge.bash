#!/bin/bash
find . | grep MU.root | grep -v list > MU.list
find . | grep ETA.root | grep -v list > ETA.list
find . | grep DPHI.root | grep -v list > DPHI.list
find . | grep MET.root | grep -v list > MET.list
find . | grep MT.root | grep -v list > MT.list
find . | grep MTLOG.root | grep -v list > MTLOG.list
find . | grep PT.root | grep -v list > PT.list
find . | grep PTLOG.root | grep -v list > PTLOG.list
find . | grep TABLE.root | grep -v list > TABLE.list

python ../../MyWlnuSelection/scripts/mergeHists.py MU MU.list
python ../../MyWlnuSelection/scripts/mergeHists.py ElETA ETA.list
python ../../MyWlnuSelection/scripts/mergeHists.py ElDPHI DPHI.list
python ../../MyWlnuSelection/scripts/mergeHists.py ElMET MET.list
python ../../MyWlnuSelection/scripts/mergeHists.py ElMT MT.list
python ../../MyWlnuSelection/scripts/mergeHists.py ElPT PT.list
if test -s TABLE.list
then
python ../../MyWlnuSelection/scripts/mergeHists.py ElTABLE TABLE.list
fi
if test -s MTLOG.list
then
python ../../MyWlnuSelection/scripts/mergeHists.py ElMTLOG MTLOG.list
fi
if test -s PTLOG.list
then
python ../../MyWlnuSelection/scripts/mergeHists.py ElPTLOG PTLOG.list
fi
