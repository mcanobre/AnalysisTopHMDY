#!/usr/bin/env python

import ROOT
import sys
import numpy as np 

if len(sys.argv) < 3:
	sys.exit("Uage: ntuple2hists [config file] [file list]\n")

#Definitions and parameters
sys.path.append("./")
plotparams = __import__(sys.argv[1], globals(), locals(), [], -1)

MuBaseNames = [
	"MuETA",
	"MuPT",
	"MuMET",
	"MuMT",
	"MuDPHI"
]

ElBaseNames = [
	"ElETA",
	"ElPT",
	"ElMET",
	"ElMT",
	"ElCALOISO",
	"ElTRKISO",
	"ElD0SIG",
	"ElZ0SINT",
	"ElDPHI"
]

mPi = np.arccos(-1.0)

#book histograms
h = {}
h["MuETA"] = ROOT.TH1F("MuETA", "Muon #eta", 50, -2.5, 2.5)
h["MuPT"] = ROOT.TH1F("MuPT", "Muon p_{T}", 50, 25, 100)
h["MuMET"] = ROOT.TH1F("MuMET", "E_{T}^{miss}", 50, 25, 100)
h["MuMT"] = ROOT.TH1F("MuMT", "m_{T}", 50, 50, 120)
h["MuDPHI"] = ROOT.TH1F("MuDPHI", "#Delta#phi", 100, -mPi, mPi)
h["MuMW"] = ROOT.TH1F("MuMW", "born m_{W}", 100, 120, 5120)

h["ElETA"] = ROOT.TH1F("ElETA", "Electron #eta", 50, -2.5, 2.5)
h["ElPT"] = ROOT.TH1F("ElPT", "Electron p_{T}", 50, 25, 100)
h["ElMET"] = ROOT.TH1F("ElMET", "E_{T}^{miss}", 50, 25, 100)
h["ElMT"] = ROOT.TH1F("ElMT", "m_{T}", 50, 50, 120)
h["ElDPHI"] = ROOT.TH1F("ElDPHI", "#Delta#phi", 40, 0, mPi)
h["ElCALOISO"] = ROOT.TH1F("ElCALOISO", "Electron topoetcone20/p_{T}", 50, 0, 0.4)
h["ElTRKISO"] = ROOT.TH1F("ElTRKISO", "Electron ptvarcone20/p_{T}", 50, 0, 0.3)
h["ElD0SIG"] = ROOT.TH1F("ElD0SIG", "Electron d_{0} sig", 100, -5, 5)
h["ElZ0SINT"] = ROOT.TH1F("ElZ0SINT", "Electron #Delta z_{0}#sin{#theta}", 60, -0.3, 0.3)
h["ElMW"] = ROOT.TH1F("ElMW", "born m_{W}", 100, 120, 5120)
h["MU"] = ROOT.TH1F("MU", "#mu", 40, 0, 40)


lista = open(sys.argv[2], "r");
chain = ROOT.TChain("nominal")

#read files
while True:
	fName = lista.readline()
	fName = fName[:-1]
	if fName == "": break
	chain.Add(fName)

lista.close()

nEntries = chain.GetEntries()
sys.stdout.write("chain has "+str(nEntries)+" events\n")

#Final sum of weights for MC normalization
sumWeights = 0.0
#main loop
for evt in range(nEntries):
	chain.GetEntry(evt)
	if plotparams.DoMuons and chain.MuonSelection == 1:
		DeltaPhi = chain.met_phi - chain.mu_phi[0]
		if DeltaPhi > mPi: DeltaPhi = DeltaPhi - 2 * mPi
		if DeltaPhi < -mPi: DeltaPhi = DeltaPhi + 2 * mPi
		if DeltaPhi < 0: DeltaPhi = - DeltaPhi
		pTl = 0.001 * chain.mu_pt[0]
		MET = 0.001 * chain.met_met
		if (pTl < 27) or (MET < 27): continue
		MT = np.sqrt(2 * pTl * MET * (1 - np.cos(DeltaPhi)))
		if plotparams.isMC:
			WbornMass = 0.001 * chain.WbornMass
		#weights, if MC
		w = 1.0
		if plotparams.isMC:
			w = w * chain.weight_mc
			w = w * chain.weight_pileup
			sumWeights += w
			w = w * chain.LPXKfactor
			w = w * chain.weight_indiv_SF_MU_ID
			w = w * chain.weight_indiv_SF_MU_Isol
			w = w * chain.weight_indiv_SF_MU_Trigger
			w = w * chain.weight_indiv_SF_MU_TTVA
		h["MuETA"].Fill(chain.mu_eta[0], w)
		h["MuPT"].Fill(pTl, w)
		h["MuMET"].Fill(MET, w)
		h["MuDPHI"].Fill(DeltaPhi, w)
		h["MuMT"].Fill(MT, w)
		if plotparams.isMC: 
			h["MuMW"].Fill(WbornMass, w)
			h["MU"].Fill(chain.mu, w)
		else:
			h["MU"].Fill(chain.mu / 1.09, w)

	if plotparams.DoElectrons and chain.InclusiveSelection == 1:
		DeltaPhi = chain.met_phi - chain.el_phi[0]
		if DeltaPhi > mPi: DeltaPhi = DeltaPhi - 2 * mPi
		if DeltaPhi < -mPi: DeltaPhi = DeltaPhi + 2 * mPi
		if DeltaPhi < 0: DeltaPhi = - DeltaPhi
		pTl = 0.001 * chain.el_pt[0]
		caloIso = 0.001 * chain.el_topoetcone20[0] / pTl
		trkIso = 0.001 * chain.el_ptvarcone20[0] / pTl
		MET = 0.001 * chain.met_met
		if (pTl < 27) or (MET < 27): continue
		MT = np.sqrt(2 * pTl * MET * (1 - np.cos(DeltaPhi)))
		if plotparams.isMC:
			WbornMass = 0.001 * chain.WbornMass
		#weights, if MC
		w = 1.0
		if plotparams.isMC:
			w = w * chain.weight_mc
			w = w * chain.weight_pileup
			sumWeights += w
			w = w * chain.LPXKfactor
			w = w * chain.weight_indiv_SF_EL_ID
			w = w * chain.weight_indiv_SF_EL_Isol
			w = w * chain.weight_indiv_SF_EL_Trigger
			w = w * chain.weight_indiv_SF_EL_Reco
		h["ElETA"].Fill(chain.el_eta[0], w)
		h["ElPT"].Fill(pTl, w)
		h["ElMET"].Fill(MET, w)
		h["ElDPHI"].Fill(DeltaPhi, w)
		h["ElMT"].Fill(MT, w)
		h["ElCALOISO"].Fill(caloIso, w)
		h["ElTRKISO"].Fill(trkIso, w)
		h["ElD0SIG"].Fill(chain.el_d0sig[0], w)
		h["ElZ0SINT"].Fill(chain.el_delta_z0_sintheta[0], w)
		if plotparams.isMC: 
			h["ElMW"].Fill(WbornMass, w)
			h["MU"].Fill(chain.mu, w)
		else:
			h["MU"].Fill(chain.mu / 1.09, w)

#Normalize to luminosity if is MC
if plotparams.isMC:
	scale = plotparams.Xsec * sumWeights
	if plotparams.DoMuons:
		scale = scale * plotparams.MuLumi / plotparams.initialEvtMC
		N = h["MU"].Integral()
		h["MU"].Scale(scale / N)
		if plotparams.isMC: h["MuMW"].Scale(scale / N)
		for BaseName in plotparams.MuBaseNames:
			N = h[BaseName].Integral()
			h[BaseName].Scale(scale / N)
	if plotparams.DoElectrons:
		scale = scale * plotparams.ElLumi / plotparams.initialEvtMC
		N = h["MU"].Integral()
		h["MU"].Scale(scale / N)
		if plotparams.isMC: h["ElMW"].Scale(scale / N)
		for BaseName in ElBaseNames:
			#sys.stderr.write(BaseName+" Final sum weights: "+"{0:6.5e}".format(h[BaseName].Integral())+"\n")
			N = h[BaseName].Integral()
			h[BaseName].Scale(scale / N)
#save histograms
f = {} 
if plotparams.DoMuons:
	f["MU"] = ROOT.TFile("hMU.root", "recreate")
	h["MU"].Write()
	f["MU"].Write()
	f["MU"].Close()
	if plotparams.isMC:
		f["MuMW"] = ROOT.TFile("hElMW.root", "recreate")
		h["MuMW"].Write()
		f["MuMW"].Write()
		f["MuMW"].Close()
	for BaseName in MuBaseNames:
		f[BaseName] = ROOT.TFile("h"+BaseName+".root", "recreate")
		h[BaseName].Write()
		f[BaseName].Write()
		f[BaseName].Close()

if plotparams.DoElectrons:
	f["MU"] = ROOT.TFile("hMU.root", "recreate")
	h["MU"].Write()
	f["MU"].Write()
	f["MU"].Close()
	if plotparams.isMC:
		f["ElMW"] = ROOT.TFile("hElMW.root", "recreate")
		h["ElMW"].Write()
		f["ElMW"].Write()
		f["ElMW"].Close()
	for BaseName in ElBaseNames:
		f[BaseName] = ROOT.TFile("h"+BaseName+".root", "recreate")
		h[BaseName].Write()
		f[BaseName].Write()
		f[BaseName].Close()


