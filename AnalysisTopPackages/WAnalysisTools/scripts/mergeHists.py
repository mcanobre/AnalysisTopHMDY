#!/usr/bin/env python
"""
Simple tool for sum histograms.
"""
import sys
import ROOT

if len(sys.argv) < 3:
	sys.exit("Usage: mergeHists [hist Name] [file list] ...\n")

hFile = []
hName = sys.argv[1]
files = open(sys.argv[2], "r")
fName1 = files.readline()
fName1 = fName1[:-1]
sys.stdout.write("openning"+fName1+"\n")
hFile.append(ROOT.TFile(fName1, "r"))
if hFile[0].IsZombie(): sys.exit("Error openning file.\n")
h = hFile[0].Get(hName)

while True:
	fName = files.readline()
	fName = fName[:-1]
	if fName == "": break
	sys.stdout.write("openning "+fName+"\n")
	hFile.append(ROOT.TFile(fName, "r"))
	n = len(hFile)
	h.Add(hFile[n - 1].Get(hName))

files.close()
outFile = ROOT.TFile("h"+hName+".root", "recreate")
h.Write()
outFile.Write()
outFile.Close()

for f in hFile: f.Close()
