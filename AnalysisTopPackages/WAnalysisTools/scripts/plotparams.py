#!/usr/bin/env python

MuBaseNames = [
	"MuEta",
	"MuPt",
	"MuMET",
	"MuMT",
	"MuDeltaPhi"
]

ElBaseNames = [
	"ElEta",
	"ElPt",
	"ElMET",
	"ElMT",
	"ElDeltaPhi"
]

DoMuons = False
DoElectrons = True
isMC = True

#For MC
ElEff = 0.08844
MuEff = 0.0
#triggers HLT_e24_lhmedium_L1EM20VH HLT_e60_lhmedium HLT_e120_lhloose
ElLumi = 394.131
#triggers HLT_mu50 HLT_mu20_iloose_L1MU15
MuLumi = 262.754
#Xsec for *Wplusenu_120M180
Xsec = 0.032053
