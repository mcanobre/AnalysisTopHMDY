/*
 * LPXKfactorTool.cxx
 *
 *  Created on: Jan 30, 2015
 *      Author: Markus Zinser (markus.zinser@cern.ch)
 */

#include "LPXKfactorTool/LPXKfactorTool.h"

//#include "LPXKfactorTool/Zprime2012KFactors.h"
//#include "LPXKfactorTool/QCDKFac_CT10NLO_8TeV_CentralValue_WMINUS.h"
//#include "LPXKfactorTool/QCDKFac_CT10NLO_8TeV_CentralValue_WPLUS.h"
//#include "LPXKfactorTool/QCDKFac_MSTW2008LO_8TeV_CentralValue_WMINUS.h"
//#include "LPXKfactorTool/QCDKFac_MSTW2008LO_8TeV_CentralValue_WPLUS.h"

#include "KFactors/CT14/Zgamma_CT14nnlo_CT10.h" // http://hep.ph.liv.ac.uk/~uklein/Vrap13TeV/fits/pdffits/CT14nnlo/Zgamma_CT10nnlo_CT14nnlo.C.pdf
#include "KFactors/CT14/Zgamma_CT14nnlo_NNPDF23_lo_as_0130_qed.h" // http://hep.ph.liv.ac.uk/~uklein/Vrap13TeV/fits/pdffits/CT14nnlo/Zgamma_CT14nnlo_NNPDF23_lo_as_0130_qed.C.pdf
#include "KFactors/z_scalelow.h"
#include "KFactors/z_scaleup.h"
#include "KFactors/EW/ew_z_spline.h"
#include "KFactors/EW/Zgamma_CT10nnlo_CT10nnlo.h"

#include "KFactors/CT14/Wminus_CT14nnlo_CT10.h" // http://hep.ph.liv.ac.uk/~uklein/Vrap13TeV/fits/pdffits/CT14nnlo/Wminus_CT14nnlo_CT10.C.pdf
#include "KFactors/CT14/Wminus_CT14nnlo_NNPDF23_lo_as_0130_qed.h" // http://hep.ph.liv.ac.uk/~uklein/Vrap13TeV/fits/pdffits/CT14nnlo/Wminus_CT14nnlo_NNPDF23_lo_as_0130_qed.C.pdf
#include "KFactors/wm_scalelow.h"
#include "KFactors/wm_scaleup.h"
#include "KFactors/EW/ew_wm_spline.h"
#include "KFactors/EW/Wminus_CT10nnlo_CT10nnlo.h"

#include "KFactors/CT14/Wplus_CT14nnlo_CT10.h" // http://hep.ph.liv.ac.uk/~uklein/Vrap13TeV/fits/pdffits/CT14nnlo/Wplus_CT14nnlo_CT10.C.pdf
#include "KFactors/CT14/Wplus_CT14nnlo_NNPDF23_lo_as_0130_qed.h" // http://hep.ph.liv.ac.uk/~uklein/Vrap13TeV/fits/pdffits/CT14nnlo/Wplus_CT14nnlo_NNPDF23_lo_as_0130_qed.C.pdf
#include "KFactors/wp_scalelow.h"
#include "KFactors/wp_scaleup.h"
#include "KFactors/EW/ew_wp_spline.h"
#include "KFactors/EW/Wplus_CT10nnlo_CT10nnlo.h"

#include "KFactors/PI/z_all_pi_ave_spline.h"
#include "KFactors/PI/z_all_pi_mem0_spline.h"
#include "KFactors/PI/z_all_pi_mem1_spline.h"

// PDF Sys
#include "PDFSystematics/CT14/Zgamma_ct14nnlo_pdf_up.h" // http://hep.ph.liv.ac.uk/~uklein/Vrap13TeV/fits/pdffits/CT14nnlo/Zgamma_ct14nnlo_pdf_up.C.pdf
#include "PDFSystematics/CT14/Zgamma_ct14nnlo_pdf_down.h" // http://hep.ph.liv.ac.uk/~uklein/Vrap13TeV/fits/pdffits/CT14nnlo/Zgamma_ct14nnlo_pdf_down.C.pdf
#include "PDFSystematics/CT14/Wplus_ct14nnlo_pdf_up.h" // http://hep.ph.liv.ac.uk/~uklein/Vrap13TeV/fits/pdffits/CT14nnlo/Wplus_ct14nnlo_pdf_up.C.pdf
#include "PDFSystematics/CT14/Wplus_ct14nnlo_pdf_down.h" // http://hep.ph.liv.ac.uk/~uklein/Vrap13TeV/fits/pdffits/CT14nnlo/Wplus_ct14nnlo_pdf_down.C.pdf
#include "PDFSystematics/CT14/Wminus_ct14nnlo_pdf_up.h" // http://hep.ph.liv.ac.uk/~uklein/Vrap13TeV/fits/pdffits/CT14nnlo/Wminus_ct14nnlo_pdf_up.C.pdf
#include "PDFSystematics/CT14/Wminus_ct14nnlo_pdf_down.h" // http://hep.ph.liv.ac.uk/~uklein/Vrap13TeV/fits/pdffits/CT14nnlo/Wminus_ct14nnlo_pdf_down.C.pdf
#include "PDFSystematics/CT14/Wall_ct14nnlo_pdf_up.h" // http://hep.ph.liv.ac.uk/~uklein/Vrap13TeV/fits/pdffits/CT14nnlo/Wall_ct14nnlo_pdf_up.C.pdf
#include "PDFSystematics/CT14/Wall_ct14nnlo_pdf_down.h" // http://hep.ph.liv.ac.uk/~uklein/Vrap13TeV/fits/pdffits/CT14nnlo/Wall_ct14nnlo_pdf_down.C.pdf
#include "PDFSystematics/CT14/Wall_Wplus_CT14nnlo.h" // http://hep.ph.liv.ac.uk/~uklein/Vrap13TeV/fits/pdffits/CT14nnlo/Wall_Wplus_CT14nnlo.C.pdf
#include "PDFSystematics/CT14/Wall_Wminus_CT14nnlo.h" // http://hep.ph.liv.ac.uk/~uklein/Vrap13TeV/fits/pdffits/CT14nnlo/Wall_Wminus_CT14nnlo.C.pdf

// PDF Sys Reduced EV Set (CT14 authors): http://hep.ph.liv.ac.uk/~uklein/Vrap13TeV/fits/pdffits/CT14bundles/EWbun/
#include "PDFSystematics/CT14_EVRed/Wcomb_CT14nnlo_EWbun_EV_1.h"
#include "PDFSystematics/CT14_EVRed/Wcomb_CT14nnlo_EWbun_EV_2.h"
#include "PDFSystematics/CT14_EVRed/Wcomb_CT14nnlo_EWbun_EV_3.h"
#include "PDFSystematics/CT14_EVRed/Wcomb_CT14nnlo_EWbun_EV_4.h"
#include "PDFSystematics/CT14_EVRed/Wcomb_CT14nnlo_EWbun_EV_5.h"
#include "PDFSystematics/CT14_EVRed/Wcomb_CT14nnlo_EWbun_EV_6.h"
#include "PDFSystematics/CT14_EVRed/Wcomb_CT14nnlo_EWbun_EV_7.h"
#include "PDFSystematics/CT14_EVRed/Zgamma_CT14nnlo_EWbun_EV_1.h"
#include "PDFSystematics/CT14_EVRed/Zgamma_CT14nnlo_EWbun_EV_2.h"
#include "PDFSystematics/CT14_EVRed/Zgamma_CT14nnlo_EWbun_EV_3.h"
#include "PDFSystematics/CT14_EVRed/Zgamma_CT14nnlo_EWbun_EV_4.h"
#include "PDFSystematics/CT14_EVRed/Zgamma_CT14nnlo_EWbun_EV_5.h"
#include "PDFSystematics/CT14_EVRed/Zgamma_CT14nnlo_EWbun_EV_6.h"
#include "PDFSystematics/CT14_EVRed/Zgamma_CT14nnlo_EWbun_EV_7.h"

// PDF Choice
#include "PDFSystematics/CT14/Zgamma_NNPDF30_CT14nnlo.h" // http://hep.ph.liv.ac.uk/~uklein/Vrap13TeV/fits/pdffits/CT14nnlo/Zgamma_NNPDF30_CT14nnlo.C.pdf
#include "PDFSystematics/CT14/Wall_NNPDF30_CT14nnlo.h" // http://hep.ph.liv.ac.uk/~uklein/Vrap13TeV/fits/pdffits/CT14nnlo/Wall_NNPDF30_CT14nnlo.C.pdf
#include "PDFSystematics/CT14/Wplus_NNPDF30_CT14nnlo.h" // http://hep.ph.liv.ac.uk/~uklein/Vrap13TeV/fits/pdffits/CT14nnlo/Wplus_NNPDF30_CT14nnlo.C.pdf
#include "PDFSystematics/CT14/Wminus_NNPDF30_CT14nnlo.h" // http://hep.ph.liv.ac.uk/~uklein/Vrap13TeV/fits/pdffits/CT14nnlo/Wminus_NNPDF30_CT14nnlo.C.pdf
#include "PDFSystematics/CT14/Zgamma_HERAPDF20_CT14nnlo.h" // http://hep.ph.liv.ac.uk/~uklein/Vrap13TeV/fits/pdffits/CT14nnlo/Zgamma_HERAPDF20_CT14nnlo.C.pdf
#include "PDFSystematics/CT14/Wplus_HERAPDF20_CT14nnlo.h" // http://hep.ph.liv.ac.uk/~uklein/Vrap13TeV/fits/pdffits/CT14nnlo/Wplus_HERAPDF20_CT14nnlo.C.pdf
#include "PDFSystematics/CT14/Wminus_HERAPDF20_CT14nnlo.h" // http://hep.ph.liv.ac.uk/~uklein/Vrap13TeV/fits/pdffits/CT14nnlo/Wminus_HERAPDF20_CT14nnlo.C.pdf

// Alpha_S
#include "PDFSystematics/AlphaS/Zgamma_CT14nnlo_as_0118_alpha_s_down_fit.h"
#include "PDFSystematics/AlphaS/Zgamma_CT14nnlo_as_0118_alpha_s_up_fit.h"
#include "PDFSystematics/AlphaS/Wcomb_CT14nnlo_as_0118_alpha_s_down_fit.h"
#include "PDFSystematics/AlphaS/Wcomb_CT14nnlo_as_0118_alpha_s_up_fit.h"
#include "PDFSystematics/AlphaS/Wminus_CT14nnlo_as_0118_alpha_s_down_fit.h" 
#include "PDFSystematics/AlphaS/Wminus_CT14nnlo_as_0118_alpha_s_up_fit.h"
#include "PDFSystematics/AlphaS/Wplus_CT14nnlo_as_0118_alpha_s_down_fit.h"
#include "PDFSystematics/AlphaS/Wplus_CT14nnlo_as_0118_alpha_s_up_fit.h"

// Beam Energy
#include "PDFSystematics/BeamEnergy/Zgamma_beamUp_spline.h"
#include "PDFSystematics/BeamEnergy/Zgamma_beamDown_spline.h"
#include "PDFSystematics/BeamEnergy/Wplus_beamUp_spline.h"
#include "PDFSystematics/BeamEnergy/Wplus_beamDown_spline.h"
#include "PDFSystematics/BeamEnergy/Wminus_beamUp_spline.h"
#include "PDFSystematics/BeamEnergy/Wminus_beamDown_spline.h"

#include "CrossSections/DC14CrossSections.h"
#include "CrossSections/MC15CrossSections.h"

LPXKfactorTool::LPXKfactorTool( const std::string& name ) :
	asg::AsgTool( name ),
	classname(name.c_str())
{

	declareProperty("isMC15", m_isMC15=true, "If false DC14 configuration will be loaded");
	declareProperty("applyEWCorr", m_applyEWCorr=true, "Apply k-Factor for EW corrections");
	declareProperty("applyPICorr", m_applyPICorr=true, "Apply k-Factor for PI corrections");
	declareProperty("kFactorWeightName", m_kFactorWeightName="KfactorWeight", "Name of KFactor decoration");

	bornMass = 0;
	charge   = 0;
	m_truthParticleKey = "";
	m_sysKey = "";
	m_eventInfo = 0;
}

LPXKfactorTool::~LPXKfactorTool() {
}

StatusCode
LPXKfactorTool::initialize(){

	// Currently doing nothing
    ATH_MSG_INFO ("Initializing " << name() << "...");
    if( m_isMC15 ) {
        ATH_MSG_INFO ("Using MC15 configuration.");
        m_truthParticleKey = "TruthParticles";
    }else{
        ATH_MSG_INFO ("Using DC14 configuration.");
        m_truthParticleKey = "TruthParticle";
    }
    if( m_applyEWCorr ){
        ATH_MSG_INFO ("Applying EW k-Factor.");
    }else{
        ATH_MSG_INFO ("NOT applying EW k-Factor.");
    }
    if( m_applyPICorr ){
        ATH_MSG_INFO ("Applying PI k-Factor.");
    }else{
        ATH_MSG_INFO ("NOT applying PI k-Factor.");
    }

	// set up for default running without systematics
	if (!applySystematicVariation (CP::SystematicSet ())) {
		Error(classname,"loading the central value systematic set failed");
		return StatusCode::FAILURE;
	}
	// Add the affecting systematics to the global registry
	CP::SystematicRegistry& registry = CP::SystematicRegistry::getInstance();
	if (!registry.registerSystematics(*this)){
		Error (classname, "unable to register the systematics");
		return StatusCode::FAILURE;
	}
	Info(classname,"Successfully initialized! ");

	return StatusCode::SUCCESS;

}

StatusCode
LPXKfactorTool::execute(){
  double kFactorWeight = 1.;
  ATH_CHECK(getKFactor(kFactorWeight));
  m_eventInfo->auxdecor<double>(m_kFactorWeightName) = kFactorWeight;
  
  return StatusCode::SUCCESS;
}

StatusCode
LPXKfactorTool::getKFactor( double& kFactorWeight ){
  
	if(m_truthParticleKey==""){
	    ATH_MSG_FATAL("Truth particle container key not set. Please initialize the tool.");
	}

	// Retrieve event info
	ATH_CHECK(evtStore()->retrieve(m_eventInfo, "EventInfo"));

	// Retrieve truth particles
	const xAOD::TruthParticleContainer* truthParticles = 0;
	ATH_CHECK(evtStore()->retrieve( truthParticles, m_truthParticleKey ));


	// Select born propagator mass and calculate k-Factor
	kFactorWeight = 1.0;
	if(isPowhegPythiaZllMC(m_eventInfo)){

		setBosonBornMass(truthParticles, m_eventInfo, 23);
		kFactorWeight = Zgamma_CT14nnlo_CT10(bornMass);
		//if(m_applyEWCorr) kFactorWeight*=ew_z_spline(bornMass);
		if(m_applyEWCorr) kFactorWeight*=(1+((ew_z_spline(bornMass)-1)/Zgamma_CT10nnlo_CT10nnlo(bornMass)));
		if(m_applyPICorr && m_sysKey != "LPX_KFACTOR_PI__1up" && m_sysKey != "LPX_KFACTOR_PI__1down"){
			//kFactorWeight*=(1+z_all_pi_ave_spline(bornMass)/100.);
		}

		if(m_sysKey == "LPX_KFACTOR_CHOICE_HERAPDF20") kFactorWeight*=Zgamma_HERAPDF20_CT14nnlo(bornMass);
		else if(m_sysKey == "LPX_KFACTOR_CHOICE_NNPDF30")   kFactorWeight*=Zgamma_NNPDF30_CT14nnlo(bornMass);
		else if(m_sysKey == "LPX_KFACTOR_PDF__1up")         kFactorWeight*=(1+Zgamma_ct14nnlo_pdf_up(bornMass)/100.);
		else if(m_sysKey == "LPX_KFACTOR_PDF__1down")       kFactorWeight*=(1+Zgamma_ct14nnlo_pdf_down(bornMass)/100.);
		else if(m_sysKey == "LPX_KFACTOR_PI__1up")          kFactorWeight*=(1+z_all_pi_mem0_spline(bornMass)/100.);
		else if(m_sysKey == "LPX_KFACTOR_PI__1down")        kFactorWeight*=(1+z_all_pi_mem1_spline(bornMass)/100.);
		else if(m_sysKey == "LPX_KFACTOR_SCALE_Z__1up")     kFactorWeight*=1.0+z_scaleup(bornMass)/100.;
		else if(m_sysKey == "LPX_KFACTOR_SCALE_Z__1down")   kFactorWeight*=1.0+z_scalelow(bornMass)/100.;
		else if(m_sysKey == "LPX_KFACTOR_ALPHAS__1up")     kFactorWeight*=1.0+Zgamma_CT14nnlo_as_0118_alpha_s_up_fit(bornMass)/100.;
		else if(m_sysKey == "LPX_KFACTOR_ALPHAS__1down")   kFactorWeight*=1.0+Zgamma_CT14nnlo_as_0118_alpha_s_down_fit(bornMass)/100.;
		else if(m_sysKey == "LPX_KFACTOR_BEAM_ENERGY__1up")     kFactorWeight*=1.0+Zgamma_beamUp_spline(bornMass)/100.;
		else if(m_sysKey == "LPX_KFACTOR_BEAM_ENERGY__1down")   kFactorWeight*=1.0+Zgamma_beamDown_spline(bornMass)/100.;
		else if(m_sysKey == "LPX_KFACTOR_PDF_EV1")        kFactorWeight*=(1+Zgamma_CT14nnlo_EWbun_EV_1(bornMass)/100.);
		else if(m_sysKey == "LPX_KFACTOR_PDF_EV2")        kFactorWeight*=(1+Zgamma_CT14nnlo_EWbun_EV_2(bornMass)/100.);
		else if(m_sysKey == "LPX_KFACTOR_PDF_EV3")        kFactorWeight*=(1+Zgamma_CT14nnlo_EWbun_EV_3(bornMass)/100.);
		else if(m_sysKey == "LPX_KFACTOR_PDF_EV4")        kFactorWeight*=(1+Zgamma_CT14nnlo_EWbun_EV_4(bornMass)/100.);
		else if(m_sysKey == "LPX_KFACTOR_PDF_EV5")        kFactorWeight*=(1+Zgamma_CT14nnlo_EWbun_EV_5(bornMass)/100.);
		else if(m_sysKey == "LPX_KFACTOR_PDF_EV6")        kFactorWeight*=(1+Zgamma_CT14nnlo_EWbun_EV_6(bornMass)/100.);
		else if(m_sysKey == "LPX_KFACTOR_PDF_EV7")        kFactorWeight*=(1+Zgamma_CT14nnlo_EWbun_EV_7(bornMass)/100.);
		else if(m_sysKey == "LPX_KFACTOR_PDF_EW__1up")	  kFactorWeight*=(2-(1-((ew_z_spline(bornMass)-1))/Zgamma_CT10nnlo_CT10nnlo(bornMass)-1+ew_z_spline(bornMass)));
		else if(m_sysKey == "LPX_KFACTOR_PDF_EW__1down")  kFactorWeight*=((1-((ew_z_spline(bornMass)-1))/Zgamma_CT10nnlo_CT10nnlo(bornMass)-1+ew_z_spline(bornMass)));
		else if(m_sysKey == "LPX_KFACTOR_REDCHOICE_NNPDF30"){
		  // Take Quadratic Sum of PDF Var EV Bundles.
		  double Var_Largest=pow((Zgamma_CT14nnlo_EWbun_EV_1(bornMass)/100.),2)+pow((Zgamma_CT14nnlo_EWbun_EV_2(bornMass)/100.),2)+pow((Zgamma_CT14nnlo_EWbun_EV_3(bornMass)/100.),2)+pow((Zgamma_CT14nnlo_EWbun_EV_4(bornMass)/100.),2)+pow((Zgamma_CT14nnlo_EWbun_EV_5(bornMass)/100.),2)+pow((Zgamma_CT14nnlo_EWbun_EV_6(bornMass)/100.),2)+pow((Zgamma_CT14nnlo_EWbun_EV_7(bornMass)/100.),2);
		  // Take the quadratic difference between the PDF Var and the PDF Choice.
		  double Reduced_Choice=pow((1-Zgamma_NNPDF30_CT14nnlo(bornMass)),2)-Var_Largest;
		  // If the difference^2 is < 0, then PDFVar > PDFChoice, and so PDFChoice = 0.
		  // If the difference^2 is > 0, then PDFChoice > PDFVar, so sqrt to get residual PDFChoice uncertainty.
		  Reduced_Choice=(Reduced_Choice <= 0 ? 0 : sqrt(Reduced_Choice));
		  // Convert back to factor (will always be > 1, so consider symmetrically).
		  kFactorWeight*=(1+Reduced_Choice);
		}

	}else if(isPythiaZllMC(m_eventInfo)){

		setBosonBornMass(truthParticles, m_eventInfo, 23);
		kFactorWeight = Zgamma_CT14nnlo_NNPDF23_lo_as_0130_qed(bornMass);
		//if(m_applyEWCorr) kFactorWeight*=ew_z_spline(bornMass);
		if(m_applyEWCorr) kFactorWeight*=(1+((ew_z_spline(bornMass)-1))/Zgamma_CT10nnlo_CT10nnlo(bornMass));

		if(m_sysKey == "LPX_KFACTOR_CHOICE_HERAPDF20") kFactorWeight*=Zgamma_HERAPDF20_CT14nnlo(bornMass);
		else if(m_sysKey == "LPX_KFACTOR_CHOICE_NNPDF30")   kFactorWeight*=Zgamma_NNPDF30_CT14nnlo(bornMass);
		else if(m_sysKey == "LPX_KFACTOR_PDF__1up")       kFactorWeight*=(1+Zgamma_ct14nnlo_pdf_up(bornMass)/100.);
		else if(m_sysKey == "LPX_KFACTOR_PDF__1down")     kFactorWeight*=(1+Zgamma_ct14nnlo_pdf_down(bornMass)/100.);
		else if(m_sysKey == "LPX_KFACTOR_SCALE_Z__1up")   kFactorWeight*=1.0+z_scaleup(bornMass)/100.;
		else if(m_sysKey == "LPX_KFACTOR_SCALE_Z__1down") kFactorWeight*=1.0+z_scalelow(bornMass)/100.;
		else if(m_sysKey == "LPX_KFACTOR_ALPHAS__1up")     kFactorWeight*=1.0+Zgamma_CT14nnlo_as_0118_alpha_s_up_fit(bornMass)/100.;
		else if(m_sysKey == "LPX_KFACTOR_ALPHAS__1down")   kFactorWeight*=1.0+Zgamma_CT14nnlo_as_0118_alpha_s_down_fit(bornMass)/100.;
		else if(m_sysKey == "LPX_KFACTOR_BEAM_ENERGY__1up")     kFactorWeight*=1.0+Zgamma_beamUp_spline(bornMass)/100.;
		else if(m_sysKey == "LPX_KFACTOR_BEAM_ENERGY__1down")   kFactorWeight*=1.0+Zgamma_beamDown_spline(bornMass)/100.;
		else if(m_sysKey == "LPX_KFACTOR_PDF_EV1")        kFactorWeight*=(1+Zgamma_CT14nnlo_EWbun_EV_1(bornMass)/100.);
		else if(m_sysKey == "LPX_KFACTOR_PDF_EV2")        kFactorWeight*=(1+Zgamma_CT14nnlo_EWbun_EV_2(bornMass)/100.);
		else if(m_sysKey == "LPX_KFACTOR_PDF_EV3")        kFactorWeight*=(1+Zgamma_CT14nnlo_EWbun_EV_3(bornMass)/100.);
		else if(m_sysKey == "LPX_KFACTOR_PDF_EV4")        kFactorWeight*=(1+Zgamma_CT14nnlo_EWbun_EV_4(bornMass)/100.);
		else if(m_sysKey == "LPX_KFACTOR_PDF_EV5")        kFactorWeight*=(1+Zgamma_CT14nnlo_EWbun_EV_5(bornMass)/100.);
		else if(m_sysKey == "LPX_KFACTOR_PDF_EV6")        kFactorWeight*=(1+Zgamma_CT14nnlo_EWbun_EV_6(bornMass)/100.);
		else if(m_sysKey == "LPX_KFACTOR_PDF_EV7")        kFactorWeight*=(1+Zgamma_CT14nnlo_EWbun_EV_7(bornMass)/100.);
		else if(m_sysKey == "LPX_KFACTOR_PDF_EW__1up")	  kFactorWeight*=(2-(1-((ew_z_spline(bornMass)-1))/Zgamma_CT10nnlo_CT10nnlo(bornMass)-1+ew_z_spline(bornMass)));
		else if(m_sysKey == "LPX_KFACTOR_PDF_EW__1down")  kFactorWeight*=((1-((ew_z_spline(bornMass)-1))/Zgamma_CT10nnlo_CT10nnlo(bornMass)-1+ew_z_spline(bornMass)));
		else if(m_sysKey == "LPX_KFACTOR_REDCHOICE_NNPDF30"){
		  // Take Quadratic Sum of PDF Var EV Bundles.
		  double Var_Largest=pow((Zgamma_CT14nnlo_EWbun_EV_1(bornMass)/100.),2)+pow((Zgamma_CT14nnlo_EWbun_EV_2(bornMass)/100.),2)+pow((Zgamma_CT14nnlo_EWbun_EV_3(bornMass)/100.),2)+pow((Zgamma_CT14nnlo_EWbun_EV_4(bornMass)/100.),2)+pow((Zgamma_CT14nnlo_EWbun_EV_5(bornMass)/100.),2)+pow((Zgamma_CT14nnlo_EWbun_EV_6(bornMass)/100.),2)+pow((Zgamma_CT14nnlo_EWbun_EV_7(bornMass)/100.),2);
		  // Take the quadratic difference between the PDF Var and the PDF Choice.
		  double Reduced_Choice=pow((1-Zgamma_NNPDF30_CT14nnlo(bornMass)),2)-Var_Largest;
		  // If the difference^2 is < 0, then PDFVar > PDFChoice, and so PDFChoice = 0.
		  // If the difference^2 is > 0, then PDFChoice > PDFVar, so sqrt to get residual PDFChoice uncertainty.
		  Reduced_Choice=(Reduced_Choice <= 0 ? 0 : sqrt(Reduced_Choice));
		  // Convert back to factor (will always be > 1, so consider symmetrically).
		  kFactorWeight*=(1+Reduced_Choice);
		}

	}else if(isPythiaZPrimeMC(m_eventInfo)){

		setBosonBornMass(truthParticles, m_eventInfo, 32);
		kFactorWeight = Zgamma_CT14nnlo_NNPDF23_lo_as_0130_qed(bornMass);

		if(m_sysKey == "LPX_KFACTOR_CHOICE_HERAPDF20") kFactorWeight*=Zgamma_HERAPDF20_CT14nnlo(bornMass);
		else if(m_sysKey == "LPX_KFACTOR_CHOICE_NNPDF30")   kFactorWeight*=Zgamma_NNPDF30_CT14nnlo(bornMass);
		else if(m_sysKey == "LPX_KFACTOR_PDF__1up")       kFactorWeight*=(1+Zgamma_ct14nnlo_pdf_up(bornMass)/100.);
		else if(m_sysKey == "LPX_KFACTOR_PDF__1down")     kFactorWeight*=(1+Zgamma_ct14nnlo_pdf_down(bornMass)/100.);
		else if(m_sysKey == "LPX_KFACTOR_SCALE_Z__1up")   kFactorWeight*=1.0+z_scaleup(bornMass)/100.;
		else if(m_sysKey == "LPX_KFACTOR_SCALE_Z__1down") kFactorWeight*=1.0+z_scalelow(bornMass)/100.;
		else if(m_sysKey == "LPX_KFACTOR_ALPHAS__1up")     kFactorWeight*=1.0+Zgamma_CT14nnlo_as_0118_alpha_s_up_fit(bornMass)/100.;
		else if(m_sysKey == "LPX_KFACTOR_ALPHAS__1down")   kFactorWeight*=1.0+Zgamma_CT14nnlo_as_0118_alpha_s_down_fit(bornMass)/100.;
		else if(m_sysKey == "LPX_KFACTOR_BEAM_ENERGY__1up")     kFactorWeight*=1.0+Zgamma_beamUp_spline(bornMass)/100.;
		else if(m_sysKey == "LPX_KFACTOR_BEAM_ENERGY__1down")   kFactorWeight*=1.0+Zgamma_beamDown_spline(bornMass)/100.;
		else if(m_sysKey == "LPX_KFACTOR_PDF_EV1")        kFactorWeight*=(1+Zgamma_CT14nnlo_EWbun_EV_1(bornMass)/100.);
		else if(m_sysKey == "LPX_KFACTOR_PDF_EV2")        kFactorWeight*=(1+Zgamma_CT14nnlo_EWbun_EV_2(bornMass)/100.);
		else if(m_sysKey == "LPX_KFACTOR_PDF_EV3")        kFactorWeight*=(1+Zgamma_CT14nnlo_EWbun_EV_3(bornMass)/100.);
		else if(m_sysKey == "LPX_KFACTOR_PDF_EV4")        kFactorWeight*=(1+Zgamma_CT14nnlo_EWbun_EV_4(bornMass)/100.);
		else if(m_sysKey == "LPX_KFACTOR_PDF_EV5")        kFactorWeight*=(1+Zgamma_CT14nnlo_EWbun_EV_5(bornMass)/100.);
		else if(m_sysKey == "LPX_KFACTOR_PDF_EV6")        kFactorWeight*=(1+Zgamma_CT14nnlo_EWbun_EV_6(bornMass)/100.);
		else if(m_sysKey == "LPX_KFACTOR_PDF_EV7")        kFactorWeight*=(1+Zgamma_CT14nnlo_EWbun_EV_7(bornMass)/100.);
		else if(m_sysKey == "LPX_KFACTOR_REDCHOICE_NNPDF30"){
		  // Take Quadratic Sum of PDF Var EV Bundles.
		  double Var_Largest=pow((Zgamma_CT14nnlo_EWbun_EV_1(bornMass)/100.),2)+pow((Zgamma_CT14nnlo_EWbun_EV_2(bornMass)/100.),2)+pow((Zgamma_CT14nnlo_EWbun_EV_3(bornMass)/100.),2)+pow((Zgamma_CT14nnlo_EWbun_EV_4(bornMass)/100.),2)+pow((Zgamma_CT14nnlo_EWbun_EV_5(bornMass)/100.),2)+pow((Zgamma_CT14nnlo_EWbun_EV_6(bornMass)/100.),2)+pow((Zgamma_CT14nnlo_EWbun_EV_7(bornMass)/100.),2);
		  // Take the quadratic difference between the PDF Var and the PDF Choice.
		  double Reduced_Choice=pow((1-Zgamma_NNPDF30_CT14nnlo(bornMass)),2)-Var_Largest;
		  // If the difference^2 is < 0, then PDFVar > PDFChoice, and so PDFChoice = 0.
		  // If the difference^2 is > 0, then PDFChoice > PDFVar, so sqrt to get residual PDFChoice uncertainty.
		  Reduced_Choice=(Reduced_Choice <= 0 ? 0 : sqrt(Reduced_Choice));
		  // Convert back to factor (will always be > 1, so consider symmetrically).
		  kFactorWeight*=(1+Reduced_Choice);
		}

	}else if(isPythiaCIMC(m_eventInfo)){

		if(isPythiaElectronCIMC(m_eventInfo)){
			setCIBornMass(truthParticles, m_eventInfo, 11);
		}
		if(isPythiaMuonCIMC(m_eventInfo)){
			setCIBornMass(truthParticles, m_eventInfo, 13);
		}
		kFactorWeight = Zgamma_CT14nnlo_NNPDF23_lo_as_0130_qed(bornMass);
		//if(m_applyEWCorr) kFactorWeight*=ew_z_spline(bornMass);
		if(m_applyEWCorr) kFactorWeight*=(1+((ew_z_spline(bornMass)-1))/Zgamma_CT10nnlo_CT10nnlo(bornMass));

		if(m_sysKey == "LPX_KFACTOR_CHOICE_HERAPDF20") kFactorWeight*=Zgamma_HERAPDF20_CT14nnlo(bornMass);
		else if(m_sysKey == "LPX_KFACTOR_CHOICE_NNPDF30")   kFactorWeight*=Zgamma_NNPDF30_CT14nnlo(bornMass);
		else if(m_sysKey == "LPX_KFACTOR_PDF__1up")       kFactorWeight*=(1+Zgamma_ct14nnlo_pdf_up(bornMass)/100.);
		else if(m_sysKey == "LPX_KFACTOR_PDF__1down")     kFactorWeight*=(1+Zgamma_ct14nnlo_pdf_down(bornMass)/100.);
		else if(m_sysKey == "LPX_KFACTOR_SCALE_Z__1up")   kFactorWeight*=1.0+z_scaleup(bornMass)/100.;
		else if(m_sysKey == "LPX_KFACTOR_SCALE_Z__1down") kFactorWeight*=1.0+z_scalelow(bornMass)/100.;
		else if(m_sysKey == "LPX_KFACTOR_ALPHAS__1up")     kFactorWeight*=1.0+Zgamma_CT14nnlo_as_0118_alpha_s_up_fit(bornMass)/100.;
		else if(m_sysKey == "LPX_KFACTOR_ALPHAS__1down")   kFactorWeight*=1.0+Zgamma_CT14nnlo_as_0118_alpha_s_down_fit(bornMass)/100.;
		else if(m_sysKey == "LPX_KFACTOR_BEAM_ENERGY__1up")     kFactorWeight*=1.0+Zgamma_beamUp_spline(bornMass)/100.;
		else if(m_sysKey == "LPX_KFACTOR_BEAM_ENERGY__1down")   kFactorWeight*=1.0+Zgamma_beamDown_spline(bornMass)/100.;
		else if(m_sysKey == "LPX_KFACTOR_PDF_EV1")        kFactorWeight*=(1+Zgamma_CT14nnlo_EWbun_EV_1(bornMass)/100.);
		else if(m_sysKey == "LPX_KFACTOR_PDF_EV2")        kFactorWeight*=(1+Zgamma_CT14nnlo_EWbun_EV_2(bornMass)/100.);
		else if(m_sysKey == "LPX_KFACTOR_PDF_EV3")        kFactorWeight*=(1+Zgamma_CT14nnlo_EWbun_EV_3(bornMass)/100.);
		else if(m_sysKey == "LPX_KFACTOR_PDF_EV4")        kFactorWeight*=(1+Zgamma_CT14nnlo_EWbun_EV_4(bornMass)/100.);
		else if(m_sysKey == "LPX_KFACTOR_PDF_EV5")        kFactorWeight*=(1+Zgamma_CT14nnlo_EWbun_EV_5(bornMass)/100.);
		else if(m_sysKey == "LPX_KFACTOR_PDF_EV6")        kFactorWeight*=(1+Zgamma_CT14nnlo_EWbun_EV_6(bornMass)/100.);
		else if(m_sysKey == "LPX_KFACTOR_PDF_EV7")        kFactorWeight*=(1+Zgamma_CT14nnlo_EWbun_EV_7(bornMass)/100.);
		else if(m_sysKey == "LPX_KFACTOR_PDF_EW__1up")	  kFactorWeight*=(2-(1-((ew_z_spline(bornMass)-1))/Zgamma_CT10nnlo_CT10nnlo(bornMass)-1+ew_z_spline(bornMass)));
		else if(m_sysKey == "LPX_KFACTOR_PDF_EW__1down")  kFactorWeight*=((1-((ew_z_spline(bornMass)-1))/Zgamma_CT10nnlo_CT10nnlo(bornMass)-1+ew_z_spline(bornMass)));
		else if(m_sysKey == "LPX_KFACTOR_REDCHOICE_NNPDF30"){
		  // Take Quadratic Sum of PDF Var EV Bundles.
		  double Var_Largest=pow((Zgamma_CT14nnlo_EWbun_EV_1(bornMass)/100.),2)+pow((Zgamma_CT14nnlo_EWbun_EV_2(bornMass)/100.),2)+pow((Zgamma_CT14nnlo_EWbun_EV_3(bornMass)/100.),2)+pow((Zgamma_CT14nnlo_EWbun_EV_4(bornMass)/100.),2)+pow((Zgamma_CT14nnlo_EWbun_EV_5(bornMass)/100.),2)+pow((Zgamma_CT14nnlo_EWbun_EV_6(bornMass)/100.),2)+pow((Zgamma_CT14nnlo_EWbun_EV_7(bornMass)/100.),2);
		  // Take the quadratic difference between the PDF Var and the PDF Choice.
		  double Reduced_Choice=pow((1-Zgamma_NNPDF30_CT14nnlo(bornMass)),2)-Var_Largest;
		  // If the difference^2 is < 0, then PDFVar > PDFChoice, and so PDFChoice = 0.
		  // If the difference^2 is > 0, then PDFChoice > PDFVar, so sqrt to get residual PDFChoice uncertainty.
		  Reduced_Choice=(Reduced_Choice <= 0 ? 0 : sqrt(Reduced_Choice));
		  // Convert back to factor (will always be > 1, so consider symmetrically).
		  kFactorWeight*=(1+Reduced_Choice);
		}

	}else if(isPowhegPythiaWlnuMC(m_eventInfo)){

		setBosonBornMass(truthParticles, m_eventInfo, 24);
		if(charge > 0){
			kFactorWeight = Wplus_CT14nnlo_CT10(bornMass);
			//if(m_applyEWCorr) kFactorWeight*=ew_wp_spline(bornMass);
			if(m_applyEWCorr) kFactorWeight*=(1+((ew_wp_spline(bornMass)-1))/Wplus_CT10nnlo_CT10nnlo(bornMass));

			if(m_sysKey == "LPX_KFACTOR_CHOICE_HERAPDF20") kFactorWeight*=Wplus_HERAPDF20_CT14nnlo(bornMass);
			else if(m_sysKey == "LPX_KFACTOR_CHOICE_NNPDF30")   kFactorWeight*=Wplus_NNPDF30_CT14nnlo(bornMass);
			//if(m_sysKey == "LPX_KFACTOR_PDF__1up")       kFactorWeight*=(1+Wall_ct14nnlo_pdf_up(bornMass)/100./Wall_Wplus_CT14nnlo(bornMass));
			//if(m_sysKey == "LPX_KFACTOR_PDF__1down")     kFactorWeight*=(1+Wall_ct14nnlo_pdf_down(bornMass)/100./Wall_Wplus_CT14nnlo(bornMass));
			else if(m_sysKey == "LPX_KFACTOR_PDF__1up")       kFactorWeight*=1+Wall_ct14nnlo_pdf_up(bornMass)/100.;
			else if(m_sysKey == "LPX_KFACTOR_PDF__1down")     kFactorWeight*=1+Wall_ct14nnlo_pdf_down(bornMass)/100.;
			else if(m_sysKey == "LPX_KFACTOR_SCALE_W__1up")   kFactorWeight*=1.0+wp_scaleup(bornMass)/100.;
			else if(m_sysKey == "LPX_KFACTOR_SCALE_W__1down") kFactorWeight*=1.0+wp_scalelow(bornMass)/100.;
			else if(m_sysKey == "LPX_KFACTOR_ALPHAS__1up")     kFactorWeight*=1.0+Wplus_CT14nnlo_as_0118_alpha_s_up_fit(bornMass)/100.;
			else if(m_sysKey == "LPX_KFACTOR_ALPHAS__1down")   kFactorWeight*=1.0+Wplus_CT14nnlo_as_0118_alpha_s_down_fit(bornMass)/100.;
			else if(m_sysKey == "LPX_KFACTOR_BEAM_ENERGY__1up")     kFactorWeight*=1.0+Wplus_beamUp_spline(bornMass)/100.;
			else if(m_sysKey == "LPX_KFACTOR_BEAM_ENERGY__1down")   kFactorWeight*=1.0+Wplus_beamDown_spline(bornMass)/100.;
			else if(m_sysKey == "LPX_KFACTOR_PDF_EV1")        kFactorWeight*=(1+Wcomb_CT14nnlo_EWbun_EV_1(bornMass)/100.);
			else if(m_sysKey == "LPX_KFACTOR_PDF_EV2")        kFactorWeight*=(1+Wcomb_CT14nnlo_EWbun_EV_2(bornMass)/100.);
			else if(m_sysKey == "LPX_KFACTOR_PDF_EV3")        kFactorWeight*=(1+Wcomb_CT14nnlo_EWbun_EV_3(bornMass)/100.);
			else if(m_sysKey == "LPX_KFACTOR_PDF_EV4")        kFactorWeight*=(1+Wcomb_CT14nnlo_EWbun_EV_4(bornMass)/100.);
			else if(m_sysKey == "LPX_KFACTOR_PDF_EV5")        kFactorWeight*=(1+Wcomb_CT14nnlo_EWbun_EV_5(bornMass)/100.);
			else if(m_sysKey == "LPX_KFACTOR_PDF_EV6")        kFactorWeight*=(1+Wcomb_CT14nnlo_EWbun_EV_6(bornMass)/100.);
			else if(m_sysKey == "LPX_KFACTOR_PDF_EV7")        kFactorWeight*=(1+Wcomb_CT14nnlo_EWbun_EV_7(bornMass)/100.);
			else if(m_sysKey == "LPX_KFACTOR_PDF_EW__1up")	  kFactorWeight*=(2-(1-((ew_wp_spline(bornMass)-1))/Wplus_CT10nnlo_CT10nnlo(bornMass)-1+ew_wp_spline(bornMass)));
			else if(m_sysKey == "LPX_KFACTOR_PDF_EW__1down")  kFactorWeight*=((1-((ew_wp_spline(bornMass)-1))/Wplus_CT10nnlo_CT10nnlo(bornMass)-1+ew_wp_spline(bornMass)));
			else if(m_sysKey == "LPX_KFACTOR_REDCHOICE_NNPDF30"){
			  // Take Quadratic Sum of PDF Var EV Bundles.
			  double Var_Largest=pow((Wcomb_CT14nnlo_EWbun_EV_1(bornMass)/100.),2)+pow((Wcomb_CT14nnlo_EWbun_EV_2(bornMass)/100.),2)+pow((Wcomb_CT14nnlo_EWbun_EV_3(bornMass)/100.),2)+pow((Wcomb_CT14nnlo_EWbun_EV_4(bornMass)/100.),2)+pow((Wcomb_CT14nnlo_EWbun_EV_5(bornMass)/100.),2)+pow((Wcomb_CT14nnlo_EWbun_EV_6(bornMass)/100.),2)+pow((Wcomb_CT14nnlo_EWbun_EV_7(bornMass)/100.),2);
			  // Take the quadratic difference between the PDF Var and the PDF Choice.
			  double Reduced_Choice=pow((1-Wplus_NNPDF30_CT14nnlo(bornMass)),2)-Var_Largest;
			  // If the difference^2 is < 0, then PDFVar > PDFChoice, and so PDFChoice = 0.
			  // If the difference^2 is > 0, then PDFChoice > PDFVar, so sqrt to get residual PDFChoice uncertainty.
			  Reduced_Choice=(Reduced_Choice <= 0 ? 0 : sqrt(Reduced_Choice));
			  // Convert back to factor (will always be > 1, so consider symmetrically).
			  if((Wplus_NNPDF30_CT14nnlo(bornMass)-1) < 0) Reduced_Choice *= -1;
			  if( Wplus_NNPDF30_CT14nnlo(bornMass) < 0 ) kFactorWeight = 0;
			  kFactorWeight*=(1+Reduced_Choice);
			}	       
		}else{
			kFactorWeight = Wminus_CT14nnlo_CT10(bornMass);
			//if(m_applyEWCorr) kFactorWeight*=ew_wm_spline(bornMass);
			if(m_applyEWCorr) kFactorWeight*=(1+((ew_wm_spline(bornMass)-1))/Wminus_CT10nnlo_CT10nnlo(bornMass));

			if(m_sysKey == "LPX_KFACTOR_CHOICE_HERAPDF20") kFactorWeight*=Wminus_HERAPDF20_CT14nnlo(bornMass);
			else if(m_sysKey == "LPX_KFACTOR_CHOICE_NNPDF30")   kFactorWeight*=Wminus_NNPDF30_CT14nnlo(bornMass);
			//if(m_sysKey == "LPX_KFACTOR_PDF__1up")       kFactorWeight*=(1+Wall_ct14nnlo_pdf_up(bornMass)/100./Wall_Wminus_CT14nnlo(bornMass));
			//if(m_sysKey == "LPX_KFACTOR_PDF__1down")     kFactorWeight*=(1+Wall_ct14nnlo_pdf_down(bornMass)/100./Wall_Wminus_CT14nnlo(bornMass));
			else if(m_sysKey == "LPX_KFACTOR_PDF__1up")       kFactorWeight*=1+Wall_ct14nnlo_pdf_up(bornMass)/100.;
			else if(m_sysKey == "LPX_KFACTOR_PDF__1down")     kFactorWeight*=1+Wall_ct14nnlo_pdf_down(bornMass)/100.;
			else if(m_sysKey == "LPX_KFACTOR_SCALE_W__1up")   kFactorWeight*=1.0+wm_scaleup(bornMass)/100.;
			else if(m_sysKey == "LPX_KFACTOR_SCALE_W__1down") kFactorWeight*=1.0+wm_scalelow(bornMass)/100.;
			else if(m_sysKey == "LPX_KFACTOR_ALPHAS__1up")     kFactorWeight*=1.0+Wminus_CT14nnlo_as_0118_alpha_s_up_fit(bornMass)/100.;
			else if(m_sysKey == "LPX_KFACTOR_ALPHAS__1down")   kFactorWeight*=1.0+Wminus_CT14nnlo_as_0118_alpha_s_down_fit(bornMass)/100.;
			else if(m_sysKey == "LPX_KFACTOR_BEAM_ENERGY__1up")     kFactorWeight*=1.0+Wminus_beamUp_spline(bornMass)/100.;
			else if(m_sysKey == "LPX_KFACTOR_BEAM_ENERGY__1down")   kFactorWeight*=1.0+Wminus_beamDown_spline(bornMass)/100.;
			else if(m_sysKey == "LPX_KFACTOR_PDF_EV1")        kFactorWeight*=(1+Wcomb_CT14nnlo_EWbun_EV_1(bornMass)/100.);
			else if(m_sysKey == "LPX_KFACTOR_PDF_EV2")        kFactorWeight*=(1+Wcomb_CT14nnlo_EWbun_EV_2(bornMass)/100.);
			else if(m_sysKey == "LPX_KFACTOR_PDF_EV3")        kFactorWeight*=(1+Wcomb_CT14nnlo_EWbun_EV_3(bornMass)/100.);
			else if(m_sysKey == "LPX_KFACTOR_PDF_EV4")        kFactorWeight*=(1+Wcomb_CT14nnlo_EWbun_EV_4(bornMass)/100.);
			else if(m_sysKey == "LPX_KFACTOR_PDF_EV5")        kFactorWeight*=(1+Wcomb_CT14nnlo_EWbun_EV_5(bornMass)/100.);
			else if(m_sysKey == "LPX_KFACTOR_PDF_EV6")        kFactorWeight*=(1+Wcomb_CT14nnlo_EWbun_EV_6(bornMass)/100.);
			else if(m_sysKey == "LPX_KFACTOR_PDF_EV7")        kFactorWeight*=(1+Wcomb_CT14nnlo_EWbun_EV_7(bornMass)/100.);
			else if(m_sysKey == "LPX_KFACTOR_PDF_EW__1up")	  kFactorWeight*=(2-(1-((ew_wm_spline(bornMass)-1))/Wminus_CT10nnlo_CT10nnlo(bornMass)-1+ew_wm_spline(bornMass)));
			else if(m_sysKey == "LPX_KFACTOR_PDF_EW__1down")  kFactorWeight*=((1-((ew_wm_spline(bornMass)-1))/Wminus_CT10nnlo_CT10nnlo(bornMass)-1+ew_wm_spline(bornMass)));
			else if(m_sysKey == "LPX_KFACTOR_REDCHOICE_NNPDF30"){
			  // Take Quadratic Sum of PDF Var EV Bundles.
			  double Var_Largest=pow((Wcomb_CT14nnlo_EWbun_EV_1(bornMass)/100.),2)+pow((Wcomb_CT14nnlo_EWbun_EV_2(bornMass)/100.),2)+pow((Wcomb_CT14nnlo_EWbun_EV_3(bornMass)/100.),2)+pow((Wcomb_CT14nnlo_EWbun_EV_4(bornMass)/100.),2)+pow((Wcomb_CT14nnlo_EWbun_EV_5(bornMass)/100.),2)+pow((Wcomb_CT14nnlo_EWbun_EV_6(bornMass)/100.),2)+pow((Wcomb_CT14nnlo_EWbun_EV_7(bornMass)/100.),2);
			  // Take the quadratic difference between the PDF Var and the PDF Choice.
			  double Reduced_Choice=pow((1-Wminus_NNPDF30_CT14nnlo(bornMass)),2)-Var_Largest;
			  // If the difference^2 is < 0, then PDFVar > PDFChoice, and so PDFChoice = 0.
			  // If the difference^2 is > 0, then PDFChoice > PDFVar, so sqrt to get residual PDFChoice uncertainty.
			  Reduced_Choice=(Reduced_Choice <= 0 ? 0 : sqrt(Reduced_Choice));
			  // Convert back to factor (will always be > 1, so consider symmetrically).
			  if((Wminus_NNPDF30_CT14nnlo(bornMass)-1) < 0) Reduced_Choice *= -1;
			  if( Wminus_NNPDF30_CT14nnlo(bornMass) < 0 ) kFactorWeight = 0;
			  kFactorWeight*=(1+Reduced_Choice);
			}	       
		}

	}else if(isPythiaWPrimeMC(m_eventInfo) || isPythiaFlatWPrimeMC(m_eventInfo)){
		setBosonBornMass(truthParticles, m_eventInfo, 34);
		if(charge > 0){
			kFactorWeight = Wplus_CT14nnlo_NNPDF23_lo_as_0130_qed(bornMass);

			if(m_sysKey == "LPX_KFACTOR_CHOICE_HERAPDF20") kFactorWeight*=Wplus_HERAPDF20_CT14nnlo(bornMass);
			else if(m_sysKey == "LPX_KFACTOR_CHOICE_NNPDF30")   kFactorWeight*=Wplus_NNPDF30_CT14nnlo(bornMass);
			//if(m_sysKey == "LPX_KFACTOR_PDF__1up")       kFactorWeight*=(1+Wall_ct14nnlo_pdf_up(bornMass)/100./Wall_Wplus_CT14nnlo(bornMass));
			//if(m_sysKey == "LPX_KFACTOR_PDF__1down")     kFactorWeight*=(1+Wall_ct14nnlo_pdf_down(bornMass)/100./Wall_Wplus_CT14nnlo(bornMass));
			else if(m_sysKey == "LPX_KFACTOR_PDF__1up")       kFactorWeight*=1+Wall_ct14nnlo_pdf_up(bornMass)/100.;
			else if(m_sysKey == "LPX_KFACTOR_PDF__1down")     kFactorWeight*=1+Wall_ct14nnlo_pdf_down(bornMass)/100.;
			else if(m_sysKey == "LPX_KFACTOR_SCALE_W__1up")   kFactorWeight*=1.0+wp_scaleup(bornMass)/100.;
			else if(m_sysKey == "LPX_KFACTOR_SCALE_W__1down") kFactorWeight*=1.0+wp_scalelow(bornMass)/100.;
			else if(m_sysKey == "LPX_KFACTOR_ALPHAS__1up")     kFactorWeight*=1.0+Wplus_CT14nnlo_as_0118_alpha_s_up_fit(bornMass)/100.;
			else if(m_sysKey == "LPX_KFACTOR_ALPHAS__1down")   kFactorWeight*=1.0+Wplus_CT14nnlo_as_0118_alpha_s_down_fit(bornMass)/100.;
			else if(m_sysKey == "LPX_KFACTOR_BEAM_ENERGY__1up")     kFactorWeight*=1.0+Wplus_beamUp_spline(bornMass)/100.;
			else if(m_sysKey == "LPX_KFACTOR_BEAM_ENERGY__1down")   kFactorWeight*=1.0+Wplus_beamDown_spline(bornMass)/100.;
			else if(m_sysKey == "LPX_KFACTOR_PDF_EV1")        kFactorWeight*=(1+Wcomb_CT14nnlo_EWbun_EV_1(bornMass)/100.);
			else if(m_sysKey == "LPX_KFACTOR_PDF_EV2")        kFactorWeight*=(1+Wcomb_CT14nnlo_EWbun_EV_2(bornMass)/100.);
			else if(m_sysKey == "LPX_KFACTOR_PDF_EV3")        kFactorWeight*=(1+Wcomb_CT14nnlo_EWbun_EV_3(bornMass)/100.);
			else if(m_sysKey == "LPX_KFACTOR_PDF_EV4")        kFactorWeight*=(1+Wcomb_CT14nnlo_EWbun_EV_4(bornMass)/100.);
			else if(m_sysKey == "LPX_KFACTOR_PDF_EV5")        kFactorWeight*=(1+Wcomb_CT14nnlo_EWbun_EV_5(bornMass)/100.);
			else if(m_sysKey == "LPX_KFACTOR_PDF_EV6")        kFactorWeight*=(1+Wcomb_CT14nnlo_EWbun_EV_6(bornMass)/100.);
			else if(m_sysKey == "LPX_KFACTOR_PDF_EV7")        kFactorWeight*=(1+Wcomb_CT14nnlo_EWbun_EV_7(bornMass)/100.);
			else if(m_sysKey == "LPX_KFACTOR_REDCHOICE_NNPDF30"){
			  // Take Quadratic Sum of PDF Var EV Bundles.
			  double Var_Largest=pow((Wcomb_CT14nnlo_EWbun_EV_1(bornMass)/100.),2)+pow((Wcomb_CT14nnlo_EWbun_EV_2(bornMass)/100.),2)+pow((Wcomb_CT14nnlo_EWbun_EV_3(bornMass)/100.),2)+pow((Wcomb_CT14nnlo_EWbun_EV_4(bornMass)/100.),2)+pow((Wcomb_CT14nnlo_EWbun_EV_5(bornMass)/100.),2)+pow((Wcomb_CT14nnlo_EWbun_EV_6(bornMass)/100.),2)+pow((Wcomb_CT14nnlo_EWbun_EV_7(bornMass)/100.),2);
			  // Take the quadratic difference between the PDF Var and the PDF Choice.
			  double Reduced_Choice=pow((1-Wplus_NNPDF30_CT14nnlo(bornMass)),2)-Var_Largest;
			  // If the difference^2 is < 0, then PDFVar > PDFChoice, and so PDFChoice = 0.
			  // If the difference^2 is > 0, then PDFChoice > PDFVar, so sqrt to get residual PDFChoice uncertainty.
			  Reduced_Choice=(Reduced_Choice <= 0 ? 0 : sqrt(Reduced_Choice));
			  // Convert back to factor (will always be > 1, so consider symmetrically).
			  if((Wplus_NNPDF30_CT14nnlo(bornMass)-1) < 0) Reduced_Choice *= -1;
			  if( Wplus_NNPDF30_CT14nnlo(bornMass) < 0 ) kFactorWeight = 0;
			  kFactorWeight*=(1+Reduced_Choice);
			}	       
		}else{
		  kFactorWeight = Wminus_CT14nnlo_NNPDF23_lo_as_0130_qed(bornMass);

			if(m_sysKey == "LPX_KFACTOR_CHOICE_HERAPDF20") kFactorWeight*=Wminus_HERAPDF20_CT14nnlo(bornMass);
			else if(m_sysKey == "LPX_KFACTOR_CHOICE_NNPDF30")   kFactorWeight*=Wminus_NNPDF30_CT14nnlo(bornMass);
			//if(m_sysKey == "LPX_KFACTOR_PDF__1up")       kFactorWeight*=(1+Wall_ct14nnlo_pdf_up(bornMass)/100./Wall_Wminus_CT14nnlo(bornMass));
			//if(m_sysKey == "LPX_KFACTOR_PDF__1down")     kFactorWeight*=(1+Wall_ct14nnlo_pdf_down(bornMass)/100./Wall_Wminus_CT14nnlo(bornMass));
			else if(m_sysKey == "LPX_KFACTOR_PDF__1up")       kFactorWeight*=1+Wall_ct14nnlo_pdf_up(bornMass)/100.;
			else if(m_sysKey == "LPX_KFACTOR_PDF__1down")     kFactorWeight*=1+Wall_ct14nnlo_pdf_down(bornMass)/100.;
			else if(m_sysKey == "LPX_KFACTOR_SCALE_W__1up")   kFactorWeight*=1.0+wm_scaleup(bornMass)/100.;
			else if(m_sysKey == "LPX_KFACTOR_SCALE_W__1down") kFactorWeight*=1.0+wm_scalelow(bornMass)/100.;
			else if(m_sysKey == "LPX_KFACTOR_ALPHAS__1up")     kFactorWeight*=1.0+Wminus_CT14nnlo_as_0118_alpha_s_up_fit(bornMass)/100.;
			else if(m_sysKey == "LPX_KFACTOR_ALPHAS__1down")   kFactorWeight*=1.0+Wminus_CT14nnlo_as_0118_alpha_s_down_fit(bornMass)/100.;
			else if(m_sysKey == "LPX_KFACTOR_BEAM_ENERGY__1up")     kFactorWeight*=1.0+Wminus_beamUp_spline(bornMass)/100.;
			else if(m_sysKey == "LPX_KFACTOR_BEAM_ENERGY__1down")   kFactorWeight*=1.0+Wminus_beamDown_spline(bornMass)/100.;
			else if(m_sysKey == "LPX_KFACTOR_PDF_EV1")        kFactorWeight*=(1+Wcomb_CT14nnlo_EWbun_EV_1(bornMass)/100.);
			else if(m_sysKey == "LPX_KFACTOR_PDF_EV2")        kFactorWeight*=(1+Wcomb_CT14nnlo_EWbun_EV_2(bornMass)/100.);
			else if(m_sysKey == "LPX_KFACTOR_PDF_EV3")        kFactorWeight*=(1+Wcomb_CT14nnlo_EWbun_EV_3(bornMass)/100.);
			else if(m_sysKey == "LPX_KFACTOR_PDF_EV4")        kFactorWeight*=(1+Wcomb_CT14nnlo_EWbun_EV_4(bornMass)/100.);
			else if(m_sysKey == "LPX_KFACTOR_PDF_EV5")        kFactorWeight*=(1+Wcomb_CT14nnlo_EWbun_EV_5(bornMass)/100.);
			else if(m_sysKey == "LPX_KFACTOR_PDF_EV6")        kFactorWeight*=(1+Wcomb_CT14nnlo_EWbun_EV_6(bornMass)/100.);
			else if(m_sysKey == "LPX_KFACTOR_PDF_EV7")        kFactorWeight*=(1+Wcomb_CT14nnlo_EWbun_EV_7(bornMass)/100.);
			else if(m_sysKey == "LPX_KFACTOR_REDCHOICE_NNPDF30"){
			  // Take Quadratic Sum of PDF Var EV Bundles.
			  double Var_Largest=pow((Wcomb_CT14nnlo_EWbun_EV_1(bornMass)/100.),2)+pow((Wcomb_CT14nnlo_EWbun_EV_2(bornMass)/100.),2)+pow((Wcomb_CT14nnlo_EWbun_EV_3(bornMass)/100.),2)+pow((Wcomb_CT14nnlo_EWbun_EV_4(bornMass)/100.),2)+pow((Wcomb_CT14nnlo_EWbun_EV_5(bornMass)/100.),2)+pow((Wcomb_CT14nnlo_EWbun_EV_6(bornMass)/100.),2)+pow((Wcomb_CT14nnlo_EWbun_EV_7(bornMass)/100.),2);
			  // Take the quadratic difference between the PDF Var and the PDF Choice.
			  double Reduced_Choice=pow((1-Wminus_NNPDF30_CT14nnlo(bornMass)),2)-Var_Largest;
			  // If the difference^2 is < 0, then PDFVar > PDFChoice, and so PDFChoice = 0.
			  // If the difference^2 is > 0, then PDFChoice > PDFVar, so sqrt to get residual PDFChoice uncertainty.
			  Reduced_Choice=(Reduced_Choice <= 0 ? 0 : sqrt(Reduced_Choice));
			  // Convert back to factor (will always be > 1, so consider symmetrically).
			  if((Wminus_NNPDF30_CT14nnlo(bornMass)-1) < 0) Reduced_Choice *= -1;
			  if( Wminus_NNPDF30_CT14nnlo(bornMass) < 0 ) kFactorWeight = 0;
			  kFactorWeight*=(1+Reduced_Choice);
			}	       
		}
	}else if(isTTbarUBMC(m_eventInfo)){
		// PowhegPythia ttbar
		// https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MC15TTbarSamplesPMG
		kFactorWeight = 1.1949;
	}else if(isTTbarMBMC(m_eventInfo)){
		// Sherpa k-Factor 1.1484 (PMG Twiki)
		kFactorWeight = 1.1484;
		// Matching Factor Weight = 1.37.
		kFactorWeight *= 1.37;
	}else if(isDBUBMC(m_eventInfo)){
		kFactorWeight = 0.91;
	}else if(isDBMBMC(m_eventInfo)){
	  kFactorWeight = 0.91;
	  // lvlv
	  if ((m_eventInfo->mcChannelNumber() >= 303014 && m_eventInfo->mcChannelNumber() <= 303021) || (m_eventInfo->mcChannelNumber() >= 303046 && m_eventInfo->mcChannelNumber() <= 303053)){
	    kFactorWeight *= 1.49;
	  }
	  // llll
	  if ((m_eventInfo->mcChannelNumber() >= 303022 && m_eventInfo->mcChannelNumber() <= 303029) || (m_eventInfo->mcChannelNumber() >= 303054 && m_eventInfo->mcChannelNumber() <= 303061)){
	    kFactorWeight *= 1.86;
	  }
	  // lvll
	  if ((m_eventInfo->mcChannelNumber() >= 303030 && m_eventInfo->mcChannelNumber() <= 303037) || (m_eventInfo->mcChannelNumber() >= 303062 && m_eventInfo->mcChannelNumber() <= 303069)){
	    kFactorWeight *= 1.54;
	  }
	  // qqll
	  if ((m_eventInfo->mcChannelNumber() >= 303038 && m_eventInfo->mcChannelNumber() <= 303045) || (m_eventInfo->mcChannelNumber() >= 303070 && m_eventInfo->mcChannelNumber() <= 303077)){
	    kFactorWeight *= 0.8;
	  }
	}else if(m_eventInfo->mcChannelNumber() == 410011){
		// SingleTop t-channel
		// https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MC15SingleTopSamplesPMG
		kFactorWeight = 1.0094;
	}else if(m_eventInfo->mcChannelNumber() == 410012){
		// SingleTop t-channel
		// https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MC15SingleTopSamplesPMG
		kFactorWeight = 1.0193;
	}else if(m_eventInfo->mcChannelNumber() == 410013){
		// SingleTop Wt top
		// https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MC15SingleTopSamplesPMG
		kFactorWeight = 1.054;
	}else if(m_eventInfo->mcChannelNumber() == 410014){
		// SingleTop Wt anti-top
		// https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MC15SingleTopSamplesPMG
		kFactorWeight = 1.054;
	}else if(m_eventInfo->mcChannelNumber() == 410015){
		// SingleTop Wt top (dilepton filter)
		// https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MC15SingleTopSamplesPMG
		kFactorWeight = 1.054;
	}else if(m_eventInfo->mcChannelNumber() == 410016){
		// SingleTop Wt anti-top (dilepton filter)
		// https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MC15SingleTopSamplesPMG
		kFactorWeight = 1.054;
	}else if(m_eventInfo->mcChannelNumber() == 410025){
		// Single Top
	        // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/XsecSummarySingleTop
		kFactorWeight = 1.0046;
	}else if(m_eventInfo->mcChannelNumber() == 410026){
		// Single Anti-Top
	        // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/XsecSummarySingleTop
		kFactorWeight = 1.0215;
	}else if((m_eventInfo->mcChannelNumber() >= 364100 && m_eventInfo->mcChannelNumber() <= 364141) || (m_eventInfo->mcChannelNumber() >= 363388 && m_eventInfo->mcChannelNumber() <= 363411) || (m_eventInfo->mcChannelNumber() >= 363361 && m_eventInfo->mcChannelNumber() <= 363387) || (m_eventInfo->mcChannelNumber() >= 363102 && m_eventInfo->mcChannelNumber() <= 363122)){
	        // Sherpa 2.2.1 Zee, Zmumu, Ztautau
	        // https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/XsecSummaryZjetsSherpa221 //
	        // Sherpa 2.2.0 Zjets: ee, mumu, tautau //
	        // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/XsecSummaryZjetsSherpa22Light //
	        // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/XsecSummaryZjetsSherpa22C //
	        // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/XsecSummaryZjetsSherpa22B //
	        kFactorWeight = 0.9751;
	}else if((m_eventInfo->mcChannelNumber() >= 363436 && m_eventInfo->mcChannelNumber() <= 363483) || (m_eventInfo->mcChannelNumber() >= 363331 && m_eventInfo->mcChannelNumber() <= 363354) || (m_eventInfo->mcChannelNumber() >= 364156 && m_eventInfo->mcChannelNumber() <= 364197)){
	        // Sherpa 2.2.0 Wjets: enu, munu, taunu //
	        // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/XsecSummaryWjetsSherpa22Light //
	        // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/XsecSummaryWjetsSherpa22C //
	        // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/XsecSummaryWjetsSherpa22B //
	        // Sherpa 2.2.1 W+jets: enu, munu, taunu
	        // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/XsecSummaryWjetsSherpa221 //
	        kFactorWeight = 0.9702;
	}else{

		kFactorWeight = 1.0;

	}

	// Set k-Factor to 0 for events with mass above 10 TeV since no valid k-Factor is available
	if(bornMass >= 9500.0){
		kFactorWeight = 0.0;
	}
	// Set k-Factor to 0 for events with negative cross section (can happen for NNPDF)
	if(kFactorWeight < 0.0){
		kFactorWeight = 0.0;
	}

	// reset m_sys_key
	m_sysKey = "";

	return StatusCode::SUCCESS;

}


bool
LPXKfactorTool::isAffectedBySystematic( const CP::SystematicVariation& systematic )
const {

    CP::SystematicSet sys = affectingSystematics();
    return sys.find (systematic) != sys.end ();

}


/// returns: the list of all systematics this tool can be affected by
CP::SystematicSet
LPXKfactorTool::affectingSystematics()
const{
    CP::SystematicSet result;
    result.insert (CP::SystematicVariation ("LPX_KFACTOR_CHOICE_HERAPDF20"));
    result.insert (CP::SystematicVariation ("LPX_KFACTOR_CHOICE_NNPDF30"));
    result.insert (CP::SystematicVariation ("LPX_KFACTOR_REDCHOICE_NNPDF30"));
    result.insert (CP::SystematicVariation ("LPX_KFACTOR_PDF", 1));
    result.insert (CP::SystematicVariation ("LPX_KFACTOR_PDF", -1));
    result.insert (CP::SystematicVariation ("LPX_KFACTOR_SCALE_Z", 1));
    result.insert (CP::SystematicVariation ("LPX_KFACTOR_SCALE_Z", -1));
    result.insert (CP::SystematicVariation ("LPX_KFACTOR_SCALE_W", 1));
    result.insert (CP::SystematicVariation ("LPX_KFACTOR_SCALE_W", -1));
    result.insert (CP::SystematicVariation ("LPX_KFACTOR_PI", 1));
    result.insert (CP::SystematicVariation ("LPX_KFACTOR_PI", -1));
    result.insert (CP::SystematicVariation ("LPX_KFACTOR_PDF_EV1"));
    result.insert (CP::SystematicVariation ("LPX_KFACTOR_PDF_EV2"));
    result.insert (CP::SystematicVariation ("LPX_KFACTOR_PDF_EV3"));
    result.insert (CP::SystematicVariation ("LPX_KFACTOR_PDF_EV4"));
    result.insert (CP::SystematicVariation ("LPX_KFACTOR_PDF_EV5"));
    result.insert (CP::SystematicVariation ("LPX_KFACTOR_PDF_EV6"));
    result.insert (CP::SystematicVariation ("LPX_KFACTOR_PDF_EV7"));
    result.insert (CP::SystematicVariation ("LPX_KFACTOR_ALPHAS", 1));
    result.insert (CP::SystematicVariation ("LPX_KFACTOR_ALPHAS", -1));
    result.insert (CP::SystematicVariation ("LPX_KFACTOR_PDF_EW", 1));
    result.insert (CP::SystematicVariation ("LPX_KFACTOR_PDF_EW", -1));
    result.insert (CP::SystematicVariation ("LPX_KFACTOR_BEAM_ENERGY", 1));
    result.insert (CP::SystematicVariation ("LPX_KFACTOR_BEAM_ENERGY", -1));


    return result;
}


/// returns: the list of all systematics this tool recommends to use
CP::SystematicSet
LPXKfactorTool::recommendedSystematics()
const{
    return affectingSystematics();
}


CP::SystematicCode
LPXKfactorTool::applySystematicVariation (const CP::SystematicSet& systConfig)
{
    CP::SystematicSet mySysConf;
    static CP::SystematicSet affectingSys = affectingSystematics();
    if (m_filtered_sys_sets.find(systConfig) == m_filtered_sys_sets.end()){
        if (!CP::SystematicSet::filterForAffectingSystematics(systConfig,affectingSys, mySysConf)){
            Error(classname, "Unsupported combination of systematics passed to the tool! ");
            return CP::SystematicCode::Unsupported;
        }
        m_filtered_sys_sets[systConfig] = mySysConf;
    }
    mySysConf = m_filtered_sys_sets[systConfig] ;
    m_sysKey = mySysConf.name();
    //boost::unordered_map<SystematicSet,EffiCollection*>::iterator SFiter = m_sf_sets.find (mySysConf);
    return CP::SystematicCode::Ok;
}


void
LPXKfactorTool::setBosonBornMass(const xAOD::TruthParticleContainer* truthParticles, const xAOD::EventInfo* eventInfo, int pdgID)
{
	bornMass = 0;
	for ( const auto* truthP : *truthParticles ) {
		if(abs(truthP->pdgId()) == pdgID ){
			truthP->pdgId() > 0 ? charge = 1 : charge = -1;
			bornMass = (truthP->p4()).M()/1000.;
			eventInfo->auxdecor<double>("BornMass") = bornMass;
			break;
		}
	}
	if(bornMass==0){
	    ATH_MSG_FATAL("Found no born particle with PDG ID "<<pdgID<<" !");
	}
}


void
LPXKfactorTool::setCIBornMass(const xAOD::TruthParticleContainer* truthParticles, const xAOD::EventInfo* eventInfo, int pdgID)
{
	bornMass = 0;


	  // Before the truth loop
	  TVector3 ele(1,1,1);
	  TVector3 pos(1,1,1);
	  bool ele_found = false;
	  bool ele_found_23 = false;
	  bool ele_found_3 = false;
	  bool ele_found_1 = false;
	  bool pos_found = false;
	  bool pos_found_23 = false;
	  bool pos_found_3 = false;
	  bool pos_found_1 = false;


	for ( const auto* truthP : *truthParticles ) {

		int PDG_ID = truthP->pdgId();
		int Status = truthP->status();
		// Inside the truth particle loop, change PDGID to 11 and -11 for dielectron samples
		if (PDG_ID == -pdgID && ( (Status == 23 && !ele_found_23) || (Status == 3 && !ele_found_23 && !ele_found_3) || (Status == 1 && !ele_found_23 && !ele_found_3 && !ele_found_1) ) ){

									ele.SetPtEtaPhi( truthP->pt(), truthP->eta(), truthP->phi());
									ele_found = true;
									if (Status == 23) ele_found_23 = true;
									if (Status == 3)  ele_found_3 = true;
									if (Status == 1)  ele_found_1 = true;


		} // end of search for muon

		if (PDG_ID == pdgID && ( (Status == 23 && !pos_found_23) || (Status == 3 && !pos_found_23 && !pos_found_3) || (Status == 1 && !pos_found_23 && !pos_found_3 && !pos_found_1) ) ){

									pos.SetPtEtaPhi( truthP->pt(), truthP->eta(), truthP->phi());
									pos_found = true;
									if (Status == 23) pos_found_23 = true;
									if (Status == 3)  pos_found_3 = true;
									if (Status == 1)  pos_found_1 = true;

		} // end of search for antimuon

		if(pos_found && ele_found) break;

	}

	// After the loop
	bornMass = (sqrt(2*(ele.Mag() * pos.Mag()- ele*pos )))*0.001;
	eventInfo->auxdecor<double>("BornMass") = bornMass;

	if(bornMass==0){
	    ATH_MSG_FATAL("Found no born mass for CI !");
	}

}


//const xAOD::TruthVertex*
//LPXKfactorTool::getDecayVertexOfIncomingParticle(const xAOD::TruthVertexContainer* truthVertices, int pdgID){
//
//	pdgID = fabs(pdgID);
//	xAOD::TruthVertexContainer::const_iterator truthV_itr; // Start iterating over truth container
//	for(truthV_itr = truthVertices->begin(); truthV_itr != truthVertices->end(); ++truthV_itr ) {
//		for(unsigned int iIn=0; iIn < (*truthV_itr)->nIncomingParticles(); iIn++){
//			int curPdgID  = (*truthV_itr)->incomingParticle(iIn)->pdgId();
//			int curStatus = (*truthV_itr)->incomingParticle(iIn)->status();
//			curPdgID = fabs(curPdgID);
//
//			if(curPdgID==pdgID && curStatus==62){
//				return (*truthV_itr);
//			}
//
//		}
//	}
//
//	std::cout<<"Error: No truth vertex with incoming particle of pdg ID "<<pdgID<<" and status 62!"<<std::endl;
//
//	return 0;
//
//}


//void
//LPXKfactorTool::storeBornMassW(const xAOD::TruthVertex* decayVertex, const xAOD::EventInfo* eventInfo)
//{
//
//	const xAOD::TruthParticle* lepton   = 0;
//	const xAOD::TruthParticle* neutrino = 0;
//
//	for(unsigned int iOut=0; iOut < decayVertex->nOutgoingParticles(); iOut++){
//		const xAOD::TruthParticle* outParticle = decayVertex->outgoingParticle(iOut);
//		if(outParticle->isChLepton()) lepton = outParticle;
//		if(outParticle->isNeutrino()) neutrino = outParticle;
//	}
//	bornMass = (lepton->p4()+neutrino->p4()).M();
//	eventInfo->auxdecor<double>("BornMass") = bornMass;
//
//}
//
//
//void
//LPXKfactorTool::storeBornMassZ(const xAOD::TruthVertex* decayVertex, const xAOD::EventInfo* eventInfo)
//{
//
//	const xAOD::TruthParticle* lepton1 = 0;
//	const xAOD::TruthParticle* lepton2 = 0;
//
//	for(unsigned int iOut=0; iOut < decayVertex->nOutgoingParticles(); iOut++){
//		const xAOD::TruthParticle* outParticle = decayVertex->outgoingParticle(iOut);
//		if(outParticle->isChLepton() && (outParticle->charge() > 0)) lepton1 = outParticle;
//		if(outParticle->isChLepton() && (outParticle->charge() < 0)) lepton2 = outParticle;
//	}
//	bornMass = (lepton1->p4()+lepton2->p4()).M();
//	eventInfo->auxdecor<double>("BornMass") = bornMass;
//
//}


bool
LPXKfactorTool::isPowhegPythiaZllMC(const xAOD::EventInfo* eventInfo){

	unsigned int datasetID = eventInfo->mcChannelNumber();

	if(m_isMC15){
		//MC15

		if( (datasetID >= 301000) && (datasetID <= 301018) ){ // mass binned Zee
			return true;
		}
		if( datasetID == 361106){ // Inclusive Zee
			return true;
		}

		if( (datasetID >= 301020) && (datasetID <= 301038) ){ // mass binned Zmumu
			return true;
		}
		if( datasetID == 361107){ // Inclusive Zmumu
			return true;
		}

		if( (datasetID >= 301040) && (datasetID <= 301058) ){ // mass binned Ztautau
			return true;
		}
		if( datasetID == 361108){ // Inclusive Ztautau
			return true;
		}
	}else{
		//DC14

		if( (datasetID >= 203500) && (datasetID <= 203518) ){ // mass binned Zee
			return true;
		}
		if( datasetID == 147446){ // Inclusive Zee Dilepton filter
			return true;
		}
		if( datasetID == 147436){ // Inclusive Zee Single lepton filter
			return true;
		}
		if( datasetID == 147406){ // Inclusive Zee
			return true;
		}

		if( (datasetID >= 203519) && (datasetID <= 203537) ){ // mass binned Zmumu
			return true;
		}
		if( datasetID == 147407){ // Inclusive Zmumu
			return true;
		}

		if( (datasetID >= 203538) && (datasetID <= 203556) ){ // mass binned Ztautau
			return true;
		}
		if( datasetID == 147408){ // Inclusive Ztautau
			return true;
		}
	}

	return false;
}


bool
LPXKfactorTool::isPythiaZllMC(const xAOD::EventInfo* eventInfo){

	unsigned int datasetID = eventInfo->mcChannelNumber();

	if(m_isMC15){
		//MC15

		if( (datasetID >= 301540) && (datasetID <= 301559) ){ // mass binned Zee
			return true;
		}

		if( (datasetID >= 301560) && (datasetID <= 301579) ){ // mass binned Zmumu
			return true;
		}

		if( (datasetID >= 303437) && (datasetID <= 303455) ){ // mass binned Ztautau
			return true;
		}

	}

	return false;
}


bool
LPXKfactorTool::isPowhegPythiaWlnuMC(const xAOD::EventInfo* eventInfo){

	unsigned int datasetID = eventInfo->mcChannelNumber();

	if(m_isMC15){
		//MC15

		if( (datasetID >= 301060) && (datasetID <= 301078) ){ // mass binned Wenuplus
			return true;
		}
		if( (datasetID >= 301080) && (datasetID <= 301098) ){ // mass binned Wenumin
			return true;
		}
		if( datasetID == 361100){ // Inclusive Wenuplus
			return true;
		}
		if( datasetID == 361103){ // Inclusive Wenumin
			return true;
		}

		if( (datasetID >= 301100) && (datasetID <= 301118) ){ // mass binned Wmumuplus
			return true;
		}
		if( (datasetID >= 301120) && (datasetID <= 301138) ){ // mass binned Wmunumin
			return true;
		}
		if( datasetID == 361101){ // Inclusive Wmunuplus
			return true;
		}
		if( datasetID == 361104){ // Inclusive Wmunumin
			return true;
		}

		if( (datasetID >= 301140) && (datasetID <= 301158) ){ // mass binned Wtautauplus/Wtautaumin
			return true;
		}
		if( (datasetID >= 301160) && (datasetID <= 301178) ){ // mass binned Wtautauplus/Wtautaumin
			return true;
		}
		if( datasetID == 361102){ // Inclusive Wtautauplus
			return true;
		}
		if( datasetID == 361105){ // Inclusive Wtautaumin
			return true;
		}
	}else{
		//DC14

		if( (datasetID >= 203557) && (datasetID <= 203594) ){ // mass binned Wenuplus/Wenumin
			return true;
		}
		if( datasetID == 147400){ // Inclusive Wenuplus
			return true;
		}
		if( datasetID == 147403){ // Inclusive Wenumin
			return true;
		}

		if( (datasetID >= 203595) && (datasetID <= 203632) ){ // mass binned Wmumuplus/Wmunumin
			return true;
		}
		if( datasetID == 147401){ // Inclusive Wmunuplus
			return true;
		}
		if( datasetID == 147404){ // Inclusive Wmunumin
			return true;
		}

		if( (datasetID >= 203633) && (datasetID <= 203670) ){ // mass binned Wtautauplus/Wtautaumin
			return true;
		}
		if( datasetID == 147402){ // Inclusive Wtautauplus
			return true;
		}
		if( datasetID == 147405){ // Inclusive Wtautaumin
			return true;
		}
	}

	return false;
}


bool
LPXKfactorTool::isPythiaZPrimeMC(const xAOD::EventInfo* eventInfo){

	unsigned int datasetID = eventInfo->mcChannelNumber();

	if(m_isMC15){
		//MC15
		if( (datasetID >= 301215) && (datasetID <= 301218)){ // ZPrime ee 2, 3, 4, 5 TeV
			return true;
		}
		if( (datasetID >= 301220) && (datasetID <= 301223)){ // ZPrime mumu 2, 3, 4, 5 TeV
			return true;
		}
	}else{
		//DC14
		if( (datasetID >= 182916) && (datasetID <= 182919)){ // ZPrime ee 2, 3, 4, 5 TeV
			return true;
		}
		if( (datasetID >= 182920) && (datasetID <= 182923)){ // ZPrime mumu 2, 3, 4, 5 TeV
			return true;
		}
	}

	return false;
}


bool
LPXKfactorTool::isPythiaCIMC(const xAOD::EventInfo* eventInfo){

	unsigned int datasetID = eventInfo->mcChannelNumber();

	if(m_isMC15){
		//MC15
		if( (datasetID >= 301580) && (datasetID <= 301819)){ // CI Samples
			return true;
		}
	}else{
	}

	return false;

}


bool
LPXKfactorTool::isPythiaElectronCIMC(const xAOD::EventInfo* eventInfo){

	unsigned int datasetID = eventInfo->mcChannelNumber();

	if(m_isMC15){
		//MC15
		if( (datasetID >= 301580) && (datasetID <= 301699)){ // CI Samples
			return true;
		}
	}else{
	}

	return false;

}


bool
LPXKfactorTool::isPythiaMuonCIMC(const xAOD::EventInfo* eventInfo){

	unsigned int datasetID = eventInfo->mcChannelNumber();

	if(m_isMC15){
		//MC15
		if( (datasetID >= 301700) && (datasetID <= 301819)){ // CI Samples
			return true;
		}
	}else{
	}

	return false;

}


bool
LPXKfactorTool::isPythiaFlatWPrimeMC(const xAOD::EventInfo* eventInfo){

	unsigned int datasetID = eventInfo->mcChannelNumber();

	if(m_isMC15){
		//MC15
		if( datasetID == 301533){ // Flat W'enu Sample
			return true;
		}
		if( datasetID == 301534){ // Flat W'munu Sample
			return true;
		}
	}

	return false;
}


bool
LPXKfactorTool::isPythiaWPrimeMC(const xAOD::EventInfo* eventInfo){

	unsigned int datasetID = eventInfo->mcChannelNumber();

	if(m_isMC15){
		//MC15
		if( (datasetID >= 301242) && (datasetID <= 301245)){ // WPrime enu 2, 3, 4, 5 TeV
			return true;
		}
		if( (datasetID >= 301246) && (datasetID <= 301249)){ // WPrime munu 2, 3, 4, 5 TeV
			return true;
		}
	}else{
		//DC14
		if( datasetID == 203671){ // WPrime emutau 2 TeV
			return true;
		}
		if( datasetID == 158762){ // WPrime emutau 3 TeV
			return true;
		}
		if( datasetID == 203672){ // WPrime emutau 4 TeV
			return true;
		}
		if( datasetID == 203673){ // WPrime emutau 5 TeV
			return true;
		}
	}

	return false;
}

bool
LPXKfactorTool::isTTbarUBMC(const xAOD::EventInfo* eventInfo){

	unsigned int datasetID = eventInfo->mcChannelNumber();

	//MC15
	if(datasetID == 410000 || datasetID == 410009){ // ttbar nonallhad and dilepton
		return true;
	}
	return false;
}

bool
LPXKfactorTool::isTTbarMBMC(const xAOD::EventInfo* eventInfo){

	unsigned int datasetID = eventInfo->mcChannelNumber();

	//MC15
	if(datasetID >= 306600 && datasetID <= 306617){ // ttbar ee/mm mass-binned
		return true;
	}
	if(datasetID >= 307485 && datasetID <= 307502){ // ttbar tautau/etau/mtau mass-binned
		return true;
	}

	return false;
}

bool
LPXKfactorTool::isDBUBMC(const xAOD::EventInfo* eventInfo){

	unsigned int datasetID = eventInfo->mcChannelNumber();

	//MC15
	if((datasetID >= 361063 && datasetID <= 361068) || (datasetID >= 361081 && datasetID <= 361088) || (datasetID >= 361091 && datasetID <= 361097) || datasetID == 361071){ // diboson unbinned
		return true;
	}
	return false;
}

bool
LPXKfactorTool::isDBMBMC(const xAOD::EventInfo* eventInfo){

	unsigned int datasetID = eventInfo->mcChannelNumber();

	//MC15
	if(datasetID >= 303014 && datasetID <= 303077){ // diboson mass-binned
		return true;
	}
	return false;
}

double
LPXKfactorTool::getMCCrossSection(){

	const xAOD::EventInfo* eventInfo =0;
	ATH_CHECK(evtStore()->retrieve(eventInfo, "EventInfo"));
	unsigned int datasetID = eventInfo->mcChannelNumber();

	if(m_isMC15){
		return getMC15CrossSection(datasetID);
	}else{
		return getDC14CrossSection(datasetID);
	}

}


double
LPXKfactorTool::getMCFilterEfficiency(){

	const xAOD::EventInfo* eventInfo =0;
	ATH_CHECK(evtStore()->retrieve(eventInfo, "EventInfo"));
	unsigned int datasetID = eventInfo->mcChannelNumber();

	if(m_isMC15){
		return getMC15FilterEfficiency(datasetID);
	}else{
		return getDC14FilterEfficiency(datasetID);
	}

}



